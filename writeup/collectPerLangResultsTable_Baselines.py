with open("../languages/ud_languages_2.8.txt", "r") as inFile:
    languages = inFile.read().strip().split("\n")

languages = sorted(languages)

iso_codes = {}
with open("../languages/languages-iso639-3.tsv", "r") as inFile:
    for x in inFile:
        x = x.strip().split("\t")
        if len(x) > 1:
            iso_codes[x[0]] = x[1]

with open(f"tex/{__file__.replace('.','_')}.tex", "w") as outFile:
  for i in range(0, len(languages), 6):
    print(" & ".join(["{\\footnotesize "+x.replace("_2.8", "").replace("_2.9", "").replace("_", " ")+"}" for x in languages[i:i+6]]), file=outFile)
    print("\\\\", file=outFile)
    for lang in languages[i:i+6]:
        print("\\includegraphics[draft=false,width=0.1\\textwidth]{../analysis/process/figures/perLanguage/process9_frontier18_Baselines_Viz.py_" + lang + "_raw.png-1.png} &", file=outFile)
    print("\\\\", file=outFile)
 #  for lang in languages[i:i+8]:
#    print(lang.replace("_2.8", "").replace("_2.9", "").replace("_", " "), "&", file=outFile)
 #   print(iso_codes[lang.replace("_2.8", "").replace("_2.9", "")], "&", file=outFile)
  #  print("\\includegraphics[draft=false,width=0.1\\textwidth]{../analysis/process/figures/perLanguage/process9_frontier18_Baselines_Viz.py_" + lang + "_raw.png-1.png} &", file=outFile)
#    print(f"\\includegraphics[draft=false,width=0.1\\textwidth]{../analysis/process/figures/perLanguage/process9_frontier18_FuncHead.py_{lang}_smoothed_scaled.pdf}", file=outFile)


#        Language &ISO & Grammar & Interpolated & Category & Attested  & Average  \\
#    & Code               & Samples &              &          & Congruence & Congruence\\
#

