from ud_languages_28 import languages
from corpusIterator_V import CorpusIterator_V as CorpusIterator
import os

paths = set([x[3:x.index("-")] for x in os.listdir("/u/scr/corpora/Universal_Dependencies/Universal_Dependencies_2.8/ud-treebanks-v2.8")])
languages = [x for x in paths if x+"_2.8" not in languages and x+"_2.9" not in languages]

with open("excluded.tex", "w") as outFile:
 for language in sorted(languages):
  try:
   corpus = sorted(list(CorpusIterator(language+"_2.8", "together").iterator()))
   print >> outFile, (language.replace("_2.6", "").replace("_", " ")+" & "+"{:,}".format(len(corpus)) + " & " +"{:,}".format(sum([len(x) for x in corpus]))+"\\\\")
  except AssertionError:
   continue

