# Data for the languages

# Case Marking
* case_marking
* case_marking_groups_2.8.tsv
* case_marking_revised_2.8.tsv

# Categorical Orderings
* categorical_order

# Coexpression
* collectPerVerbProperties

# Corpus Size
* corpus_size

# Extract subject-object position congruence
* extract_congruence

# Families
* families_2.8.tsv
* genera_2.8.tsv

# Extract grammars
* fitGrammars

# Geolocations
* geolocations
* groups2_2.8.tsv

# ISO Codes
* languages-iso639-3.tsv
* languages-iso_codes.tsv

# Trees
* trees
* trees2_2.8.tsv

# List of languages
* ud_languages_2.8.txt
* ud_languages_2.8.txt_sorted
