# Counting co-expressed subjects and objects

* [Count coexpressed subjects and objects](coexpression5.py)
* [Results as used by downstream scripts](outputs/coexpression5.py.tsv)
