import random
import sys
from math import log, exp
from random import random, shuffle
from corpusIterator_V import CorpusIterator_V as CorpusIterator


import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--language', type=str)

args = parser.parse_args()





partition = "together"
sentences = list(CorpusIterator(args.language,partition).iterator())
def annotateChildren(sentence):
   for l in sentence:
      l["children"] = []
   for l in sentence:
      if l["head"] != 0:
         sentence[l["head"]-1]["children"].append(l["index"]) 


from collections import defaultdict
matrix = {}
for x in ["Yes", "No"]:
   matrix[x] = 0
def mean(x):
   return sum(x)/(len(x)+1e-10)


# Quote from paper:
#We quantified the frequency of co-expression of
#subjects and objects by calculating the fraction of all verbs that
#realize at least a subject or an object simultaneously realize
#both.


for sent in sentences:
  annotateChildren(sent)
  for word in sent:
   if "children" not in word:
     continue
   if word["posUni"] == "VERB":
    subjects = [i for i in word["children"] if sent[i-1]["dep"] == "nsubj"]
    objects = [i for i in word["children"] if sent[i-1]["dep"] == "obj"]
    if len(subjects) > 0 or len(objects) > 0:
      coexpressed = "Yes" if len(subjects) > 0 and len(objects) > 0 else "No"
      matrix[coexpressed] += 1
columns = sorted(list(matrix))
print(columns)
with open("outputs/"+__file__+".tsv", "a") as outFile:
   print >> outFile, "\t".join([args.language] + [str(matrix[x]) for x in columns])

