import os
import subprocess
import sys
keys = {}
import sys

import random
import subprocess


languages = []
for i in range(1000, 2100, 100):
   languages.append(f"PARTITION-Icelandic_CENTURY{i}")
for i in range(1100, 2050, 50):
   languages.append(f"PARTITION-Icelandic_HALFCENTURY{i}")

script = "evaluateCongruence_POS_NoSplit_Icelandic.py"

with open(f"output/{script}.tsv", "r") as inFile:
   next(inFile)
   done_languages = [x.split("\t")[0] for x in inFile]
#with open(f"output/{script}.tsv", "w") as outFile:
#  print("\t".join(["Language", "Model", "Congruence_All", "Congruence_VN", "Congruence_VP", "Congruence_V", "Congruence_VProp"]), file=outFile)
for language in languages:
  if language in done_languages:
    continue
  subprocess.call(['/u/nlp/anaconda/ubuntu_16/envs/py27-mhahn/bin/python2.7', script, "--language="+language])

