# Communicative efficiency in crosslinguistic word order variation and evolution

Code and data for: Michael Hahn, Yang Xu (2022), [Crosslinguistic word order variation reflects evolutionary pressures of dependency and information locality](https://www.pnas.org/doi/10.1073/pnas.2122604119), In Proceedings of the National Academy of Sciences of the United States of America, vol. 119:24.

## Key Code

- [Estimating Memory-Surprisal Tradeoff](optimization/hillClimbing/estimateTradeoffHeldout.py)

## Results from Main Paper

- [Optimized grammars for 80 languages](optimization/output)
- [Figure 3A](analysis/process/change/visualize/figures/viz_frontier_model_50_HighCongruence_Points_Baselines_Frontier_Congruence_Figs_Select_py_overall.pdf)
- [Figure 3B](analysis/process/change/visualize/figures/viz_frontier_model_50_HighCongruence_Points_Baselines_Frontier_Congruence_Figs_py_SOV.pdf)
- [Figure 3C](analysis/process/change/visualize/figures/viz_frontier_model_50_HighCongruence_Points_Baselines_Frontier_Congruence_Figs_py_SVO.pdf)
- [Figure 4A](analysis/process/change/visualize/figures/viz_frontier_model_50_HighCongruence_Points_Congruences_CongCol_py_panel0.pdf)
- [Figure 5A](analysis/process/change/visualize/figures/analyzeDiffusion2_py1.pdf)
- [Figure 5B](analysis/process/change/visualize/figures/analyzeDiffusion2_py2.pdf)
- [Figure 6 (Afro-Asiatic)](analysis/process/change/visualize/figures/viz_frontier_model_50_HighCongruence_Points_Baselines_Frontier_Ancestral_PlotF_COngruences2.py_AfroAsiatic.pdf)
- [Phylogenetic Analysis](analysis/process/change/frontier_noLatent_historical/fits/90model.py.txt)
- [Phylogenetic Analysis, without Indo-European](analysis/process/change/frontier_noLatent_historical/fits/90model_NoIE.py.txt)
- [Phylogenetic Analysis, with case marking](analysis/process/change/frontier_historical/fits/90model_reparam2_Case.py_process9_frontier18_Revised.txt)


### References for Reported Analyses

Note: Nondeterminism in MCMC leads to slight numerical variation in posterior inference. Analysis logs were refreshed after submission to verify robustness of reported results. 

- [Correlation between DL and congruence](analysis/process/figures/perLanguage/together/raw_results/analyze2_process9_frontier18_AverageFAngleSpace_Congruence.R_Distance_DepLen_ComparedFrontier.txt)
- [Impact of congruence on DL in Mixed-Effects Analysis](analysis/process/figures/perLanguage/together/raw_results/analyze2_process9_frontier18_AverageFAngleSpace_Congruence.R_Distance_DepLen_ComparedFrontier.txt)
- [Correlation between Attested and Real Congruence](analysis/process/figures/perLanguage/together/raw_results/analyze2_process9_frontier18_AverageFAngleSpace_Congruence2.R_AverageFAngleSpace.txt)
- [Coadaptation (mixed-effects analysis)](analysis/process/figures/perLanguage/together/raw_results/analyze2_process9_frontier18_AverageFAngleSpace_Congruence2.R_AverageFAngleSpace.txt)
- [Bayes factor comparison to IL and DL only](analysis/process/figures/perLanguage/together/analyze2_process9_frontier15_AverageFAngleSpace_Congruence2_DL-IL.R)
- [Coadaptation excluding Indo-European](analysis/process/figures/perLanguage/together/raw_results/analyze2_process9_frontier18_AverageFAngleSpace_Congruence_NoIE.R_AverageFAngleSpace.txt)
- [Stationary distribution and its correlations](analysis/process/change/frontier_noLatent_historical/fits/90model.py.txt)
- [Stationary distribution and its correlations, without Indo-European](analysis/process/change/frontier_noLatent_historical/fits/90model_NoIE.py.txt)
- [Stationary distribution and its correlations, with case marking](analysis/process/change/frontier_historical/fits/90model_reparam2_Case.py_process9_frontier18_Revised.txt)
- [Coexpression predicted by real and optimized subject-object position congruence](analysis/process/figures/perLanguage/together/raw_results/analyze2_process9_frontier18_AverageFAngleSpace_Congruence_Coexpression.R.txt)


## Results from SI Appendix

- [Efficiency plane](analysis/process/change/visualize/figures/visualize_coadaptation_posterior_Pareto_pareto.pdf)
- [Efficiency plane, without Indo-European](analysis/process/change/visualize/figures/visualize_coadaptation_posterior_Pareto_NoIE_pareto.pdf)
- [Coadaptation](analysis/process/change/visualize/figures/visualize_coadaptation_posterior_coadaptation.pdf)
- [Coadaptation, without Indo-European](analysis/process/change/visualize/figures/visualize_coadaptation_posterior_NoIE_coadaptation.pdf)
- [Codaptation, with case marking](analysis/process/change/visualize/figures/visualize_coadaptation_posterior_case_coadaptation.pdf)
- [Pareto, excluding historical data](analysis/process/change/visualize/figures/visualize_coadaptation_posterior_OnlySynchronic_Pareto_pareto.pdf)
- [Coadaptation, excluding historical data](analysis/process/change/visualize/figures/visualize_coadaptation_posterior_OnlySynchronic_coadaptation.pdf)
- [Coexpression and attested congruence](analysis/process/figures/perLanguage/together/figures/coexpression-congruenceReal.pdf)
- [Coexpression and optimized congruence](analysis/process/figures/perLanguage/together/figures/coexpression-congruencePareto.pdf)
- [Robustness of mixed-effects model to priors](analysis/process/figures/perLanguage/together/posteriors-across-priors/)
- [Results for Greenberg Correlations](analysis/figures/)
- [Phylogenetic Analysis, with areal convergence](analysis/process/change/frontier_geographic_historical/fits/90model_reparam2_b5.py_process9_frontier18_Revised.txt)

