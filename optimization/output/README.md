# Optimized Grammars and IL/DL Values

Grammars optimized via hill-climbing:
* Optimized only for DL: [Basic word order](readGrammars_AUC_OnlyDLM.py.tsv), [full optimized grammars](readGrammars_AUC_OnlyDLMLog.py.tsv)
* Optimized for linear combinations of IL and DL: [Basic word order](readGrammars_AUC_PlusFracDLM.py.tsv), [full optimized grammars](readGrammars_AUC_PlusFracDLMLog.py.tsv)
* Optimized only for IL: [Basic word order](readGrammars.py.tsv), [full optimized grammars](readGrammarsLog.py.tsv)

Grammars optimized via SGD:
* [Basic word order for grammars optimized with SGD](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RecordBasic.py.tsv), [full grammars](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RecordAll.py.tsv)
* [IL/DL statistics for grammars optimized with SGD (for DL only)](SUMMARY_forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVAL.py.tsv)

Other Data:
* [IL and DL for real orderings](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVALReal.py.tsv)
* [Greenberg Correlations for optimized orderings](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RecordGreenberg.py.tsv)
* [IL/DL statistics, neural network estimators](SUMMARY_forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVAL_ForNeural.py.tsv)
* [IL/DL statistics for randomly constructed grammars](SUMMARY_forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVALRandom.py.tsv)

