FILES = []
FILES.append("readGrammars_AUC_DLM.py.tsv")
FILES.append("readGrammars_AUC_FracDLM.py.tsv")
FILES.append("readGrammars_AUC_HalfDLM.py.tsv")
FILES.append("readGrammars_AUC_TwoDLM.py.tsv")


header = ["language", "ID", "basic", "np", "Dist_D", "Dist_N", "Dist_A", "Surprisal2", "DepLen"]

with open("readGrammars_AUC_PlusFracDLM.py.tsv", "w") as outFile:
  print("\t".join(header), file=outFile)
  for f in FILES:
    with open(f, "r") as inFile:
        header_ = next(inFile).strip().split("\t")
        assert header == header_, header_
        for line in inFile:
            print(line.strip(), file=outFile)

header = ["idForProcess", "language", "dependency", "weight"]
with open("readGrammars_AUC_PlusFracDLMLog.py.tsv", "w") as outFile:
  print("\t".join(header), file=outFile)
  for f in FILES:
    with open(f.replace(".py", "Log.py"), "r") as inFile:
        header_ = next(inFile).strip().split("\t")
        assert header == header_, header_
        for line in inFile:
            print(line.strip(), file=outFile)


