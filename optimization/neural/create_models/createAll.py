import subprocess
languages = []
languages.append("Arabic")
languages.append("Basque")
languages.append("English")
languages.append("Finnish")
languages.append("Hindi")
languages.append("Persian")
languages.append("Turkish")
languages.append("Thai-Adap")
languages.append("Chinese")
languages.append("Polish")
languages.append("Indonesian")
languages.append("Wolof")

import random

for _ in range(200):
   language = random.choice(languages)
   model = ["REAL_REAL", "hillclimbing-auc", "hillclimbing-auc-fracdlm", "hillclimbing-auc-dlm", "hillclimbing-auc-onlydlm", "hillclimbing-auc-halfdlm", "hillclimbing-auc-twodlm", "RANDOM"]
   subprocess.call(["/u/nlp/anaconda/main/anaconda3/envs/py37-mhahn/bin/python", "yCreateRandomModels_"+language+"_OnlyWordForms_BoundedVocab.py", random.choice(model)])


