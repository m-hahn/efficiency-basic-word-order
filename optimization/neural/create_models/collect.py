import torch
import os
import sys
PATH = "/u/scr/mhahn/deps/neural-il-logs/"
files = sorted(os.listdir(PATH))

with open(f"output/{__file__}.tsv", "w") as outFile:
 print("\t".join([str(q) for q in ["language", "group", "modelID", "Order", "AUC", "I1"] + [f"Surp{i}" for i in range(20)]]), file=outFile)
 for f in files:
   with open(PATH+"/"+f, "r") as inFile:
     data = inFile.read().strip().split("\n")
     args = data[0].split(" ")
     surps_iter = data[1].split(" ")
     surps = data[2].split(" ")
     language = args[1]
     group, modelID = args[8].split("$")
     if len(surps_iter) > 1 and float(surps_iter[-1]) > float(surps_iter[-2]):
       surps_ = [float(x) for x in surps]
       for i in range(len(surps_)):
          surps_[i] = min(surps_[:i+1])
       AUC = 0
       surprisal = [surps_[0]]
       memory = [0]
       for j in range(1,len(surps_)):
         It = surps_[j-1] - surps_[j]
         surprisal.append(surps_[j])
         memory.append(memory[-1] + j * It)
       surprisal.append(surprisal[-1])
       memory.append(5)
       assert memory[-2] < 5
       for j in range(1,len(surprisal)):
          AUC += (memory[j] - memory[j-1]) * 0.5 * (surprisal[j] + surprisal[j-1])
       order = "NA"
       if "RANDOM" in modelID:
         if len(data) > 5:
           assert data[3] == "RUNNING", data[3]
           weights = {z[0] : z for z in [x.split("\t") for x in data[4:]]}
           DH_obj =     float(weights["obj"][2])
           DH_nsubj =   float(weights["nsubj"][2])
           Dist_obj =   float(weights["obj"][3])
           Dist_nsubj = float(weights["nsubj"][3])
           weight_obj = (1 if DH_obj < 0 else -1) * Dist_obj
           weight_nsubj = (1 if DH_nsubj < 0 else -1) * Dist_nsubj
           weights = {"S" : weight_nsubj, "O" : weight_obj, "V" : 0}
           order = "".join(sorted(["S","O","V"], key=lambda x:weights[x]))
           print(order)
       
       print("\t".join([str(q) for q in [language, group, modelID, order, AUC, surps_[0] - surps_[1]] + surps]), file=outFile)


