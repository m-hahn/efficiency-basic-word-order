import glob
# /u/scr/mhahn/deps/memory-need-neural-wordforms/search-Thai-Adap_yWithMorphologySequentialStreamDropoutDev_BaselineLanguage_Fast_SaveLast_NoFinePOS_OnlyWordForms_BoundedVocab.py_model_9955088_RANDOM_BY_TYPE.txt


import subprocess
import random

from math import exp
import sys

model = sys.argv[1]
if len(sys.argv) > 2:
  prescribedID = sys.argv[2]
else:
  prescribedID = None

language = 'Thai_2.8'

import os
if model.startswith("hillclimbing-auc"):
   models = [x[x.rfind("_")+1:-4] for x in os.listdir("/u/scr/mhahn/deps/"+model) if language in x]
#   models += [x[x.rfind("_")+1:-4] for x in os.listdir("/u/scr/mhahn/deps/"+model) if language.replace("2.8", "2.6") in x]
elif model.startswith("DLM_MEM"):
   models = [x[x.rfind("_")+1:-4] for x in os.listdir("/u/scr/mhahn/deps/"+model) if language in x]
#   models += [x[x.rfind("_")+1:-4] for x in os.listdir("/u/scr/mhahn/deps/"+model) if language.replace("2.8", "2.6") in x]
elif model == "REAL_REAL":
   models = ["REAL_REAL"]
elif model == "RANDOM":
   models = ["RANDOM"+str(random.randint(1000,100000)) for _ in range(20)]
else:
   assert False
random.shuffle(models)

dropout1 = 0.45
emb_dim = 150
lstm_dim = 128
layers = 1


learning_rate = 0.1
dropout2 = 0.0
batch_size = 2
sequence_length = 10
input_noising = 0.0


script = 'yWithMorphologySequentialStreamDropoutDev_BaselineLanguage_Fast_SaveLast_NoFinePOS_OnlyWordForms_BoundedVocab.py'
for modelID in models:
   if len(glob.glob(f"/u/scr/mhahn/deps/neural-il-logs/*{language}*{modelID}.txt")) > 0:
     continue
   if "RANDOM" in model and len(glob.glob(f"/u/scr/mhahn/deps/neural-il-logs/*{language}*RANDOM*.txt")) > 30:
     break
   command = ['./python27', script, language, language, dropout1, emb_dim, lstm_dim, layers, learning_rate, model+"$"+modelID, dropout2, batch_size,input_noising,  sequence_length]
   if prescribedID is not None:
     command.append(prescribedID)
   command = map(str,command)
   subprocess.call(command)
   if prescribedID is not None:
      break
quit()
