import glob
files = sorted(glob.glob("output/forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVAL_ForNeural.py*tsv"))

with open("output/SUMMARY_forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVAL_ForNeural.py.tsv", "w") as outFile:
 for f in files:
  with open(f, "r") as inFile:
   for line in inFile:
    if len(line) > 2:
     print(line.strip(), file=outFile)
