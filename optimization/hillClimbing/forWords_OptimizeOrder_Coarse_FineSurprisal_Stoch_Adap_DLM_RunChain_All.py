import glob
import os
import subprocess
import time
script = "forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RunChain.py"

groups = []
groups.append("hillclimbing-auc")
groups.append("hillclimbing-auc-dlm")
groups.append("hillclimbing-auc-fracdlm")
groups.append("hillclimbing-auc-halfdlm")
groups.append("hillclimbing-auc-onlydlm")
groups.append("hillclimbing-auc-twodlm")
groups += ["DLM_MEMORY_OPTIMIZED/locality_optimized_dlm/manual_output_funchead_fine_depl_nopos"]
groups.append("RANDOM")
groups.append("RANDOM")
groups.append("RANDOM")
groups.append("RANDOM")
import random

instance = random.randint(10000, 10000000)

from ud_languages_28 import languages

for iteration in range(100):
  group = random.choice(groups)
  language = random.choice(languages)
  if group != "RANDOM":
     files = glob.glob("/u/scr/mhahn/deps/"+group+"/*"+language+"*") + glob.glob("/u/scr/mhahn/deps/"+group+"/*"+language.replace("2.8", "2.6")+"*")
     if len(files) == 0:
       continue
     f = random.choice(files)
     id_ = f[f.rfind("_")+1:-4]
  else:
     id_ = str(-1)
     f = "None"
  print(f, language, id_)
  #quit()
  subprocess.call(["/u/nlp/anaconda/ubuntu_16/envs/py27-mhahn/bin/python2.7", script, "--language="+language, "--group="+group, "--model="+id_, "--instance="+str(instance)])
  #quit()
