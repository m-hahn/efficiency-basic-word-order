import os
import subprocess
script = "forWords_Finnish_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVALRandom.py"

import random


languages = []
#languages.append("Japanese-GSD_2.8SUBSET1000")
#languages.append("Japanese-GSD_2.8SUBSET2000")
#languages.append("Japanese-GSD_2.8SUBSET5000")
#languages.append("Japanese-GSD_2.8SUBSET10000")
#languages.append("English_2.8SUBSET5000")
#languages.append("English_2.8SUBSET2000")
#languages.append("English_2.8SUBSET1000")
#languages.append("English_2.8SUBSET10000")
#languages.append("Indonesian_2.8SUBSET5000")
#languages.append("Indonesian_2.8SUBSET2000")
#languages.append("Indonesian_2.8SUBSET1000")
#languages.append("Indonesian_2.8SUBSET10000")
#languages.append("Turkish_2.8SUBSET5000")
#languages.append("Turkish_2.8SUBSET1000")
#languages.append("Turkish_2.8SUBSET2000")
#languages.append("Turkish_2.8SUBSET10000")
languages.append("Korean_2.8SUBSET1000")
languages.append("Basque_2.8SUBSET1000")
languages.append("Akkadian_2.8SUBSET1000")
languages.append("Kazakh_2.8SUBSET1000")
languages.append("Persian_2.8SUBSET1000")
languages.append("Hindi_2.8SUBSET1000")
languages.append("Naija_2.8SUBSET1000")
languages.append("Coptic_2.8SUBSET1000")
languages.append("French_2.8SUBSET1000")
languages.append("Swedish_2.8SUBSET1000")
languages.append("Vietnamese_2.8SUBSET1000")
languages.append("Thai_2.8SUBSET1000")
languages.append("Korean_2.8SUBSET5000")
languages.append("Basque_2.8SUBSET5000")
languages.append("Akkadian_2.8SUBSET5000")
languages.append("Kazakh_2.8SUBSET5000")
languages.append("Persian_2.8SUBSET5000")
languages.append("Hindi_2.8SUBSET5000")
languages.append("Naija_2.8SUBSET5000")
languages.append("Coptic_2.8SUBSET5000")
languages.append("French_2.8SUBSET5000")
languages.append("Swedish_2.8SUBSET5000")
languages.append("Vietnamese_2.8SUBSET5000")
languages.append("Thai_2.8SUBSET5000")



languages.append("Korean_2.8SUBSET2000")
languages.append("Basque_2.8SUBSET2000")
languages.append("Akkadian_2.8SUBSET2000")
languages.append("Kazakh_2.8SUBSET2000")
languages.append("Persian_2.8SUBSET2000")
languages.append("Hindi_2.8SUBSET2000")
languages.append("Naija_2.8SUBSET2000")
languages.append("Coptic_2.8SUBSET2000")
languages.append("French_2.8SUBSET2000")
languages.append("Swedish_2.8SUBSET2000")
languages.append("Vietnamese_2.8SUBSET2000")
languages.append("Thai_2.8SUBSET2000")




instance = random.randint(10000, 10000000)

random.shuffle(languages)
for _ in range(100):
     language = random.choice(languages)
     subprocess.call(["/u/nlp/anaconda/ubuntu_16/envs/py27-mhahn/bin/python2.7", script, "--language="+language, "--instance="+str(instance)])

