import random
import sys
import subprocess
from random import choice

languages = []
languages.append("Japanese-GSD_2.8SUBSET1000")
languages.append("Japanese-GSD_2.8SUBSET2000")
languages.append("Japanese-GSD_2.8SUBSET5000")
languages.append("Japanese-GSD_2.8SUBSET10000")
languages.append("English_2.8SUBSET5000")
languages.append("English_2.8SUBSET2000")
languages.append("English_2.8SUBSET1000")
languages.append("English_2.8SUBSET10000")
languages.append("Indonesian_2.8SUBSET5000")
languages.append("Indonesian_2.8SUBSET2000")
languages.append("Indonesian_2.8SUBSET1000")
languages.append("Indonesian_2.8SUBSET10000")
languages.append("Turkish_2.8SUBSET5000")
languages.append("Turkish_2.8SUBSET1000")
languages.append("Turkish_2.8SUBSET2000")
languages.append("Turkish_2.8SUBSET10000")
languages.append("Korean_2.8SUBSET1000")
languages.append("Basque_2.8SUBSET1000")
languages.append("Akkadian_2.8SUBSET1000")
languages.append("Kazakh_2.8SUBSET1000")
languages.append("Persian_2.8SUBSET1000")
languages.append("Hindi_2.8SUBSET1000")
languages.append("Naija_2.8SUBSET1000")
languages.append("Coptic_2.8SUBSET1000")
languages.append("French_2.8SUBSET1000")
languages.append("Swedish_2.8SUBSET1000")
languages.append("Vietnamese_2.8SUBSET1000")
languages.append("Thai_2.8SUBSET1000")
languages.append("Korean_2.8SUBSET5000")
languages.append("Basque_2.8SUBSET5000")
languages.append("Akkadian_2.8SUBSET5000")
languages.append("Kazakh_2.8SUBSET5000")
languages.append("Persian_2.8SUBSET5000")
languages.append("Hindi_2.8SUBSET5000")
languages.append("Naija_2.8SUBSET5000")
languages.append("Coptic_2.8SUBSET5000")
languages.append("French_2.8SUBSET5000")
languages.append("Swedish_2.8SUBSET5000")
languages.append("Vietnamese_2.8SUBSET5000")
languages.append("Thai_2.8SUBSET5000")



languages.append("Korean_2.8SUBSET2000")
languages.append("Basque_2.8SUBSET2000")
languages.append("Akkadian_2.8SUBSET2000")
languages.append("Kazakh_2.8SUBSET2000")
languages.append("Persian_2.8SUBSET2000")
languages.append("Hindi_2.8SUBSET2000")
languages.append("Naija_2.8SUBSET2000")
languages.append("Coptic_2.8SUBSET2000")
languages.append("French_2.8SUBSET2000")
languages.append("Swedish_2.8SUBSET2000")
languages.append("Vietnamese_2.8SUBSET2000")
languages.append("Thai_2.8SUBSET2000")




if len(sys.argv) > 1:
   N = int(sys.argv[1])
else:
   N =1000
import glob
BASE = "hillclimbing-auc-subset"
script = "forWords_Finnish_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap.py"
for language in languages:
   print(language, "from 2.6 and 2.8:", len(glob.glob(f"/u/scr/mhahn/deps/{BASE}/optimized_{language}_{script}*tsv")) + len(glob.glob(f"/u/scr/mhahn/deps/{BASE}/optimized_{language.replace('2.8', '2.6')}_{script}*tsv")))
for _ in range(N):
   random.shuffle(languages)
   found = False
   for language in languages:
     print("Considering", language, "from 2.6 and 2.8:", len(glob.glob(f"/u/scr/mhahn/deps/{BASE}/optimized_{language}_{script}*tsv")) + len(glob.glob(f"/u/scr/mhahn/deps/{BASE}/optimized_{language.replace('2.8', '2.6')}_{script}*tsv")))
     if len(set(glob.glob(f"/u/scr/mhahn/deps/{BASE}/optimized_{language}_{script}*tsv") + glob.glob(f"/u/scr/mhahn/deps/{BASE}/optimized_{language.replace('2.8', '2.6')}_{script}*tsv"))) < 40:
         found = True
         break
   if not found:
     break
   subprocess.call(["/u/nlp/anaconda/ubuntu_16/envs/py27-mhahn/bin/python2.7", script, "--language="+language])
