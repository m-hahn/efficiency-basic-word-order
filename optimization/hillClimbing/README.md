# Hill Climbing Grammar Optimization


# Core Functionality

## Optimization
* Only IL: [One run](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap.py), [running a batch](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_All.py), [running a batch](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_All_28.py)
* Only DL: [One run](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_OnlyDLM.py),  [running a batch](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_OnlyDLM_All_28.py)
* Linear combinations: [One run](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_PlusFracDLM.py), [running a batch](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_PlusFracDLM_All_28.py)

## Evaluation
* [Evaluate optimized grammar](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVAL.py), [Run batch](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVAL_All2.py)
* [Collect results](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVAL_Collect.py)


## Evaluate baseline grammars
* [Create and evaluate grammars](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVALRandom.py), [run batch](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVALRandom_All.py)
* [Collect results](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVALRandom_Collect.py)

## Evaluate real orderings
* [run on one language](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVALReal.py), [running a batch](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVALReal_All.py)


# Auxiliary Scripts

## Estimating Information Locality

* [Heldout estimator](estimateTradeoffHeldout.py)
* [Naive estimator (not used)](estimateTradeoffInSample.py)


## Loading corpus data

* Python scripts for accessing corpora: [UD 2.8 (reassigning train/heldout split)](corpusIterator_Adap_V.py), [UD 2.8 (base)](corpusIterator_V.py) 
* [List of UD languages](ud_languages_2.8.txt), [Python wrapper](ud_languages_28.py), [Additional historical languages](ud_languages_2.8_potentialExtra.txt)
* Scripts for other datasets: [Ancient Greek](accessAncientGreekData.py) ([Authors](perseus-authors.txt), [Documents](perseus-docs.txt)),  [Icelandic](accessIcelandicData.py), [ISWOC](accessISWOCData.py), [Switchboard](accessSWBD.py), [TuebaJS](accessTuebaJS.py)


# Workflow Scripts
These scrips collect results from individual runs into several TSV tables. Note that, while the outputs of the individual optimization runs are stored outside of the repository, their content is transferred into the TSV tables created by the workflow scripts, which are part of the repository.

* [Shell script for collecting all results](runReadGrammars.sh)

Below are individual Python scripts that perform parts of the tasks:

* [Collect weights for grammars](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RecordAll.py), [run batch](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RecordAll_All.py)
* [Record basic word order](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RecordBasic.py), [run batch](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RecordBasic_All.py)
* [Extract Greenberg correlations](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RecordGreenberg.py), [run batch](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RecordGreenberg_All.py)
* Read and collect optimized grammars: [Linear combinations](readGrammars_AUC_FracDLM.py),  [Only DL](readGrammars_AUC_OnlyDLM.py),  [Only IL](readGrammars.py)
* Make lists of optimized grammars: [Linear combinations](readGrammars_AUC_FracDLMLog.py),  [Only DL](readGrammars_AUC_OnlyDLMLog.py),  [Only IL](readGrammarsLog.py)

# Other Studies

* [Spoken Data (SI Section S26)](spoken)
* [Subsets of varying sizes (SI Section S27)](subsets)
* Interface with neural-network control study (SI Section S23) [Evaluate grammar previously evaluated using neural-network-based estimation method](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVAL_ForNeural.py), [run batch](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVAL_ForNeual_All2.py), [Collect results](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVAL_ForNeural_Collect.py)
* Run mutation chains (SI Section S9): [Run multiple chains](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RunChain_Multiple.py), [run batch](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RunChain_Multiple_All.py). [Run a single chain](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RunChain.py), [run batch](forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RunChain_All.py).
* Fit ordering (SI Section S28): [Run for one language](forWords_OptimizeOrder_Coarse_FitOrder.py), [run batch](forWords_OptimizeOrder_Coarse_FitOrder_All.py)

# Bookkeeping (correcting errors in an earlier version)
Not needed for replication.
* [DLM](removeOldTurkishDLM.py), [Random](removeOldTurkishRandom.py)

# Further analyses



