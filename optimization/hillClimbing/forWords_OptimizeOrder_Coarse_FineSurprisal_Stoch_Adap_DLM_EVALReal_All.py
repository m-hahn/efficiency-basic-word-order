import os
import subprocess
script = "forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVALReal.py"

from ud_languages_28 import languages
import random
languages += ["German-GSD_2.8", "Czech-PDT_2.8", "Japanese-GSD_2.8"]
random.shuffle(languages)
languages = [x for x in languages if "2.8" not in x]
for language in languages:
     subprocess.call(["/u/nlp/anaconda/ubuntu_16/envs/py27-mhahn/bin/python2.7", script, "--language="+language])

