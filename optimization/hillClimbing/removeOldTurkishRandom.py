import glob
import sys
files = glob.glob("output/forWords*Random*tsv")
print(files)
for f in files:
  with open(f, "r") as inFile:
     data = inFile.read().split("\n")
  with open(f, "w") as outFile:
    print("\n".join([x for x in data if (not (x.startswith("Turkish")))]), file=outFile)
 
