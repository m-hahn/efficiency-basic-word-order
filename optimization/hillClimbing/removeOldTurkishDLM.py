import glob
import sys
files = glob.glob("output/forWords*EVAL.py*.tsv")
print(files)
for f in files:
  with open(f, "r") as inFile:
     data = inFile.read().split("\n")
  with open(f, "w") as outFile:
    print("\n".join([x for x in data if (("9.18461322" in x) or (not (x.startswith("Turkish"))))]), file=outFile)
 
