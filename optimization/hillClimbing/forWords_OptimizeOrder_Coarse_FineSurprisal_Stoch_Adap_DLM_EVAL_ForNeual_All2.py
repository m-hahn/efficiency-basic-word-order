import glob
import os
import subprocess
import time
script = "forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVAL_ForNeural.py"

groups = ["neural-il-logs"]
import random

instance = random.randint(10000, 10000000)

for group in groups:
  files = os.listdir("/u/scr/mhahn/deps/"+group)
  random.shuffle(files)
  for f in files:
     print(f)
     resultsPath = f"/u/scr/mhahn/deps/{group}/{f}"
     print(f)
     f = f.replace("estimates-", "")
     if "RANDOM" not in f:
        continue
     print(f)
     language = f[:f.index("_yWith")]
     model = f[f.rfind("_")+1:-4]
     print(model)
     relevantLogs = glob.glob("output/forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_EVAL_ForNeural.py*tsv")
     found = False
     print("Looking for", model)
#     print("LOGs", relevantLogs)
     for log in relevantLogs:
        with open(log, "r") as inFile:
            for line in inFile:
              line = line.strip().split("\t")
             # print("Model", line[0], line[2], "looking for", model)
              if (line[2]) == (model) and line[0] == language:
                found = True
                print("Found", model)
                break
        if found:
             break
     if found:
       continue
     print("not found", model, language)
     #continue
     print(f, language, model)
     #quit()
     subprocess.call(["/u/nlp/anaconda/ubuntu_16/envs/py27-mhahn/bin/python2.7", script, "--language="+language, "--group="+group, "--model="+model, "--instance="+str(instance)])
     #quit()
