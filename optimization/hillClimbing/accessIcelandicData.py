import os
import random
#import accessISWOCData
#import accessTOROTData
import sys

header = ["index", "word", "lemma", "posUni", "posFine", "morph", "head", "dep", "_", "_"]


def readUDCorpus(language, partition, ignoreCorporaWithoutWords=True):
      assert partition == "together"
      l = language.split("_")
      language = "_".join(l[:-1])
      version = l[-1]
      #print(l, language)
      basePath = "/u/scr/corpora/Universal_Dependencies/Universal_Dependencies_"+version+"/ud-treebanks-v"+version+"/"
      files = os.listdir(basePath)
      files = list(filter(lambda x:x.startswith("UD_"+language.replace("-Adap", "")), files))
      print >> sys.stderr, ("FILES", files)
      data = []
      for name in files:
        if "Sign" in name:
           print >> sys.stderr, ("Skipping "+name)
           continue
        assert ("Sign" not in name)
        if "Chinese-CFL" in name or "English-ESL" in name or "Hindi_English" in name or "French-FQB" in name or "Latin-ITTB" in name or "Latin-LLCT" in name:
           print >> sys.stderr, ("Skipping "+name)
           continue
        suffix = name[len("UD_"+language):]
        if name == "UD_French-FTB":
            subDirectory = "/u/scr/mhahn/corpus-temp/UD_French-FTB/"
        else:
            subDirectory =basePath+"/"+name
        subDirFiles = os.listdir(subDirectory)
        partitionHere = partition
            
        candidates = list(filter(lambda x:"-ud-" in x and x.endswith(".conllu"), subDirFiles))
        print >> sys.stderr, ("SUBDIR FILES", subDirFiles)

        print >> sys.stderr, candidates
        assert len(candidates) >= 1, candidates
        for cand in candidates:
           try:
              dataPath = subDirectory+"/"+cand
              with open(dataPath, "r") as inFile:
                 newData = inFile.read().strip().split("\n\n")
                 assert len(newData) > 1
                 data = data + newData
           except IOError:
              print >> sys.stderr, ("Did not find "+dataPath)

      assert len(data) > 0, (language, partition, files)


      print >> sys.stderr, ("Read "+str(len(data))+ " sentences from "+str(len(files))+" "+partition+" datasets. "+str(files)+"   "+basePath)
      return data

def readCorpus(language, partition):
      if partition != "train":
        return []
      epoch = language.replace("PARTITION-Icelandic_", "")
      years = {"early" : (1000, 1600), "middle" : (1600, 1900), "modern" : (1900, 2021)}[epoch]
      print >> sys.stderr, ("LANGUAGE", language)
      data = readUDCorpus("Icelandic-IcePaHC_2.8", "together")
      if epoch == "modern":
        data += readUDCorpus("Icelandic-Modern_2.8", "together")
        data += readUDCorpus("Icelandic-PUD_2.8", "together")
      random.Random(5).shuffle(data)

      result_ = []
      for sentence in data:
        original = sentence
        sentence = list(map(lambda x:x.split("\t"), sentence.split("\n")))
        result = []
        metadata = {}
        for i in range(len(sentence)):
           if sentence[i][0].startswith("#"):
              if " = " in sentence[i][0]:
                posEq = sentence[i][0].index(" = ")
                key = sentence[i][0][2:posEq]
                value = sentence[i][0][posEq+3:]
                metadata[key] = value
              continue
           if "-" in sentence[i][0]: # if it is NUM-NUM
              continue
           if "." in sentence[i][0]:
              continue
           sentence[i] = dict([(y, sentence[i][x]) for x, y in enumerate(header)])
           sentence[i]["head"] = int(sentence[i]["head"])
           sentence[i]["index"] = int(sentence[i]["index"])
           sentence[i]["word"] = sentence[i]["word"].lower()

           sentence[i]["dep"] = sentence[i]["dep"].lower()

           sentence[i]["coarse_dep"] = sentence[i]["dep"].split(":")[0]



           result.append(sentence[i])
 #          print sentence[i]
        if "sent_id" in metadata:
          text_id = metadata["sent_id"]
          try:
             year = int(text_id.split(".")[0])
          except ValueError:
#              print(metadata)
              year = 2000
        else:
           assert False
#        print(year, self.years, year >= self.years[0] and year <= self.years[1])
        if year >= years[0] and year <= years[1]:
           result_.append(original)
      return result_

