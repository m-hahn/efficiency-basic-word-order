# SGD Optimization (for DL only)

# Scripts for accessing corpora

* [Python script for accessing list of languages](ud_languages_28.py)
* [List of extra historical datasets](ud_languages_2.8_potentialExtra.txt)
* [List of UD languages](ud_languages_2.8.txt)
* [Ancient Greek](accessAncientGreekData.py), [Authors](perseus-authors.txt), [Documents](perseus-docs.txt)
* [Icelandic](accessIcelandicData.py)
* [ISWOC](accessISWOCData.py)
* [Switchboard](accessSWBD.py)
* [TuebaJS](accessTuebaJS.py)
* [UD 2.8 (reassigning train/heldout split)](corpusIterator_Adap_V.py), [UD 2.8 (base)](corpusIterator_V.py)


# Bookkeeping
* [Collect results](collectResults_Efficiency_2.8_NoPOS.py)

# Optimization
* [Optimize (all languages)](optimizeDependencyLength_NoPOS_AllLanguages_2.8.py)
* [Optimize](optimizeDependencyLength_NoPOS_NoSplit.py)

# Further control studies
* [Effect of corpus size](subset)


