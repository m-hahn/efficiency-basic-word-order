#python3 process9_frontier18_Neural_AUC_Viz.py Arabic_2.8
#python3 process9_frontier18_Neural_AUC_Viz.py Basque_2.8
#python3 process9_frontier18_Neural_AUC_Viz.py Chinese_2.8
#python3 process9_frontier18_Neural_AUC_Viz.py English_2.8
#python3 process9_frontier18_Neural_AUC_Viz.py Finnish_2.8
#python3 process9_frontier18_Neural_AUC_Viz.py Hindi_2.8
#python3 process9_frontier18_Neural_AUC_Viz.py Indonesian_2.8
#python3 process9_frontier18_Neural_AUC_Viz.py Persian_2.8
#python3 process9_frontier18_Neural_AUC_Viz.py Polish_2.8
#python3 process9_frontier18_Neural_AUC_Viz.py Thai_2.8
#python3 process9_frontier18_Neural_AUC_Viz.py Turkish_2.8
#python3 process9_frontier18_Neural_AUC_Viz.py Wolof_2.8

import math
import random
import torch
import sys

language = sys.argv[1] #"Korean_2.8"
languageCorpus = language
if languageCorpus == "Japanese_2.8":
   languageCorpus = "Japanese-GSD_2.8"
elif languageCorpus == "Czech_2.8":
   languageCorpus = "Czech-PDT_2.8"
elif languageCorpus == "German_2.8":
   languageCorpus = "German-GSD_2.8"



with open("../landscapes_noPOS_2.8_new.R.tsv", "r") as inFile:
    header = next(inFile).replace('"', "").strip().split(" ")
    header = dict(list(zip(header, range(len(header)))))
    data = [x.replace('"', "").split(" ")[1:] for x in inFile.read().strip().split("\n")]
realCongruences = {line[header["Language"]] : float(line[header["Congruence_All"]]) for line in data}
if language not in realCongruences:
    print("REAL CONGRUENCE NOT FOUND", language)
realCongruence = realCongruences.get(language, 0)
print("CONGRUENCE", realCongruence)




with open("../raw_output/real_neural.tsv", "r") as inFile:
    header = next(inFile).strip().split("\t")
    header = dict(list(zip(header, range(len(header)))))
    data = [x.split("\t") for x in inFile.read().strip().split("\n")]
data = [x for x in data if x[header["language"]] == languageCorpus]
deplenReal = float(data[0][header["DepLen"]])
surprisalReal = float(data[0][header["AUC"]])


with open("../raw_output/landscape_neural.tsv", "r") as inFile:
    header = next(inFile).strip().split("\t")
    header = dict(list(zip(header, range(len(header)))))
    data = [x.split("\t") for x in inFile.read().strip().split("\n")]




data = [x for x in data if x[header["language"]] == languageCorpus and x[header["basic"]] not in ["", "NA"]]


surprisal = torch.FloatTensor([float(x[header["AUC"]]) for x in data])
deplen = torch.FloatTensor([float(x[header["DepLen"]]) for x in data])


with open("../raw_output/baselines_neural.tsv", "r") as inFile:
    header_baselines = next(inFile).strip().split("\t")
    header_baselines = dict(list(zip(header_baselines, range(len(header_baselines)))))
    baselines = [x.split("\t") for x in inFile.read().strip().split("\n")]
baselines = [x for x in baselines if x[header_baselines["language"]] == languageCorpus and x[header_baselines["Order"]] not in ["", "NA"]]
surprisal_baselines = torch.FloatTensor([float(x[header_baselines["AUC"]]) for x in baselines])
deplen_baselines = torch.FloatTensor([float(x[header_baselines["DepLen"]]) for x in baselines])

surprisal = torch.cat([surprisal, surprisal_baselines], dim=0)
deplen = torch.cat([deplen, deplen_baselines], dim=0)


#print(surprisal)

surprisalRange = (surprisal.min(), surprisal.max())
#print(surprisalRange)
deplenRange = (deplen.min(), deplen.max())
#print(deplenRange)

deplenReal -= deplenRange[0]
deplenReal /= deplenRange[1] - deplenRange[0]
surprisalReal -= surprisalRange[0]
surprisalReal /= surprisalRange[1] - surprisalRange[0]

assert deplenReal <= 2, deplenReal
assert surprisalReal <= 2, surprisalReal

#print("S", surprisal_baselines.mean())
#print("D", deplen_baselines.mean())
#print("S", surprisal.mean())
#print("D", deplen.mean())

surprisal_baselines = surprisal_baselines - surprisalRange[0]
surprisal_baselines /= (surprisalRange[1] - surprisalRange[0])
deplen_baselines = deplen_baselines - deplenRange[0]
deplen_baselines /= (deplenRange[1] - deplenRange[0])

#print("S", surprisal_baselines.mean(), "---", surprisal.max())
#print("D", deplen_baselines.mean())
#quit()

surprisal = surprisal - surprisalRange[0]
surprisal /= surprisal.max()
deplen = deplen - deplenRange[0]
deplen /= deplen.max()

#print("S", surprisal.mean())
#print("D", deplen.mean())
#quit()
raw_surprisalRange = surprisalRange
raw_deplenRange = deplenRange


surprisalRange = (0,1)
deplenRange = (0,1)



order = [x[header["basic"]] for x in data]+[x[header_baselines["Order"]] for x in baselines]
order = torch.FloatTensor([{"SOV" : 1, "SVO" : 0, "OSV" : 1, "OVS" : 0, "VSO" : 1, "VOS" : 1}[x] for x in order])



#print(surprisal.size())


def loo_smoothed(k_x, k_y):
    d_x = (surprisal.unsqueeze(1) - surprisal.unsqueeze(0)).pow(2)
    d_y = (deplen.unsqueeze(1) - deplen.unsqueeze(0)).pow(2)
    logit = (k_x * d_x + k_y * d_y)
 #   print(logit)
#    print(1e10 * torch.eye(surprisal.size()[0]))
    logit -= 1e10 * torch.eye(surprisal.size()[0])
    kernel = torch.exp(logit) / torch.exp(logit).sum(dim=1).unsqueeze(1)
    smoothed = (kernel * order.unsqueeze(0)).sum(dim=1)
    return smoothed

def optimize():
  # Initialization
  bestLoss = 1e100
  bestK = (0,0)
  for iteration in range(5000):
      k_x = -100*random.random()
      k_y = -100*random.random()
      prediction = loo_smoothed(k_x, k_y)
      loss = (prediction - order).pow(2).mean() + 0.00001 * (k_x**2 + k_y**2)
      loss = float(loss)
      if iteration % 500 == 0:
         print("Randomly exploring grid", iteration, bestLoss, bestK,"testing currently", loss, k_x, k_y, languageCorpus)
      if loss < bestLoss:
          bestK = (k_x, k_y)
          bestLoss = loss
  k_x = torch.FloatTensor([bestK[0]])
  k_y = torch.FloatTensor([bestK[1]])
  k_x.requires_grad = True
  k_y.requires_grad = True
  
  optim = torch.optim.Adam([k_x, k_y], lr=0.001)
  
  lastLoss = 100
  for iteration in range(100): #100000):
    prediction = loo_smoothed(k_x, k_y)
    loss = (prediction - order).pow(2).mean() + 0.00001 * (k_x.pow(2) + k_y.pow(2))
    if iteration % 500 == 0:
      print(iteration, "LOSS", loss, k_x, k_y, languageCorpus)
    if iteration % 2000 == 0:
        if lastLoss - float(loss) < 0.0001:
            break
        lastLoss = float(loss)
    loss.backward()
    optim.step()
    optim.zero_grad()
  return float(k_x), float(k_y)

W = 50
grid = ([[[-0.05 + i/W * 1.1  ,-0.05 + j/W *  1.1] for j in range(W)] for i in range(W)])

N = surprisal.size()[0]
dominated = torch.ByteTensor(N)

for i in range(N):
   better = torch.logical_and((surprisal < surprisal[i]), (deplen < deplen[i])).any()
   dominated[i] = better


k_x, k_y = optimize() #-90.0241
#k_x, k_y = -4.28, -21.7
#k_y = -1.5454


def smoothed(x, y):
    d_x = (surprisal - x).pow(2)
    d_y = (deplen - y).pow(2)
    logit = (k_x * d_x + k_y * d_y)
#    print(k_x, k_y, d_x, d_y)
 #   print("Logit", logit)
    kernel = torch.exp(logit) / torch.exp(logit).sum().unsqueeze(0)
  #  print("Kernel", kernel)
    smoothed = (kernel * order).sum(dim=0)
#    assert float(smoothed) <= 1, smoothed
    return smoothed

#print( "Order", order)
print("Smoothed Real", smoothed(surprisalReal, deplenReal), languageCorpus)
#quit()


#print(smoothed(4.6, 2.6))

#print("PREDICTIVE FOR KNOWN POINTS")
#print(loo_smoothed(k_x, k_y))

#print(torch.FloatTensor(grid))




#print("SMOOTHED GRID")
#print(smoothed_grid)

import numpy as np
import matplotlib.pyplot as plt

#X = smoothed_grid


#ax.imshow(X) #, interpolation='nearest')
#print(surprisal.size(), deplen.size(), order.size())
grid_ = torch.stack([surprisal, deplen], dim=1)
dominated_ = dominated.view(-1)
grid_ = grid_[torch.logical_not(dominated_)]


frontier_ = grid_
grid_frontier_ = grid_ #torch.FloatTensor(grid).view(-1,2)[frontier_]


# Need better smoothing to get bijection between lambda and frontier

sortedFrontier = sorted(range(grid_frontier_.size()[0]), key=lambda x:tuple((lambda w: (-w[0], w[1]))([float(q) for q in grid_frontier_[x]])))
#sortedFrontier = sorted(range(grid_frontier_.size()[0]), key=lambda x:tuple([float(q) for q in grid_frontier_[x]]))
sorted_frontier = grid_frontier_[sortedFrontier]
#print(sortedFrontier)
#print(sorted_frontier)
sorted_frontier_all = sorted_frontier
sorted_frontier = [sorted_frontier_all[0]]
for i in range(1, sorted_frontier_all.size()[0]):
    if sorted_frontier_all[i][0] == sorted_frontier[-1][0]:
        continue
    if sorted_frontier_all[i][1] == sorted_frontier[-1][1]:
        continue
    sorted_frontier.append(sorted_frontier_all[i])
sorted_frontier = torch.stack(sorted_frontier)

corners=sorted_frontier

## Collecting corners: non-dominated points on the convex hull
#corners = [[float(q) for q in sorted_frontier[0]]]
#
#for i in range(1,sorted_frontier.size()[0]-1):
#    valueHere = sorted_frontier[i]
#
#
#    valuePrevious = sorted_frontier[i-1]
#    valueNext = sorted_frontier[i+1]
##    print(i, valueHere, "previous", valuePrevious,"next", valueNext)
#    assert valuePrevious[0] >= valueHere[0]
#    assert valuePrevious[1] <= valueHere[1]
#    assert valueHere[0] >= valueNext[0]
#    assert valueHere[1] <= valueNext[1]
#    a, b = [float(q) for q in valueHere]
#    a1, b1 = [float(q) for q in valuePrevious]
#    a2, b2 = [float(q) for q in valueNext]
##    if abs(a-corners[-1][0]) < 0.01 or abs(b-corners[-1][1]) < 0.01:
##        print("Very close to previous corner. Skipping")
##        continue
##    if b1 == b or b2 == b:
# #       continue
#    assert a <= a1
#    assert a2 <= a
#    assert b >= b1
#    assert b2 >= b
#    lambda_1 = (sorted_frontier[:,0]-a)/(b-sorted_frontier[:,1])
#    bounds = (lambda_1)
#    sign = (b-sorted_frontier[:,1]<0)
#    lower = bounds[sign]
#    upper = bounds[torch.logical_not(sign)]
#    lower = lower[lower.isnan().logical_not()].max()
#    upper = upper[upper.isnan().logical_not()].min()
##    if lower >= upper-1e-3:
# #       continue
#    #print("...")
#    print("Point on curve", a, b, "lambda in range:", lower, upper, language)
#
#    corners.append([a,b])
#    #assert (a+lower*b <= (sorted_frontier[:,0] + lower*sorted_frontier[:,1]).min()+1e-5)
#    #assert (a+upper*b <= (sorted_frontier[:,0] + upper*sorted_frontier[:,1]).min()+1e-5)
##    quit()
# #   lambda_2 = (a2-a)/(b-b2)
#  #  print(i, "\t", a, "\t", b, "\t", lambda_1, "\t", lambda_2, "\t", "Trade-off", a+lambda_1 *b, "at previous", a1 + lambda_1 *b1)
#
#   # assert a+lambda_1 *b <= a1 + lambda_1 *b1+1e-5
#    #assert a+lambda_2 *b <= a2 + lambda_2 *b2+1e-5, (a+lambda_2 *b , a2 + lambda_2 *b2)
#
####################
## other option: 
## - look at frontier among the raw points
## - smooth the frontier between the points (spline)
#
#corners.append([float(q) for q in sorted_frontier[-1]])

#print("CORNERS")
#print(corners)

#if abs(corners[-2][0]-corners[-1][0]) < 0.01 or abs(corners[-2][1]-corners[-1][1]) < 0.01:
#    print("Very close to previous corner. Deleting last corner")
#    del corners[-1]

#i=1
#while i < len(corners)-1: #for i in range(1,len(corners)-1):
#    a, b = corners[i]
#    a1, b1 = corners[i-1]
#    a2, b2 = corners[i+1]
#    slopeLast = (b-b2)/(a-a2)
#    slopeNext = (b1-b)/(a1-a)
#    print("Corner on curve", a, b, "lambda in range:", lower, upper, language, slopeLast, slopeNext, slopeLast/slopeNext)
#    if abs(slopeNext-slopeLast) < 0.2:
#        print("Deleting next corner")
#        del corners[i+1]
#    elif (slopeLast/slopeNext) < 1.5:
#        print("Deleting next corner")
#        del corners[i+1]
#    i+=1
#

#quit()
#corners = torch.FloatTensor(corners)
#print("Corners", corners)

import numpy as np

import matplotlib.pyplot as plt

from scipy import interpolate

#print("Corners")
#print([float(x[0]) for x in corners])
#print([float(x[1]) for x in corners])

#tck = interpolate.splrep([float(x[0]) for x in corners][::-1], [float(x[1]) for x in corners][::-1], k=2)


xnew = np.arange(corners[-1][0], corners[0][0], (corners[0][0]-corners[-1][0])/50)
xnew = np.array([])
for i in range(len(corners)-1, 0, -1):
    #print(xnew)
    #print((np.arange(corners[i][0], corners[i-1][0], (corners[i-1][0]-corners[i][0])/10)))
    xnew = np.concatenate([xnew, (np.arange(corners[i][0], corners[i-1][0], (corners[i-1][0]-corners[i][0])/10))], axis=0)
#xnew = xnew[::-1]

#ynew = interpolate.splev(xnew, tck, der=0)


# (sort-of a) cubic spline AX^2 + BX^2 + CX + D constrained to be convex
# Within a segment, we're taking the interval to be [0, deltaX] so that D=0
# Somewhat idiosyncratically, deltaX is negative throughout (so that deltaY is positive), so the interval is really [deltaX, 0]
# Constraints:
# f(deltaX) = deltaY (continuity of f)
# f'_{t}(deltaX) = f'_{t+1}(0) (continuity of f')
# Convexity: the second derivative (which is linear, 6AX+2B) is nonnegative at both 0 and deltaX (and thus in between)
# Objective: the integral of the second derivative (which is positive) -- that is, minimize the slopoe difference between start and end  point

import scipy.optimize

Q = corners.size()[0]-1
# variables: a, b, c

deltaX = corners[1:,0] - corners[:-1,0]
deltaY = corners[1:,1] - corners[:-1,1]
assert float(deltaX.max()) <= 0,deltaX
assert float(deltaY.min()) >= 0,deltaY


#Q = 2
#
#deltaX = torch.FloatTensor([-1, -1])
#deltaY = torch.FloatTensor([1, 1])


objective = torch.zeros(4,Q)

for i in range(Q):
  # Objective: the AUC
  objective[0,i] = deltaX[i].pow(4)/4
  objective[1,i] = deltaX[i].pow(3)/3
  objective[2,i] = deltaX[i].pow(2)/2
  objective[3,i] = deltaX[i]

#print(objective)


inequalityBoundMatrix = torch.zeros(5,Q,4,Q)
inequalityBound = torch.zeros(5, Q)

for i in range(Q):
  # Constraint: f''(0) >= 0
  inequalityBoundMatrix[0,i,1,i] = -2

  # Constraint: f''(deltaX) >= 0
  inequalityBoundMatrix[1,i,0,i] = -6*deltaX[i]
  inequalityBoundMatrix[1,i,1,i] = -2

  # Constraint: f(0) <= Yi
  inequalityBoundMatrix[2,i,3,i] = 1
  inequalityBound[2,i] = corners[i,1]

  # f(deltaX) <= y_{t+1}
  inequalityBoundMatrix[3,i,0,i] = deltaX[i].pow(3)
  inequalityBoundMatrix[3,i,1,i] = deltaX[i].pow(2)
  inequalityBoundMatrix[3,i,2,i] = deltaX[i].pow(1)
  inequalityBoundMatrix[3,i,3,i] = 1
  inequalityBound[3,i] = corners[i+1,1]

  # Constraint: f'(0) <= 0
  inequalityBoundMatrix[4,i,2,i] = 1
  inequalityBoundMatrix[4,i,3,i] = 0



equalityConstraintMatrix = torch.zeros(2,Q, 4, Q)
equalityConstraint = torch.zeros(2, Q)
for i in range(Q):
  if i < Q-1:
    # Constraint: f(deltaX) = f_{t+1}(0)
    equalityConstraintMatrix[0,i,0,i] = deltaX[i].pow(3)
    equalityConstraintMatrix[0,i,1,i] = deltaX[i].pow(2)
    equalityConstraintMatrix[0,i,2,i] = deltaX[i]
    equalityConstraintMatrix[0,i,3,i] = 1
    equalityConstraintMatrix[0,i,3,i+1] = -1
  
    # Constraint: f'_t(deltaX) equals f'_{t+1}(0) (Continuity of f')
    equalityConstraintMatrix[1,i,0,i] = 3*deltaX[i].pow(2)
    equalityConstraintMatrix[1,i,1,i] = 2*deltaX[i]
    equalityConstraintMatrix[1,i,2,i] = 1
    equalityConstraintMatrix[1,i,2,i+1] = -1



#print(equalityConstraintMatrix)

objective_ = objective.view(-1).numpy()
A_ub = inequalityBoundMatrix.view(-1, 4*Q).numpy()
b_ub = inequalityBound.view(-1).numpy()
A_eq = equalityConstraintMatrix.view(2*Q, 4*Q).numpy()
b_eq = equalityConstraint.view(2*Q).numpy()

x = torch.zeros(4,Q)
x[2] = -1
x = x.view(-1)


x[2] = -1
x = x.view(-1)

import cvxpy as cp
import numpy as np



import seaborn as sns
import matplotlib.pyplot as plt

# Plotting the corners for debugging purposes
#fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(3, 3))
#
#ax[0].scatter(corners[:,0], corners[:,1])
#ax[0].scatter([surprisalReal], [deplenReal], c='r')
#
#plt.show()
##
#
#


x = cp.Variable(shape=(4*Q), name="x")


#A = np.array([[4,3],[-3,4]])
constraints = []
for i in range(A_ub.shape[0]):
    #print(A_ub.shape, x.shape)
    constraints.append(cp.matmul(A_ub[i], x) <= b_ub[i])
for i in range(A_eq.shape[0]):
    constraints.append(cp.matmul(A_eq[i], x) == b_eq[i])

objective = cp.Minimize(cp.sum(cp.multiply(x, objective_)))
problem = cp.Problem(objective, constraints)
solution = problem.solve()
#print(solution)
#print(x.value)
#print("objective:")
#print(cp.multiply(x.value, objective_))
#print(torch.FloatTensor(x.value).view(3,Q))
#cp.matmul(A, x) <= 12, x<=2, x>=0]

#print(problem)
#print(problem.status)
#print(problem.value)
#print(solution)
try:
   x = torch.FloatTensor(x.value)
except TypeError:
   assert False, language
#print("ineq_part", torch.FloatTensor(b_ub)-torch.matmul(torch.FloatTensor(A_ub), x)) # this should be nonnegative
#print("eq_part", torch.FloatTensor(b_eq) - torch.matmul(torch.FloatTensor(A_eq), x)) # this should be zero

x = x.view(4,Q)

xnew = torch.FloatTensor([])
ynew = torch.FloatTensor([])
yPrimenew = torch.FloatTensor([])
for i in range(Q):
    xnew_ = torch.FloatTensor(np.arange(corners[i][0], corners[i+1][0], (corners[i+1][0] - corners[i][0])/20))
    xnew__ = xnew_ - corners[i][0]
#    print(xnew__)
    #assert deltaX[i] == corners[i+1][0] - corners[i][0]
    deltaX = corners[i+1][0] - corners[i][0]
    deltaY = corners[i+1][1] - corners[i][1]
    ynew_ = x[0,i] * xnew__.pow(3) + x[1,i] * xnew__.pow(2) + x[2,i] * xnew__ + x[3,i]
    yPrimenew_ = 3 * x[0,i] * xnew__.pow(2) + 2 * x[1,i] * xnew__ + x[2,i]
    xnew = torch.cat([xnew, xnew_], dim=0).detach()
    ynew = torch.cat([ynew, ynew_], dim=0).detach()
    yPrimenew = torch.cat([yPrimenew, yPrimenew_], dim=0).detach()
    print("Point", i, "Cubic part", x[0,i], "Quadratic part", x[1,i], "Linear part", x[2,i])

#print("Difference between derivatives at knots", (derivativeAtEnd[:-1] - b[1:]))


smoothed_grid = torch.FloatTensor([[smoothed(x,y) for x,y in z] for z in grid]).view(-1)
#grid_list = grid
#grid_within_range = torch.ByteTensor([[((surprisal-x).pow(2) + (deplen-y).pow(2)).min() < 0.02 for x,y in z] for z in grid]).view(-1)
grid_dominated = torch.ByteTensor([[torch.logical_and(x > xnew, y>ynew).any() for x,y in z] for z in grid]).view(-1)
grid = torch.FloatTensor(grid).view(-1, 2)
grid = grid[grid_dominated]
smoothed_grid = smoothed_grid[grid_dominated]


#print(xnew)
#print(ynew)

if False: # runs into errors in Danish_2.8
   lambdaUpper = torch.zeros(xnew.size()[0])+1
   lambdaLower = torch.zeros(xnew.size()[0])
   
   for i in range(1,xnew.size()[0]-1):
       a1, b1 = xnew[i-1], ynew[i-1]
       a, b = xnew[i], ynew[i]
       a2, b2 = xnew[i+1], ynew[i+1]
       assert a1 >= a-1e-2
       assert b1 <= b+1e-2, (b, b1)
       assert b >= b1-1e-2
       assert b <= b2+1e-2
       if b1 == b or b2 == b:
           continue
       assert a <= a1+1e-2
       assert a2 <= a+1e-2
       assert b >= b1-1e-2
       assert b2 >= b-1e-2
       lambda_1 = (xnew-a)/(b-ynew)
       bounds = (lambda_1)
       sign = (b-ynew<0)
       lower = bounds[sign]
       upper = bounds[torch.logical_not(sign)]
       lower = lower[lower.isnan().logical_not()].max()
       upper = upper[upper.isnan().logical_not()].min()
       lambdaUpper[i] = upper/(1+upper)
       lambdaLower[i] = lower/(1+lower)
   #    if lower >= upper:
    #       continue
     #  print("...")
       if i % 20 == 0:
         print("Point on curve", a, b, "lambda in range:", lower, upper, "Derivative should be in the lambda range:", -1/yPrimenew[i], language)
       if lower > upper+1e-4:
           print("Error 560:", (lower, upper, upper-lower))
       if (a+lower*b > (xnew + lower*ynew).min()+1e-5):
           print("Error 562", language)
       if (a+upper*b > (xnew + upper*ynew).min()+1e-5):
           print("Error 564", language)
   
   lambdaUpper[-1] = lambdaLower[-2]
   lambdaLower[0] = lambdaUpper[1]
   #print("lower", lambdaLower)
   #print("upper", lambdaUpper)
  

#lambdaEstimate = 1/(1-1/yPrimenew) # not compatible with the extended frontier


print(xnew)
print(ynew)
print(surprisal_baselines.mean())
print(deplen_baselines.mean())
xnew_extended = xnew
ynew_extended = ynew
#print(xnew_extended.size())
if xnew.max() < surprisal_baselines.mean():
   xnew_extended = torch.cat([xnew.max() +  torch.arange(20, -1, -1) * (surprisal_baselines.mean()-xnew.max())/20, xnew_extended], dim=0)
   ynew_extended = torch.cat([ynew.min() + 0*torch.arange(20 ,-1, -1), ynew_extended], dim=0)
if ynew.max() < deplen_baselines.mean():
   xnew_extended = torch.cat([xnew_extended, xnew.min() + torch.zeros(20)], dim=0)
   ynew_extended = torch.cat([ynew_extended, ynew.max() + torch.arange(20) * (deplen_baselines.mean()-ynew.max())/20], dim=0) 
#print(xnew_extended.size())
#quit()
xnew = xnew_extended
ynew = ynew_extended

fOnFrontier = torch.FloatTensor([float(smoothed(x, y)) for x, y in zip(xnew, ynew)])


nextFrontierPoint = ((xnew-surprisalReal).pow(2) + (ynew-deplenReal).pow(2)).argmin()
lambda_optim = float('nan')
#lambda_optim_lower = float(lambdaLower[nextFrontierPoint])



frontierDistanceX = (xnew - surprisal_baselines.mean())
frontierDistanceY = (ynew - deplen_baselines.mean())

# dx>0 --> surprisal is HIGHER than for the baseline
# dx=0, dy>0 --> 2pi
# dx=0, dy<0 --> pi
# dx>0, dy=0 --> 0.5pi
# dx<0, dy=0 --> 1.5pi
def getAngle(dx, dy):
  norm = (dx.pow(2) + dy.pow(2)).sqrt()
  dx = dx/norm
  dy = dy/norm
  acosBranch = torch.acos(dx)
  return torch.where(dy>0, acosBranch, 2*3.14-acosBranch)

anglesFrontier = getAngle(frontierDistanceX, frontierDistanceY)
assert anglesFrontier.max() <= 2*3.14+1e-3
assert anglesFrontier.min() >= 0
#print(frontierDistanceX)
#print(anglesFrontier)
#quit()

distanceInAngleSpace = -(anglesFrontier[1:] - anglesFrontier[:-1]) # negative, because xnew/ynew are ordered clockwise, while the angles are counted counterclockwise
assert (distanceInAngleSpace>=-0.0001).all(), anglesFrontier
print("ANGLE DISTANCE", distanceInAngleSpace.mean())

distanceInCurveSpace = ((xnew[1:] - xnew[:-1]).pow(2) + (ynew[1:] - ynew[:-1]).pow(2)).sqrt()
distanceNormalized = distanceInCurveSpace / distanceInCurveSpace.sum()
distanceCumulative = torch.zeros(distanceNormalized.size())
cumulative=0
for i in range(distanceNormalized.size()[0]):
  distanceCumulative[i] = cumulative
  cumulative += distanceNormalized[i]


#print(fOnFrontier)

# at which lambda is it the case that the 


assert fOnFrontier.max() < 1.01, fOnFrontier.max()
assert fOnFrontier.min() >= -0.01, fOnFrontier.min()
assert distanceInAngleSpace.min() >= -1e-3, distanceInAngleSpace

#distanceInLambdaSpace = lambdaEstimate[1:] - lambdaEstimate[:-1]
#assert distanceInLambdaSpace.sum() == 1, distanceInLambdaSpace.sum()
#averageFunction = float((fOnFrontier[:-1] * distanceInLambdaSpace).sum() / distanceInLambdaSpace.sum())
#with open(f"output/{__file__}.tsv", "a") as outFile:
#  print(language, "mean_x", float(surprisal_baselines.mean()), file=outFile)
#  print(language, "mean_y", float(deplen_baselines.mean()), file=outFile)
#  print(language, "min_x", float(surprisal.min()), file=outFile)
#  print(language, "min_y", float(deplen.min()), file=outFile)
#  print(language, "surprisalRange_min", float(raw_surprisalRange[0]), file=outFile)
#  print(language, "surprisalRange_max", float(raw_surprisalRange[1]), file=outFile)
#  print(language, "deplenRange_min", float(raw_deplenRange[0]), file=outFile)
#  print(language, "deplenRange_max", float(raw_deplenRange[1]), file=outFile)
#
##  print(language, "AverageFLambdaSpace", averageFunction, file=outFile)
#
#  averageFunction = float((fOnFrontier[1:] * distanceInCurveSpace).sum() / distanceInCurveSpace.sum())
#  print(language, "AverageFCurveSpace", averageFunction, file=outFile)
#
#  print("These should be about the same", distanceInAngleSpace.sum(), 1.57)
#  averageFunctionAngle  = float((fOnFrontier[1:] * distanceInAngleSpace).sum() / distanceInAngleSpace.sum())
#  print(language, "AverageFAngleSpace", averageFunctionAngle, file=outFile)
# 
#  print("Next frontier point", nextFrontierPoint, fOnFrontier.size())  
#  print(language, "FNextFrontier", float(fOnFrontier[nextFrontierPoint]), file=outFile)
#
#  distanceX = (((surprisalReal-surprisal_baselines.mean())))
#  distanceY = (deplenReal-deplen_baselines.mean())
#  print(language, "Distance_Surprisal_ComparedFrontier", float(distanceX/(surprisal.min()-surprisal_baselines.mean())), file=outFile)
#  print(language, "Distance_DepLen_ComparedFrontier", float(distanceY/(deplen.min()-deplen_baselines.mean())), file=outFile)
#
scale_x = (surprisal.min()-surprisal_baselines.mean()).abs()
scale_y = (deplen.min()-deplen_baselines.mean()).abs()
#  varianceX = (surprisal_baselines.pow(2).mean() - surprisal_baselines.mean().pow(2)) / scale_x.pow(2)
#  varianceY = (deplen_baselines.pow(2).mean() - deplen_baselines.mean().pow(2)) / scale_y.pow(2)
#  covarianceXY = ((surprisal_baselines * deplen_baselines).mean() - (surprisal_baselines.mean() * deplen_baselines.mean())) / (scale_x * scale_y)
##  print(varianceX, varianceY, covarianceXY)
#  print(language, "Baseline_Variance_X_ComparedToFrontier", float(varianceX), file=outFile)
#  print(language, "Baseline_Variance_Y_ComparedToFrontier", float(varianceY), file=outFile)
#  print(language, "Baseline_Covariance_XY_ComparedToFrontier", float(covarianceXY), file=outFile)
# # quit()
#  
#
#  print(language, "Distance_Surprisal", float(distanceX), file=outFile)
#  print(language, "Distance_DepLen", float(distanceY), file=outFile)
#  # angle in 0-1 transform
#  print(language, "Angle01", float(getAngle(distanceX, distanceY)), file=outFile)
#  # angle in z-scoring 
#  varianceX = (surprisal_baselines.pow(2).mean() - surprisal_baselines.mean().pow(2)).sqrt()
#  varianceY = (deplen_baselines.pow(2).mean() - deplen_baselines.mean().pow(2)).sqrt()
#  distanceXNormalized = distanceX / varianceX
#  distanceYNormalized = distanceY / varianceY
#  print(language, "Distance_Surprisal_Z", float(distanceXNormalized), file=outFile)
#  print(language, "Distance_DepLen_Z", float(distanceYNormalized), file=outFile)
#  print(language, "AngleZ", float(getAngle(distanceXNormalized, distanceYNormalized)), file=outFile)
# 
#  
#  distanceToFrontier = float((((xnew-surprisalReal).pow(2) + (ynew-deplenReal).pow(2)).min()).sqrt())
#  distanceToRandomMean = float(((surprisalReal-surprisal_baselines.mean()).pow(2) + (deplenReal-deplen_baselines.mean()).pow(2)).sqrt())
#  # line from the random mean to the point
#  x0 = float(surprisal_baselines.mean())
#  y0 = float(deplen_baselines.mean())
#  x1 = float(surprisalReal)
#  y1 = float(deplenReal)
#  A = (y1-y0)/(x1-x0)
#  #print("A", A)
#  B = y0 - A*x0
#  #print("B", B)
#
#  distanceFromLineAlongFrontier = (A * xnew + B - ynew).pow(2)
#
# # print(distanceFromLineAlongFrontier)
#  nextFrontierPointByLine = distanceFromLineAlongFrontier.argmin()
#  print(language, "FNextFrontierByLine", float(fOnFrontier[nextFrontierPointByLine]), file=outFile)
#
##  lambda_optim_alongLine = float(lambdaEstimate[nextFrontierPointByLine])
#  distanceFromFrontierAlongLine = ((xnew[nextFrontierPointByLine] - surprisalReal).pow(2) + (ynew[nextFrontierPointByLine] - deplenReal).pow(2)).sqrt()
##  print(distanceFromFrontierAlongLine)
#
#  print(language, "CurveSpaceEstimate", float(distanceCumulative[nextFrontierPoint]), file=outFile)
#  print(language, "CurveSpaceEstimateAlongLine", float(distanceCumulative[nextFrontierPointByLine]), file=outFile)
##  print(language, "LambdaEstimateAlongLine", float(lambda_optim_alongLine), file=outFile)
#  print(language, "DistanceFrontierAlongLine", float(distanceFromFrontierAlongLine), file=outFile)
#  print(language, "DistanceFromFrontierAlongLineRelative", float(distanceFromFrontierAlongLine/(distanceFromFrontierAlongLine + distanceToRandomMean)), file=outFile)
#  print(language, "DistanceFromFrontierRelative", float(distanceToFrontier/(distanceToFrontier + distanceToRandomMean)), file=outFile)
# 
##  print(language, "LambdaEstimate", lambda_optim, file=outFile)
##  print(language, "LambdaUpper", lambda_optim_upper, file=outFile)
#  print(language, "DistanceRandom", distanceToRandomMean, file=outFile)
#  print(language, "DistanceFrontier", distanceToFrontier, file=outFile)
#  print(language, "SmoothedReal", float(smoothed(surprisalReal, deplenReal)), file=outFile)
#
#
#for i in range(1,sorted_frontier.size()[0]-1):
#    valueHere = sorted_frontier[i]
#
#
#    valuePrevious = sorted_frontier[i-1]
#    valueNext = sorted_frontier[i+1]
##    print(i, valueHere, "previous", valuePrevious,"next", valueNext)
#    assert valuePrevious[0] >= valueHere[0]
#    assert valuePrevious[1] <= valueHere[1]
#    assert valueHere[0] >= valueNext[0]
#    assert valueHere[1] <= valueNext[1]
#    a, b = [float(q) for q in valueHere]
#    a1, b1 = [float(q) for q in valuePrevious]
#    a2, b2 = [float(q) for q in valueNext]
#    if b1 == b or b2 == b:
#        continue
#    assert a <= a1
#    assert a2 <= a
#    assert b >= b1
#    assert b2 >= b
#    lambda_1 = (a1-a)/(b-b1)
#    lambda_2 = (a2-a)/(b-b2)
#    print(i, "\t", a, "\t", b, "\t", lambda_1, "\t", lambda_2, "\t", "Trade-off", a+lambda_1 *b, "at previous", a1 + lambda_1 *b1)
#
#    assert a+lambda_1 *b <= a1 + lambda_1 *b1+1e-5
#    assert a+lambda_2 *b <= a2 + lambda_2 *b2+1e-5, (a+lambda_2 *b , a2 + lambda_2 *b2)
#
#

    

import seaborn as sns
import matplotlib.pyplot as plt
#plt.show()


# the following can be used for sanity-checking computation of the frontier
#fig, ax = plt.subplots(nrows=1, ncols=4, figsize=(15, 3))
#
##ax[2].scatter((grid).view(-1, 2)[:,0], (grid).view(-1, 2)[:,1], c=smoothed_grid.view(-1)-0.5, cmap='viridis')
##ax[2].scatter(corners[:,0], corners[:,1], cmap='viridis')
##ax[2].scatter([surprisalReal], [deplenReal])
##
#ax[3].scatter((grid).view(-1, 2)[:,0], (grid).view(-1, 2)[:,1], c=smoothed_grid.view(-1)-0.5, cmap='viridis')
#ax[3].scatter([surprisalReal], [deplenReal])
#ax[3].scatter(xnew, ynew, c='r') # c=10*smoothed_frontier_-0.5, 
#ax[3].scatter(corners[:,0], corners[:,1], c='b')
#ax[3].scatter([xnew[nextFrontierPointByLine]], [ynew[nextFrontierPointByLine]], c='g')
#ax[3].scatter([surprisalReal], [deplenReal])
#ax[3].text(0, 1, "lambda="+str(round((float(lambda_optim_alongLine)), 2)))
#
#plt.show()

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

viridis = cm.get_cmap('viridis', 8)

from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})


fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3, 3))

# TODO issue: values are sometimes > 1. some kind of bug!
ax.scatter(((grid).view(-1, 2)[:,0] - float(surprisal_baselines.mean())) / float(scale_x), ((grid).view(-1, 2)[:,1] - float(deplen_baselines.mean())) / float(scale_y), c=smoothed_grid.view(-1)-0.5, cmap='viridis', vmin=-0.5, vmax=0.5, alpha=0.1)
sns.kdeplot(x=(surprisal_baselines - float(surprisal_baselines.mean())) / float(scale_x), y=(deplen_baselines - float(deplen_baselines.mean())) / float(scale_y), ax=ax, color='white')
ax.scatter((xnew - float(surprisal_baselines.mean())) / float(scale_x), (ynew - float(deplen_baselines.mean())) / float(scale_y), c=fOnFrontier, cmap='viridis', vmin=0, vmax=1) 
ax.scatter([(surprisalReal - float(surprisal_baselines.mean())) / float(scale_x)], [(deplenReal - float(deplen_baselines.mean())) / float(scale_y)], s=80, c=viridis(float(realCongruence)   ))
#ax.text(0, 1, "lambda="+str(round((float(lambda_optim_alongLine)), 2)))
ax.set_ylim([-1.1, 0.1])
ax.set_xlim([-1.3, 0.1])

#plt.show()

fig.savefig(f"figures/perLanguage/{__file__}_{language}_smoothed_scaled.pdf")
#quit()



fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3, 3))

# TODO issue: values are sometimes > 1. some kind of bug!
ax.scatter(((grid).view(-1, 2)[:,0] - float(surprisal_baselines.mean())) / float(scale_x), ((grid).view(-1, 2)[:,1] - float(deplen_baselines.mean())) / float(scale_y), c=smoothed_grid.view(-1)-0.5, cmap='viridis', vmin=-0.5, vmax=0.5) #, alpha=0.1)
sns.kdeplot(x=(surprisal_baselines - float(surprisal_baselines.mean())) / float(scale_x), y=(deplen_baselines - float(deplen_baselines.mean())) / float(scale_y), ax=ax, color='white')
ax.scatter((xnew - float(surprisal_baselines.mean())) / float(scale_x), (ynew - float(deplen_baselines.mean())) / float(scale_y), c="red") #fOnFrontier, cmap='viridis', vmin=0, vmax=1) 
ax.scatter([(surprisalReal - float(surprisal_baselines.mean())) / float(scale_x)], [(deplenReal - float(deplen_baselines.mean())) / float(scale_y)], s=80, c="red") #viridis(float(realCongruence)   ))
#ax.text(0, 1, "lambda="+str(round((float(lambda_optim_alongLine)), 2)))
#ax.set_ylim([-1.1, 0.1])
#ax.set_xlim([-1.3, 0.1])

#plt.show()

fig.savefig(f"figures/perLanguage/{__file__}_{language}_smoothed_scaled_noBounds.pdf")
#quit()




#fig, ax = plt.subplots()

print(raw_surprisalRange) 
print(raw_deplenRange )


def convert_dl(y):
  #  y = (y - deplen_baselines.mean()) / scale_y
    y = y * (raw_deplenRange[1] - raw_deplenRange[0]) + raw_deplenRange[0]
    return y
def convert_surp(y):
 #   y = (y - surprisal_baselines.mean()) / scale_x
    y = y * (raw_surprisalRange[1] - raw_surprisalRange[0]) + raw_surprisalRange[0]
    #print(y <= raw_surprisalRange[1])
    return y
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3, 3))

ax.scatter(convert_surp(surprisal), convert_dl(deplen), c=order-0.5, cmap='summer')
ax.scatter([convert_surp(surprisalReal)], [convert_dl(deplenReal)], s=80, c='r')

fig.savefig(f"figures/perLanguage/{__file__}_{language}_raw.pdf")

#plt.show()

#ax[1].scatter(surprisal, deplen, c=loo_smoothed(k_x, k_y)-0.5, cmap='summer')
#ax[1].scatter([surprisalReal], [deplenReal])

#ax[2].scatter((grid).view(-1, 2)[:,0], (grid).view(-1, 2)[:,1], c=smoothed_grid.view(-1)-0.5, cmap='viridis')
#ax[2].scatter(corners[:,0], corners[:,1], cmap='viridis')
#ax[2].scatter([surprisalReal], [deplenReal])
#
#ax[3].scatter((grid).view(-1, 2)[:,0], (grid).view(-1, 2)[:,1], c=smoothed_grid.view(-1)-0.5, cmap='viridis')
#ax[3].scatter([surprisalReal], [deplenReal])
#ax[3].scatter(grid_frontier_[:,0], grid_frontier_[:,1], cmap='viridis') # c=10*smoothed_frontier_-0.5, 
#ax[3].scatter([surprisalReal], [deplenReal])

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3, 3))

# TODO issue: values are sometimes > 1. some kind of bug!
ax.scatter(convert_surp(grid).view(-1, 2)[:,0], convert_dl(grid).view(-1, 2)[:,1], c=smoothed_grid.view(-1)-0.5, cmap='viridis', vmin=-0.5, vmax=0.5)
sns.kdeplot(x=convert_surp(surprisal_baselines), y=convert_dl(deplen_baselines), ax=ax, color='white')
ax.scatter([convert_surp(surprisalReal)], [convert_dl(deplenReal)], s=80, c='r')
ax.scatter(convert_surp(xnew), convert_dl(ynew), c='r') # c=10*smoothed_frontier_-0.5, 
#ax.text(0, 1, "lambda="+str(round((float(lambda_optim_alongLine)), 2)))

fig.savefig(f"figures/perLanguage/{__file__}_{language}_smoothed.pdf")


fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3, 3))

## TODO issue: values are sometimes > 1. some kind of bug!
##ax.scatter((grid).view(-1, 2)[:,0], (grid).view(-1, 2)[:,1], c=smoothed_grid.view(-1)-0.5, cmap='viridis', vmin=-0.5, vmax=0.5)
#sns.kdeplot(x=surprisal_baselines, y=deplen_baselines, ax=ax, color='gray')
#ax.scatter(xnew, ynew, c=fOnFrontier, cmap='viridis', vmin=0, vmax=1) # c=10*smoothed_frontier_-0.5, 
#ax.scatter([surprisalReal], [deplenReal], c=[realCongruence], cmap='viridis', s=160, vmin=0, vmax=1)
##ax.text(0, 1, "lambda="+str(round((float(lambda_optim_alongLine)), 2)))
#
#fig.savefig(f"figures/perLanguage/{__file__}_{language}_smoothed_onlyFrontier.pdf")

print(realCongruence)

#plt.show()


##print(fOnFrontier)
##print(distanceInCurveSpace)
##print(distanceCumulative)
#with open(f"output_frontier/{__file__}_{language}_fOnFrontierDiscretized.tsv", "w") as outFile:
#  for fraction in range(10):
##    print(torch.logical_and((distanceCumulative>=fraction/10), (distanceCumulative<=(fraction+1)/10)))
#    print(fraction/10, round(float(fOnFrontier[1:][torch.logical_and((distanceCumulative>=fraction/10), (distanceCumulative<=(fraction+1)/10))].mean()),2), file=outFile)
#with open(f"output_frontier/{__file__}_{language}_fOnFrontierDiscretizedByAngle.tsv", "w") as outFile:
#  for fraction in [3.14 + 1.57*x/10 for x in range(10)]:
#    print(round(fraction, 3), round(float(fOnFrontier[torch.logical_and((anglesFrontier>=fraction), (anglesFrontier<=(fraction+1.57/10)))].mean()),2), file=outFile)
#
#
#
#with open(f"output_frontier/{__file__}_{language}_fArea.tsv", "w") as outFile:
#  for x_ in range(15):
#      for y_ in range(15):
#         x = -x_/10+0.5
#         y = -y_/10+0.5
#         x__ = (x + surprisal_baselines.mean()) / scale_x
#         y__ = (y + deplen_baselines.mean()) / scale_y
#         print("FOR SMOOTH FILE", x__, y__, grid_.mean(dim=0), grid.min(dim=0)[0], grid.max(dim=0)[0], "SMOOTHED", smoothed(x__,y__))
#         print(round(x, 2), round(y, 2), round(float(smoothed(x__,y__)),2), file=outFile)
#
##print(torch.FloatTensor(grid))
##print(smoothed_grid)
##print(torch.FloatTensor([[smoothed(x,y) for x,y in z] for z in grid_list]).view(-1))
##for z in grid_list:
##    for x, y in z:
##        print("FROM GRID", x, y, smoothed(x,y))
#
#
#print(xnew.size(), anglesFrontier.size())
#with open(f"output_frontier/{__file__}_{language}_FrontierDiscretizedByAngle.tsv", "w") as outFile:
#  for fraction in [3.14 + 1.57*x/10 for x in range(10)]:
#    print(round(fraction, 3), "X", round(float(surprisal_baselines.mean()-xnew[torch.logical_and((anglesFrontier>=fraction), (anglesFrontier<=(fraction+1.57/10)))].mean()) / float(scale_x),2), file=outFile)
#    print(round(fraction, 3), "Y", round(float(deplen_baselines.mean()-ynew[torch.logical_and((anglesFrontier>=fraction), (anglesFrontier<=(fraction+1.57/10)))].mean()) / float(scale_y),2), file=outFile)
#
#
##quit()
##with open(f"output/{__file__}.tsv", "a") as outFile:
##   print("\t".join([str(q) for q in [language, float(smoothed(surprisalReal, deplenReal))]]), file=outFile)
#
#
#
