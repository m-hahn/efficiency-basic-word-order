import sys
script = sys.argv[1]
import subprocess
import random
from ud_languages_28 import languages
random.shuffle(languages)
for lang in languages:
    subprocess.call(["python3", f"process{script}.py", lang])
