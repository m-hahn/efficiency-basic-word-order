import sys
script = sys.argv[1]
import subprocess
import random
from ud_languages_28 import languages
#random.shuffle(languages)
languages = sorted(languages)
languages.append("PARTITION-Icelandic_early")
languages.append("PARTITION-Icelandic_middle")
languages.append("PARTITION-Icelandic_modern")
languages.append("PARTITION-Ancient_Greek_archaic")
languages.append("PARTITION-Ancient_Greek_classical")
languages.append("PARTITION-Ancient_Greek_koine")
languages.append("ISWOC_Spanish")
languages.append("ISWOC_Portuguese")
languages.append("ISWOC_Old_English")
for lang in languages:
    subprocess.call(["python3", f"process{script}_frontier18_Viz.py", lang])
