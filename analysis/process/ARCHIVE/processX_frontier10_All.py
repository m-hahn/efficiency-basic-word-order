import sys
script = sys.argv[1]
import subprocess
import random
from ud_languages_28 import languages
random.shuffle(languages)
for lang in languages:
    subprocess.call(["/u/nlp/anaconda/main/anaconda3/envs/py37-mhahn/bin/python", f"process{script}_frontier10.py", lang])
