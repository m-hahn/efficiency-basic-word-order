import sys
script = sys.argv[1]
import subprocess
import random
from ud_languages_28 import languages
random.shuffle(languages)
for lang in languages:
    if "German" in lang:
        lang = "German-GSD_2.8"
    elif "Czech" in lang:
        lang = "Czech-PDT_2.8"
    elif "Japanese" in lang:
        lang = "Japanese-GSD_2.8"
    subprocess.call(["python3", f"process{script}_frontier11_New.py", lang])
