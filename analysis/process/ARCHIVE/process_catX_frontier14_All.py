import sys
script = sys.argv[1]
import subprocess
import random
from ud_languages_28 import languages
#random.shuffle(languages)
languages = sorted(languages)
for lang in languages:
    subprocess.call(["./python3", f"process_cat{script}_frontier14.py", lang])
