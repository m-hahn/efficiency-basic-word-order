import torch
import sys

language = sys.argv[1] #"Korean_2.8"


with open("../raw_output/real.tsv", "r") as inFile:
    header = next(inFile).strip().split("\t")
    header = dict(list(zip(header, range(len(header)))))
    data = [x.split("\t") for x in inFile.read().strip().split("\n")]
data = [x for x in data if x[header["language"]] == language]
deplenReal = float(data[0][header["DepLen"]])
surprisalReal = float(data[0][header["Surprisal2"]])


with open("../raw_output/landscape.tsv", "r") as inFile:
    header = next(inFile).strip().split("\t")
    header = dict(list(zip(header, range(len(header)))))
    data = [x.split("\t") for x in inFile.read().strip().split("\n")]




data = [x for x in data if x[header["language"]] == language and x[header["basic"]] != ""]


surprisal = torch.FloatTensor([float(x[header["Surprisal2"]]) for x in data])
deplen = torch.FloatTensor([float(x[header["DepLen"]]) for x in data])


with open("../raw_output/baselines.tsv", "r") as inFile:
    header_baselines = next(inFile).strip().split("\t")
    header_baselines = dict(list(zip(header_baselines, range(len(header_baselines)))))
    baselines = [x.split("\t") for x in inFile.read().strip().split("\n")]
baselines = [x for x in baselines if x[header_baselines["language"]] == language and x[header_baselines["basic"]] != ""]
surprisal_baselines = torch.FloatTensor([float(x[header_baselines["Surprisal2"]]) for x in baselines])
deplen_baselines = torch.FloatTensor([float(x[header_baselines["DepLen"]]) for x in baselines])




print(surprisal)

surprisalRange = (surprisal.min(), surprisal.max())
print(surprisalRange)
deplenRange = (deplen.min(), deplen.max())
print(deplenRange)

deplenReal -= deplenRange[0]
deplenReal /= deplenRange[1] - deplenRange[0]
surprisalReal -= surprisalRange[0]
surprisalReal /= surprisalRange[1] - surprisalRange[0]

assert deplenReal <= 2, deplenReal
assert surprisalReal <= 2, surprisalReal

print("S", surprisal_baselines.mean())
print("D", deplen_baselines.mean())
print("S", surprisal.mean())
print("D", deplen.mean())

surprisal_baselines = surprisal_baselines - surprisalRange[0]
surprisal_baselines /= (surprisalRange[1] - surprisalRange[0])
deplen_baselines = deplen_baselines - deplenRange[0]
deplen_baselines /= (deplenRange[1] - deplenRange[0])

print("S", surprisal_baselines.mean(), "---", surprisal.max())
print("D", deplen_baselines.mean())
#quit()

surprisal = surprisal - surprisalRange[0]
surprisal /= surprisal.max()
deplen = deplen - deplenRange[0]
deplen /= deplen.max()

print("S", surprisal.mean())
print("D", deplen.mean())
#quit()


surprisalRange = (0,1)
deplenRange = (0,1)



order = [x[header["basic"]] for x in data]
order = torch.FloatTensor([{"SOV" : 1, "SVO" : 0, "OSV" : 1}[x] for x in order])



print(surprisal.size())


def loo_smoothed(k_x, k_y):
    d_x = (surprisal.unsqueeze(1) - surprisal.unsqueeze(0)).pow(2)
    d_y = (deplen.unsqueeze(1) - deplen.unsqueeze(0)).pow(2)
    logit = (k_x * d_x + k_y * d_y)
 #   print(logit)
#    print(1e10 * torch.eye(surprisal.size()[0]))
    logit -= 1e10 * torch.eye(surprisal.size()[0])
    kernel = torch.exp(logit) / torch.exp(logit).sum(dim=1).unsqueeze(1)
    smoothed = (kernel * order.unsqueeze(0)).sum(dim=1)
    return smoothed

def optimize():
  k_x = torch.FloatTensor([0])
  k_y = torch.FloatTensor([0])
  k_x.requires_grad = True
  k_y.requires_grad = True
  
  optim = torch.optim.Adam([k_x, k_y], lr=0.001)
  
  lastLoss = 100
  for iteration in range(100000):
    prediction = loo_smoothed(k_x, k_y)
    loss = (prediction - order).pow(2).mean() #+ 0.001 * (k_x.pow(2) + k_y.pow(2))
    if iteration % 500 == 0:
      print(iteration, "LOSS", loss, k_x, k_y, language)
    if iteration % 10000 == 0:
        if lastLoss - float(loss) < 0.0001:
            break
        lastLoss = float(loss)
    loss.backward()
    optim.step()
    optim.zero_grad()
  return float(k_x), float(k_y)

grid = ([[[-0.05 + i/100 * 1.1  ,-0.05 + j/100 *  1.1] for j in range(100)] for i in range(100)])

N = surprisal.size()[0]
dominated = torch.ByteTensor(N)

for i in range(N):
   better = torch.logical_and((surprisal < surprisal[i]), (deplen < deplen[i])).any()
   dominated[i] = better


#k_x, k_y = optimize() #-90.0241
k_x, k_y = -4.28, -21.7
#k_y = -1.5454


def smoothed(x, y):
    d_x = (surprisal - x).pow(2)
    d_y = (deplen - y).pow(2)
    logit = (k_x * d_x + k_y * d_y)
#    print(k_x, k_y, d_x, d_y)
 #   print("Logit", logit)
    kernel = torch.exp(logit) / torch.exp(logit).sum().unsqueeze(0)
  #  print("Kernel", kernel)
    smoothed = (kernel * order).sum(dim=0)
    return smoothed

print( "Order", order)
print("Smoothed Real", smoothed(surprisalReal, deplenReal), language)
#quit()


#print(smoothed(4.6, 2.6))

print("PREDICTIVE FOR KNOWN POINTS")
print(loo_smoothed(k_x, k_y))

#print(torch.FloatTensor(grid))




#print("SMOOTHED GRID")
#print(smoothed_grid)

import numpy as np
import matplotlib.pyplot as plt

#X = smoothed_grid


#ax.imshow(X) #, interpolation='nearest')
print(surprisal.size(), deplen.size(), order.size())
grid_ = torch.stack([surprisal, deplen], dim=1)
dominated_ = dominated.view(-1)
grid_ = grid_[torch.logical_not(dominated_)]


frontier_ = grid_
grid_frontier_ = grid_ #torch.FloatTensor(grid).view(-1,2)[frontier_]


# Need better smoothing to get bijection between lambda and frontier

sortedFrontier = sorted(range(grid_frontier_.size()[0]), key=lambda x:tuple((lambda w: (-w[0], w[1]))([float(q) for q in grid_frontier_[x]])))
#sortedFrontier = sorted(range(grid_frontier_.size()[0]), key=lambda x:tuple([float(q) for q in grid_frontier_[x]]))
sorted_frontier = grid_frontier_[sortedFrontier]
print(sortedFrontier)
print(sorted_frontier)
sorted_frontier_all = sorted_frontier
sorted_frontier = [sorted_frontier_all[0]]
for i in range(1, sorted_frontier_all.size()[0]):
    if sorted_frontier_all[i][0] == sorted_frontier[-1][0]:
        continue
    if sorted_frontier_all[i][1] == sorted_frontier[-1][1]:
        continue
    sorted_frontier.append(sorted_frontier_all[i])
sorted_frontier = torch.stack(sorted_frontier)

corners = [[float(q) for q in sorted_frontier[0]]]

for i in range(1,sorted_frontier.size()[0]-1):
    valueHere = sorted_frontier[i]


    valuePrevious = sorted_frontier[i-1]
    valueNext = sorted_frontier[i+1]
#    print(i, valueHere, "previous", valuePrevious,"next", valueNext)
    assert valuePrevious[0] >= valueHere[0]
    assert valuePrevious[1] <= valueHere[1]
    assert valueHere[0] >= valueNext[0]
    assert valueHere[1] <= valueNext[1]
    a, b = [float(q) for q in valueHere]
    a1, b1 = [float(q) for q in valuePrevious]
    a2, b2 = [float(q) for q in valueNext]
    if b1 == b or b2 == b:
        continue
    assert a <= a1
    assert a2 <= a
    assert b >= b1
    assert b2 >= b
    lambda_1 = (sorted_frontier[:,0]-a)/(b-sorted_frontier[:,1])
    bounds = (lambda_1)
    sign = (b-sorted_frontier[:,1]<0)
    lower = bounds[sign]
    upper = bounds[torch.logical_not(sign)]
    lower = lower[lower.isnan().logical_not()].max()
    upper = upper[upper.isnan().logical_not()].min()
    if lower >= upper:
        continue
    print("...")
    print("Point on curve", a, b, "lambda in range:", lower, upper, language)
    corners.append([a,b])
    assert (a+lower*b <= (sorted_frontier[:,0] + lower*sorted_frontier[:,1]).min()+1e-5)
    assert (a+upper*b <= (sorted_frontier[:,0] + upper*sorted_frontier[:,1]).min()+1e-5)
#    quit()
 #   lambda_2 = (a2-a)/(b-b2)
  #  print(i, "\t", a, "\t", b, "\t", lambda_1, "\t", lambda_2, "\t", "Trade-off", a+lambda_1 *b, "at previous", a1 + lambda_1 *b1)

   # assert a+lambda_1 *b <= a1 + lambda_1 *b1+1e-5
    #assert a+lambda_2 *b <= a2 + lambda_2 *b2+1e-5, (a+lambda_2 *b , a2 + lambda_2 *b2)

###################
# other option: 
# - look at frontier among the raw points
# - smooth the frontier between the points (spline)

corners.append([float(q) for q in sorted_frontier[-1]])

#quit()
corners = torch.FloatTensor(corners)
print("Corners", corners)

import numpy as np

import matplotlib.pyplot as plt

from scipy import interpolate

print("Corners")
print([float(x[0]) for x in corners])
print([float(x[1]) for x in corners])

#tck = interpolate.splrep([float(x[0]) for x in corners][::-1], [float(x[1]) for x in corners][::-1], k=2)


xnew = np.arange(corners[-1][0], corners[0][0], (corners[0][0]-corners[-1][0])/50)
xnew = np.array([])
for i in range(len(corners)-1, 0, -1):
    print(xnew)
    print((np.arange(corners[i][0], corners[i-1][0], (corners[i-1][0]-corners[i][0])/10)))
    xnew = np.concatenate([xnew, (np.arange(corners[i][0], corners[i-1][0], (corners[i-1][0]-corners[i][0])/10))], axis=0)
#xnew = xnew[::-1]

#ynew = interpolate.splev(xnew, tck, der=0)


a = torch.zeros(corners.size()[0]-1)+0.1
#c = torch.zeros(corners.size()[0])
a.requires_grad=True
#c.requires_grad=True

optim = torch.optim.Adam([a],lr=0.001)

lastLoss = 1e100
for it in range(40000):
    loss = 0
    deltaX = corners[1:,0] - corners[:-1,0]
    deltaY = corners[1:,1] - corners[:-1,1]

    assert float(deltaX.max()) <= 0,deltaX
    assert float(deltaY.min()) >= 0,deltaY
#    print(deltaY.size(), a.size(), deltaX.size())
    b = (deltaY - a * deltaX.pow(2)) / deltaX
    assert ((b * deltaX + a * deltaX.pow(2)) - deltaY).abs().max() < 0.001
    assert b.max() <= 0+1e-5, (b, language)
 #   print(b)
  #  print(a)
    #quit()
    derivativeAtEnd = 2*a*deltaX + b
  #  print("Ratio for the derivatives at the junctures", derivativeAtEnd[:-1]/b[1:])

    # Force slopes to be as smooth at the knots as possible
    loss += 200 * torch.log((derivativeAtEnd[:-1] / b[1:]).abs()).abs().sum()

    # Ensure that the slope can never increase
    loss += 100* (derivativeAtEnd[:-1] - b[1:]).clamp(max=-0.0001).abs().sum()

    # Derivative: has to be negative always
#    loss += 200 * b.clamp(min=0).sum()
    loss += 200 * derivativeAtEnd.clamp(min=0).sum()

#    loss += a.abs().sum()
    if it % 500 == 0:
      print(it, "Loss", loss, a, "Last:", lastLoss)
    optim.zero_grad()
    loss.backward()
    optim.step()

    # Enforce hard constraints
    # a has to be nonnegative (for strong convexity)
    a.data = a.data.clamp(min=0.001)
#    print("error to be corrected to force b to be nonpositive", (a.data- deltaY/deltaX.pow(2)).max().clamp(min=0))
    # b has to be nonpositive
    a.data = torch.min(a.data, deltaY/deltaX.pow(2))


    if it % 200 == 0:
        if float(loss) >= lastLoss-1e-5:
            break
        lastLoss = float(loss)

xnew = torch.FloatTensor([])
ynew = torch.FloatTensor([])
for i in range(corners.size()[0]-1):
    xnew_ = torch.FloatTensor(np.arange(corners[i][0], corners[i+1][0], (corners[i+1][0] - corners[i][0])/20))
    xnew__ = xnew_ - corners[i][0]
#    print(xnew__)
    deltaX = corners[i+1][0] - corners[i][0]
    deltaY = corners[i+1][1] - corners[i][1]
    ynew_ = corners[i][1] + b[i] * xnew__ + a[i] * xnew__.pow(2)
    xnew = torch.cat([xnew, xnew_], dim=0).detach()
    ynew = torch.cat([ynew, ynew_], dim=0).detach()
    print("Point", i, "Quadratic part", a[i], "Linear part", b[i])

print("Difference between derivatives at knots", (derivativeAtEnd[:-1] - b[1:]))


smoothed_grid = torch.FloatTensor([[smoothed(x,y) for x,y in z] for z in grid]).view(-1)
grid_within_range = torch.ByteTensor([[((surprisal-x).pow(2) + (deplen-y).pow(2)).min() < 0.02 for x,y in z] for z in grid]).view(-1)
grid_dominated = torch.ByteTensor([[torch.logical_and(x > xnew, y>ynew).any() for x,y in z] for z in grid]).view(-1)
grid = torch.FloatTensor(grid).view(-1, 2)
grid = grid[torch.logical_and(grid_within_range, grid_dominated)]
smoothed_grid = smoothed_grid[torch.logical_and(grid_within_range, grid_dominated)]


print(xnew)
print(ynew)

lambdaUpper = torch.zeros(xnew.size()[0])+1
lambdaLower = torch.zeros(xnew.size()[0])

for i in range(1,xnew.size()[0]-1):
    a1, b1 = xnew[i-1], ynew[i-1]
    a, b = xnew[i], ynew[i]
    a2, b2 = xnew[i+1], ynew[i+1]
    assert a1 >= a
    assert b1 <= b
    assert b >= b1
    assert b <= b2
    if b1 == b or b2 == b:
        continue
    assert a <= a1
    assert a2 <= a
    assert b >= b1
    assert b2 >= b
    lambda_1 = (xnew-a)/(b-ynew)
    bounds = (lambda_1)
    sign = (b-ynew<0)
    lower = bounds[sign]
    upper = bounds[torch.logical_not(sign)]
    lower = lower[lower.isnan().logical_not()].max()
    upper = upper[upper.isnan().logical_not()].min()
    lambdaUpper[i] = upper/(1+upper)
    lambdaLower[i] = lower/(1+lower)
#    if lower >= upper:
 #       continue
  #  print("...")
    print("Point on curve", a, b, "lambda in range:", lower, upper)
    assert lower <= upper + 1e-4, (lower, upper, language)
    assert (a+lower*b <= (xnew + lower*ynew).min()+1e-5)
    assert (a+upper*b <= (xnew + upper*ynew).min()+1e-5)

lambdaUpper[-1] = lambdaLower[-2]
lambdaLower[0] = lambdaUpper[1]
print("lower", lambdaLower)
print("upper", lambdaUpper)


fOnFrontier = torch.FloatTensor([float(smoothed(x, y)) for x, y in zip(xnew, ynew)])


nextFrontierPoint = ((xnew-surprisalReal).pow(2) + (ynew-deplenReal).pow(2)).argmin()
lambda_optim_upper = float(lambdaUpper[nextFrontierPoint])
lambda_optim_lower = float(lambdaLower[nextFrontierPoint])

print(fOnFrontier)

# at which lambda is it the case that the 

distanceInLambdaSpace = (lambdaUpper-lambdaLower)
#assert distanceInLambdaSpace.sum() == 1, distanceInLambdaSpace.sum()
averageFunction = float((fOnFrontier * distanceInLambdaSpace).sum() / distanceInLambdaSpace.sum())
with open(f"output/{__file__}.tsv", "a") as outFile:
  print(language, "AverageFLambdaSpace", averageFunction, file=outFile)

  distanceInCurveSpace = ((xnew[1:] - xnew[:-1]).pow(2) + (ynew[1:] - ynew[:-1]).pow(2)).sqrt()
  averageFunction = float((fOnFrontier[1:] * distanceInCurveSpace).sum() / distanceInCurveSpace.sum())
  print(language, "AverageFCurveSpace", averageFunction, file=outFile)
  
  
  print(language, "FNextFrontier", float(fOnFrontier[nextFrontierPoint]), file=outFile)
  
  
  distanceToFrontier = float((((xnew-surprisalReal).pow(2) + (ynew-deplenReal).pow(2)).min()).sqrt())
  distanceToRandomMean = float(((surprisalReal-surprisal_baselines.mean()).pow(2) + (deplenReal-deplen_baselines.mean()).pow(2)).sqrt())
  
  
  print(language, "LambdaLower", lambda_optim_lower, file=outFile)
  print(language, "LambdaUpper", lambda_optim_upper, file=outFile)
  print(language, "DistanceRandom", distanceToRandomMean, file=outFile)
  print(language, "DistanceFrontier", distanceToFrontier, file=outFile)
  print(language, "SmoothedReal", float(smoothed(surprisalReal, deplenReal)), file=outFile)


#for i in range(1,sorted_frontier.size()[0]-1):
#    valueHere = sorted_frontier[i]
#
#
#    valuePrevious = sorted_frontier[i-1]
#    valueNext = sorted_frontier[i+1]
##    print(i, valueHere, "previous", valuePrevious,"next", valueNext)
#    assert valuePrevious[0] >= valueHere[0]
#    assert valuePrevious[1] <= valueHere[1]
#    assert valueHere[0] >= valueNext[0]
#    assert valueHere[1] <= valueNext[1]
#    a, b = [float(q) for q in valueHere]
#    a1, b1 = [float(q) for q in valuePrevious]
#    a2, b2 = [float(q) for q in valueNext]
#    if b1 == b or b2 == b:
#        continue
#    assert a <= a1
#    assert a2 <= a
#    assert b >= b1
#    assert b2 >= b
#    lambda_1 = (a1-a)/(b-b1)
#    lambda_2 = (a2-a)/(b-b2)
#    print(i, "\t", a, "\t", b, "\t", lambda_1, "\t", lambda_2, "\t", "Trade-off", a+lambda_1 *b, "at previous", a1 + lambda_1 *b1)
#
#    assert a+lambda_1 *b <= a1 + lambda_1 *b1+1e-5
#    assert a+lambda_2 *b <= a2 + lambda_2 *b2+1e-5, (a+lambda_2 *b , a2 + lambda_2 *b2)
#
#

    

import seaborn as sns
import matplotlib.pyplot as plt
#plt.show()



#fig, ax = plt.subplots()

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3, 3))

ax.scatter(surprisal, deplen, c=order-0.5, cmap='summer')
ax.scatter([surprisalReal], [deplenReal], s=80, c='r')

fig.savefig(f"figures/perLanguage/{__file__}_{language}_raw.pdf")

#plt.show()

#ax[1].scatter(surprisal, deplen, c=loo_smoothed(k_x, k_y)-0.5, cmap='summer')
#ax[1].scatter([surprisalReal], [deplenReal])

#ax[2].scatter((grid).view(-1, 2)[:,0], (grid).view(-1, 2)[:,1], c=smoothed_grid.view(-1)-0.5, cmap='viridis')
#ax[2].scatter(corners[:,0], corners[:,1], cmap='viridis')
#ax[2].scatter([surprisalReal], [deplenReal])
#
#ax[3].scatter((grid).view(-1, 2)[:,0], (grid).view(-1, 2)[:,1], c=smoothed_grid.view(-1)-0.5, cmap='viridis')
#ax[3].scatter([surprisalReal], [deplenReal])
#ax[3].scatter(grid_frontier_[:,0], grid_frontier_[:,1], cmap='viridis') # c=10*smoothed_frontier_-0.5, 
#ax[3].scatter([surprisalReal], [deplenReal])

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3, 3))

# TODO issue: values are sometimes > 1. some kind of bug!
ax.scatter((grid).view(-1, 2)[:,0], (grid).view(-1, 2)[:,1], c=smoothed_grid.view(-1)-0.5, cmap='viridis', vmin=-0.5, vmax=0.5)
sns.kdeplot(x=surprisal_baselines, y=deplen_baselines, ax=ax, color='white')
ax.scatter([surprisalReal], [deplenReal], s=80, c='r')

fig.savefig(f"figures/perLanguage/{__file__}_{language}_smoothed.pdf")


#plt.show()




#quit()
#with open(f"output/{__file__}.tsv", "a") as outFile:
#   print("\t".join([str(q) for q in [language, float(smoothed(surprisalReal, deplenReal))]]), file=outFile)



