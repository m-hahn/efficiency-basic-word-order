import torch
import sys

language = sys.argv[1] #"Korean_2.8"


with open("../raw_output/real.tsv", "r") as inFile:
    header = next(inFile).strip().split("\t")
    header = dict(list(zip(header, range(len(header)))))
    data = [x.split("\t") for x in inFile.read().strip().split("\n")]
data = [x for x in data if x[header["language"]] == language]
deplenReal = float(data[0][header["DepLen"]])
surprisalReal = float(data[0][header["Surprisal2"]])


with open("../raw_output/landscape.tsv", "r") as inFile:
    header = next(inFile).strip().split("\t")
    header = dict(list(zip(header, range(len(header)))))
    data = [x.split("\t") for x in inFile.read().strip().split("\n")]




data = [x for x in data if x[header["language"]] == language and x[header["basic"]] != ""]


surprisal = torch.FloatTensor([float(x[header["Surprisal2"]]) for x in data])
deplen = torch.FloatTensor([float(x[header["DepLen"]]) for x in data])

print(surprisal)

surprisalRange = (surprisal.min(), surprisal.max())
print(surprisalRange)
deplenRange = (deplen.min(), deplen.max())
print(deplenRange)

deplenReal -= deplenRange[0]
deplenReal /= deplenRange[1] - deplenRange[0]
surprisalReal -= surprisalRange[0]
surprisalReal /= surprisalRange[1] - surprisalRange[0]

assert deplenReal <= 2, deplenReal
assert surprisalReal <= 2, surprisalReal


surprisal = surprisal - surprisalRange[0]
surprisal /= surprisal.max()
deplen = deplen - deplenRange[0]
deplen /= deplen.max()

surprisalRange = (0,1)
deplenRange = (0,1)



order = [x[header["basic"]] for x in data]
order = torch.FloatTensor([{"SOV" : 1, "SVO" : 0, "OSV" : 1}[x] for x in order])



print(surprisal.size())


def loo_smoothed(k_x, k_y):
    d_x = (surprisal.unsqueeze(1) - surprisal.unsqueeze(0)).pow(2)
    d_y = (deplen.unsqueeze(1) - deplen.unsqueeze(0)).pow(2)
    logit = (k_x * d_x + k_y * d_y)
 #   print(logit)
#    print(1e10 * torch.eye(surprisal.size()[0]))
    logit -= 1e10 * torch.eye(surprisal.size()[0])
    kernel = torch.exp(logit) / torch.exp(logit).sum(dim=1).unsqueeze(1)
    smoothed = (kernel * order.unsqueeze(0)).sum(dim=1)
    return smoothed

def optimize():
  k_x = torch.FloatTensor([0])
  k_y = torch.FloatTensor([0])
  k_x.requires_grad = True
  k_y.requires_grad = True
  
  optim = torch.optim.Adam([k_x, k_y], lr=0.001)
  
  lastLoss = 100
  for iteration in range(100000):
    prediction = loo_smoothed(k_x, k_y)
    loss = (prediction - order).pow(2).mean() #+ 0.001 * (k_x.pow(2) + k_y.pow(2))
    if iteration % 10 == 0:
      print(iteration, "LOSS", loss, k_x, k_y)
    if iteration % 10000 == 0:
        if lastLoss - float(loss) < 0.0001:
            break
        lastLoss = float(loss)
    loss.backward()
    optim.step()
    optim.zero_grad()
  return float(k_x), float(k_y)

grid = ([[[-0.05 + i/100 * 1.1  ,-0.05 + j/100 *  1.1] for j in range(100)] for i in range(100)])

dominated = torch.ByteTensor(100,100)

for i in range(100):
    for j in range(100):
        better = torch.logical_and((surprisal < grid[i][j][0]), (deplen < grid[i][j][1])).any()
  #      print(better)
        dominated[i,j] = better
 #       print(better)
#        quit()

frontier = torch.ByteTensor(100,100)
for i in range(100):
    for j in range(100):
        if i<99 and j<99 and not dominated[i,j] and (dominated[i+1,j+1]):
           frontier[i,j] = True
        else:
           frontier[i,j] = False
print(frontier)
print(frontier.sum())


print(dominated)

#for i in range(surprisal.size()[0]):
#for row in grid:
#    for x, y in row:
#   surp = surprisal[i]
#   dep = deplen[i]
#   print(surp, dep)
#   quit()



#k_x, k_y = optimize() #-90.0241
k_x, k_y = -4.28, -21.7
#k_y = -1.5454


def smoothed(x, y):
    d_x = (surprisal - x).pow(2)
    d_y = (deplen - y).pow(2)
    logit = (k_x * d_x + k_y * d_y)
#    print(k_x, k_y, d_x, d_y)
 #   print("Logit", logit)
    kernel = torch.exp(logit) / torch.exp(logit).sum().unsqueeze(0)
  #  print("Kernel", kernel)
    smoothed = (kernel * order).sum(dim=0)
    return smoothed

print( "Order", order)
print("Smoothed Real", smoothed(surprisalReal, deplenReal), language)
#quit()


#print(smoothed(4.6, 2.6))

print("PREDICTIVE FOR KNOWN POINTS")
print(loo_smoothed(k_x, k_y))

#print(torch.FloatTensor(grid))

smoothed_grid = torch.FloatTensor([[smoothed(x,y) for x,y in z] for z in grid])
print("SMOOTHED GRID")
print(smoothed_grid)

import numpy as np
import matplotlib.pyplot as plt

X = smoothed_grid


#ax.imshow(X) #, interpolation='nearest')
print(surprisal.size(), deplen.size(), order.size())
grid_ = torch.FloatTensor(grid).view(-1, 2)
dominated_ = dominated.view(-1)
grid_ = grid_[dominated_]
smoothed_grid_ = smoothed_grid.view(-1)[dominated_]


frontier_ = frontier.view(-1)
grid_frontier_ = torch.FloatTensor(grid).view(-1,2)[frontier_]
smoothed_frontier_ = smoothed_grid.view(-1)[frontier_]

sortedFrontier = sorted(range(grid_frontier_.size()[0]), key=lambda x:tuple((lambda w: (-w[0], w[1]))([float(q) for q in grid_frontier_[x]])))
#sortedFrontier = sorted(range(grid_frontier_.size()[0]), key=lambda x:tuple([float(q) for q in grid_frontier_[x]]))
sorted_frontier = grid_frontier_[sortedFrontier]
print(sortedFrontier)
print(sorted_frontier)
sorted_frontier_all = sorted_frontier
sorted_frontier = [sorted_frontier_all[0]]
for i in range(1, sorted_frontier_all.size()[0]-1):
    if sorted_frontier_all[i][0] == sorted_frontier[-1][0]:
        continue
    if sorted_frontier_all[i][1] == sorted_frontier[-1][1]:
        continue
    sorted_frontier.append(sorted_frontier_all[i])
sorted_frontier = torch.stack(sorted_frontier)
for i in range(1,sorted_frontier.size()[0]-1):
    valueHere = sorted_frontier[i]


    valuePrevious = sorted_frontier[i-1]
    valueNext = sorted_frontier[i+1]
#    print(i, valueHere, "previous", valuePrevious,"next", valueNext)
    assert valuePrevious[0] >= valueHere[0]
    assert valuePrevious[1] <= valueHere[1]
    assert valueHere[0] >= valueNext[0]
    assert valueHere[1] <= valueNext[1]
    a, b = [float(q) for q in valueHere]
    a1, b1 = [float(q) for q in valuePrevious]
    a2, b2 = [float(q) for q in valueNext]
    if b1 == b or b2 == b:
        continue
    assert a <= a1
    assert a2 <= a
    assert b >= b1
    assert b2 >= b
    lambda_1 = (sorted_frontier[:,0]-a)/(b-sorted_frontier[:,1])
    bounds = (lambda_1)
    sign = (b-sorted_frontier[:,1]<0)
    lower = bounds[sign]
    upper = bounds[torch.logical_not(sign)]
    lower = lower[lower.isnan().logical_not()].max()
    upper = upper[upper.isnan().logical_not()].min()
    if lower >= upper:
        continue
    print("...")
    print("Point on curve", a, b, "lambda in range:", lower, upper)
    assert (a+lower*b <= (sorted_frontier[:,0] + lower*sorted_frontier[:,1]).min()+1e-5)
    assert (a+upper*b <= (sorted_frontier[:,0] + upper*sorted_frontier[:,1]).min()+1e-5)
#    quit()
 #   lambda_2 = (a2-a)/(b-b2)
  #  print(i, "\t", a, "\t", b, "\t", lambda_1, "\t", lambda_2, "\t", "Trade-off", a+lambda_1 *b, "at previous", a1 + lambda_1 *b1)

   # assert a+lambda_1 *b <= a1 + lambda_1 *b1+1e-5
    #assert a+lambda_2 *b <= a2 + lambda_2 *b2+1e-5, (a+lambda_2 *b , a2 + lambda_2 *b2)

###################
# other option: 
# - look at frontier among the raw points
# - smooth the frontier between the points (spline)

quit()



#for i in range(1,sorted_frontier.size()[0]-1):
#    valueHere = sorted_frontier[i]
#
#
#    valuePrevious = sorted_frontier[i-1]
#    valueNext = sorted_frontier[i+1]
##    print(i, valueHere, "previous", valuePrevious,"next", valueNext)
#    assert valuePrevious[0] >= valueHere[0]
#    assert valuePrevious[1] <= valueHere[1]
#    assert valueHere[0] >= valueNext[0]
#    assert valueHere[1] <= valueNext[1]
#    a, b = [float(q) for q in valueHere]
#    a1, b1 = [float(q) for q in valuePrevious]
#    a2, b2 = [float(q) for q in valueNext]
#    if b1 == b or b2 == b:
#        continue
#    assert a <= a1
#    assert a2 <= a
#    assert b >= b1
#    assert b2 >= b
#    lambda_1 = (a1-a)/(b-b1)
#    lambda_2 = (a2-a)/(b-b2)
#    print(i, "\t", a, "\t", b, "\t", lambda_1, "\t", lambda_2, "\t", "Trade-off", a+lambda_1 *b, "at previous", a1 + lambda_1 *b1)
#
#    assert a+lambda_1 *b <= a1 + lambda_1 *b1+1e-5
#    assert a+lambda_2 *b <= a2 + lambda_2 *b2+1e-5, (a+lambda_2 *b , a2 + lambda_2 *b2)
#
#

    


#fig, ax = plt.subplots()

fig, ax = plt.subplots(nrows=1, ncols=5, figsize=(10, 3))

ax[0].scatter(surprisal, deplen, c=order-0.5, cmap='summer')
ax[0].scatter([surprisalReal], [deplenReal])
ax[1].scatter(surprisal, deplen, c=loo_smoothed(k_x, k_y)-0.5, cmap='summer')
ax[1].scatter([surprisalReal], [deplenReal])

ax[2].scatter(grid_[:,0], grid_[:,1], c=smoothed_grid_-0.5, cmap='viridis')
ax[2].scatter([surprisalReal], [deplenReal])

ax[3].scatter(torch.FloatTensor(grid).view(-1, 2)[:,0], torch.FloatTensor(grid).view(-1, 2)[:,1], c=smoothed_grid.view(-1)-0.5, cmap='viridis')
ax[3].scatter([surprisalReal], [deplenReal])


ax[3].scatter(grid_frontier_[:,0], grid_frontier_[:,1], c=10*smoothed_frontier_-0.5, cmap='viridis')
ax[3].scatter([surprisalReal], [deplenReal])





plt.show()



print("Smoothed Real", smoothed(surprisalReal, deplenReal), language)

quit()
#with open(f"output/{__file__}.tsv", "a") as outFile:
#   print("\t".join([str(q) for q in [language, float(smoothed(surprisalReal, deplenReal))]]), file=outFile)



