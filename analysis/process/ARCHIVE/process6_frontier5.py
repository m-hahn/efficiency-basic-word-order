import torch
import sys

language = sys.argv[1] #"Korean_2.8"


with open("../raw_output/real.tsv", "r") as inFile:
    header = next(inFile).strip().split("\t")
    header = dict(list(zip(header, range(len(header)))))
    data = [x.split("\t") for x in inFile.read().strip().split("\n")]
data = [x for x in data if x[header["language"]] == language]
deplenReal = float(data[0][header["DepLen"]])
surprisalReal = float(data[0][header["Surprisal2"]])


with open("../raw_output/landscape.tsv", "r") as inFile:
    header = next(inFile).strip().split("\t")
    header = dict(list(zip(header, range(len(header)))))
    data = [x.split("\t") for x in inFile.read().strip().split("\n")]




data = [x for x in data if x[header["language"]] == language and x[header["basic"]] != ""]


surprisal = torch.FloatTensor([float(x[header["Surprisal2"]]) for x in data])
deplen = torch.FloatTensor([float(x[header["DepLen"]]) for x in data])

print(surprisal)

surprisalRange = (surprisal.min(), surprisal.max())
print(surprisalRange)
deplenRange = (deplen.min(), deplen.max())
print(deplenRange)

deplenReal -= deplenRange[0]
deplenReal /= deplenRange[1] - deplenRange[0]
surprisalReal -= surprisalRange[0]
surprisalReal /= surprisalRange[1] - surprisalRange[0]

assert deplenReal <= 2, deplenReal
assert surprisalReal <= 2, surprisalReal


surprisal = surprisal - surprisalRange[0]
surprisal /= surprisal.max()
deplen = deplen - deplenRange[0]
deplen /= deplen.max()

surprisalRange = (0,1)
deplenRange = (0,1)



order = [x[header["basic"]] for x in data]
order = torch.FloatTensor([{"SOV" : 1, "SVO" : 0, "OSV" : 1}[x] for x in order])



print(surprisal.size())


def loo_smoothed(k_x, k_y):
    d_x = (surprisal.unsqueeze(1) - surprisal.unsqueeze(0)).pow(2)
    d_y = (deplen.unsqueeze(1) - deplen.unsqueeze(0)).pow(2)
    logit = (k_x * d_x + k_y * d_y)
 #   print(logit)
#    print(1e10 * torch.eye(surprisal.size()[0]))
    logit -= 1e10 * torch.eye(surprisal.size()[0])
    kernel = torch.exp(logit) / torch.exp(logit).sum(dim=1).unsqueeze(1)
    smoothed = (kernel * order.unsqueeze(0)).sum(dim=1)
    return smoothed

def optimize():
  k_x = torch.FloatTensor([0])
  k_y = torch.FloatTensor([0])
  k_x.requires_grad = True
  k_y.requires_grad = True
  
  optim = torch.optim.Adam([k_x, k_y], lr=0.001)
  
  lastLoss = 100
  for iteration in range(100000):
    prediction = loo_smoothed(k_x, k_y)
    loss = (prediction - order).pow(2).mean() #+ 0.001 * (k_x.pow(2) + k_y.pow(2))
    if iteration % 10 == 0:
      print(iteration, "LOSS", loss, k_x, k_y)
    if iteration % 10000 == 0:
        if lastLoss - float(loss) < 0.0001:
            break
        lastLoss = float(loss)
    loss.backward()
    optim.step()
    optim.zero_grad()
  return float(k_x), float(k_y)

grid = ([[[-0.05 + i/100 * 1.1  ,-0.05 + j/100 *  1.1] for j in range(100)] for i in range(100)])

N = surprisal.size()[0]
dominated = torch.ByteTensor(N)

for i in range(N):
   better = torch.logical_and((surprisal < surprisal[i]), (deplen < deplen[i])).any()
   dominated[i] = better


#k_x, k_y = optimize() #-90.0241
k_x, k_y = -4.28, -21.7
#k_y = -1.5454


def smoothed(x, y):
    d_x = (surprisal - x).pow(2)
    d_y = (deplen - y).pow(2)
    logit = (k_x * d_x + k_y * d_y)
#    print(k_x, k_y, d_x, d_y)
 #   print("Logit", logit)
    kernel = torch.exp(logit) / torch.exp(logit).sum().unsqueeze(0)
  #  print("Kernel", kernel)
    smoothed = (kernel * order).sum(dim=0)
    return smoothed

print( "Order", order)
print("Smoothed Real", smoothed(surprisalReal, deplenReal), language)
#quit()


#print(smoothed(4.6, 2.6))

print("PREDICTIVE FOR KNOWN POINTS")
print(loo_smoothed(k_x, k_y))

#print(torch.FloatTensor(grid))

smoothed_grid = torch.FloatTensor([[smoothed(x,y) for x,y in z] for z in grid])
print("SMOOTHED GRID")
print(smoothed_grid)

import numpy as np
import matplotlib.pyplot as plt

X = smoothed_grid


#ax.imshow(X) #, interpolation='nearest')
print(surprisal.size(), deplen.size(), order.size())
grid_ = torch.stack([surprisal, deplen], dim=1)
dominated_ = dominated.view(-1)
grid_ = grid_[torch.logical_not(dominated_)]


frontier_ = grid_
grid_frontier_ = grid_ #torch.FloatTensor(grid).view(-1,2)[frontier_]


# Need better smoothing to get bijection between lambda and frontier

sortedFrontier = sorted(range(grid_frontier_.size()[0]), key=lambda x:tuple((lambda w: (-w[0], w[1]))([float(q) for q in grid_frontier_[x]])))
#sortedFrontier = sorted(range(grid_frontier_.size()[0]), key=lambda x:tuple([float(q) for q in grid_frontier_[x]]))
sorted_frontier = grid_frontier_[sortedFrontier]
print(sortedFrontier)
print(sorted_frontier)
sorted_frontier_all = sorted_frontier
sorted_frontier = [sorted_frontier_all[0]]
for i in range(1, sorted_frontier_all.size()[0]):
    if sorted_frontier_all[i][0] == sorted_frontier[-1][0]:
        continue
    if sorted_frontier_all[i][1] == sorted_frontier[-1][1]:
        continue
    sorted_frontier.append(sorted_frontier_all[i])
sorted_frontier = torch.stack(sorted_frontier)

corners = [[float(q) for q in sorted_frontier[0]]]

for i in range(1,sorted_frontier.size()[0]-1):
    valueHere = sorted_frontier[i]


    valuePrevious = sorted_frontier[i-1]
    valueNext = sorted_frontier[i+1]
#    print(i, valueHere, "previous", valuePrevious,"next", valueNext)
    assert valuePrevious[0] >= valueHere[0]
    assert valuePrevious[1] <= valueHere[1]
    assert valueHere[0] >= valueNext[0]
    assert valueHere[1] <= valueNext[1]
    a, b = [float(q) for q in valueHere]
    a1, b1 = [float(q) for q in valuePrevious]
    a2, b2 = [float(q) for q in valueNext]
    if b1 == b or b2 == b:
        continue
    assert a <= a1
    assert a2 <= a
    assert b >= b1
    assert b2 >= b
    lambda_1 = (sorted_frontier[:,0]-a)/(b-sorted_frontier[:,1])
    bounds = (lambda_1)
    sign = (b-sorted_frontier[:,1]<0)
    lower = bounds[sign]
    upper = bounds[torch.logical_not(sign)]
    lower = lower[lower.isnan().logical_not()].max()
    upper = upper[upper.isnan().logical_not()].min()
    if lower >= upper:
        continue
    print("...")
    print("Point on curve", a, b, "lambda in range:", lower, upper)
    corners.append([a,b])
    assert (a+lower*b <= (sorted_frontier[:,0] + lower*sorted_frontier[:,1]).min()+1e-5)
    assert (a+upper*b <= (sorted_frontier[:,0] + upper*sorted_frontier[:,1]).min()+1e-5)
#    quit()
 #   lambda_2 = (a2-a)/(b-b2)
  #  print(i, "\t", a, "\t", b, "\t", lambda_1, "\t", lambda_2, "\t", "Trade-off", a+lambda_1 *b, "at previous", a1 + lambda_1 *b1)

   # assert a+lambda_1 *b <= a1 + lambda_1 *b1+1e-5
    #assert a+lambda_2 *b <= a2 + lambda_2 *b2+1e-5, (a+lambda_2 *b , a2 + lambda_2 *b2)

###################
# other option: 
# - look at frontier among the raw points
# - smooth the frontier between the points (spline)

corners.append([float(q) for q in sorted_frontier[-1]])

#quit()
corners = torch.FloatTensor(corners)
print("Corners", corners)

import numpy as np

import matplotlib.pyplot as plt

from scipy import interpolate

print("Corners")
print([float(x[0]) for x in corners])
print([float(x[1]) for x in corners])

tck = interpolate.splrep([float(x[0]) for x in corners][::-1], [float(x[1]) for x in corners][::-1], k=2)


xnew = np.arange(corners[-1][0], corners[0][0], (corners[0][0]-corners[-1][0])/50)
xnew = np.array([])
for i in range(len(corners)-1, 0, -1):
    print(xnew)
    print((np.arange(corners[i][0], corners[i-1][0], (corners[i-1][0]-corners[i][0])/10)))
    xnew = np.concatenate([xnew, (np.arange(corners[i][0], corners[i-1][0], (corners[i-1][0]-corners[i][0])/10))], axis=0)
#xnew = xnew[::-1]

ynew = interpolate.splev(xnew, tck, der=0)


a = torch.zeros(corners.size()[0]-1)+0.1
#c = torch.zeros(corners.size()[0])
a.requires_grad=True
#c.requires_grad=True

optim = torch.optim.Adam([a],lr=0.001)

for it in range(20000):
    loss = 0
    deltaX = corners[1:,0] - corners[:-1,0]
    deltaY = corners[1:,1] - corners[:-1,1]

#    print(deltaY.size(), a.size(), deltaX.size())
    b = (deltaY - a * deltaX.pow(2)) / deltaX
    assert ((b * deltaX + a * deltaX.pow(2)) - deltaY).abs().max() < 0.001
 #   print(b)
  #  print(a)
    #quit()
    derivativeAtEnd = 2*a*deltaX + b
    print("Ratio for the derivatives at the junctures", derivativeAtEnd[:-1]/b[1:])
    loss += 200 * torch.log((derivativeAtEnd[:-1] / b[1:]).abs()).abs().sum()

    # Derivative: has to be negative always
    loss += 200 * b.clamp(min=0).sum()
    loss += 200 * derivativeAtEnd.clamp(min=0).sum()

#    loss += a.abs().sum()
    print(it, "Loss", loss, a)
    optim.zero_grad()
    loss.backward()
    optim.step()
    a.data = a.data.clamp(min=0.001)

xnew = torch.FloatTensor([])
ynew = torch.FloatTensor([])
for i in range(corners.size()[0]-1):
    xnew_ = torch.FloatTensor(np.arange(corners[i][0], corners[i+1][0], (corners[i+1][0] - corners[i][0])/5))
    xnew__ = xnew_ - corners[i][0]
#    print(xnew__)
    deltaX = corners[i+1][0] - corners[i][0]
    deltaY = corners[i+1][1] - corners[i][1]
    ynew_ = corners[i][1] + b[i] * xnew__ + a[i] * xnew__.pow(2)
    xnew = torch.cat([xnew, xnew_], dim=0).detach()
    ynew = torch.cat([ynew, ynew_], dim=0).detach()
    print("Point", i, "Quadratic part", a[i], "Linear part", b[i])


#for i in range(1,sorted_frontier.size()[0]-1):
#    valueHere = sorted_frontier[i]
#
#
#    valuePrevious = sorted_frontier[i-1]
#    valueNext = sorted_frontier[i+1]
##    print(i, valueHere, "previous", valuePrevious,"next", valueNext)
#    assert valuePrevious[0] >= valueHere[0]
#    assert valuePrevious[1] <= valueHere[1]
#    assert valueHere[0] >= valueNext[0]
#    assert valueHere[1] <= valueNext[1]
#    a, b = [float(q) for q in valueHere]
#    a1, b1 = [float(q) for q in valuePrevious]
#    a2, b2 = [float(q) for q in valueNext]
#    if b1 == b or b2 == b:
#        continue
#    assert a <= a1
#    assert a2 <= a
#    assert b >= b1
#    assert b2 >= b
#    lambda_1 = (a1-a)/(b-b1)
#    lambda_2 = (a2-a)/(b-b2)
#    print(i, "\t", a, "\t", b, "\t", lambda_1, "\t", lambda_2, "\t", "Trade-off", a+lambda_1 *b, "at previous", a1 + lambda_1 *b1)
#
#    assert a+lambda_1 *b <= a1 + lambda_1 *b1+1e-5
#    assert a+lambda_2 *b <= a2 + lambda_2 *b2+1e-5, (a+lambda_2 *b , a2 + lambda_2 *b2)
#
#

    


#fig, ax = plt.subplots()

fig, ax = plt.subplots(nrows=1, ncols=5, figsize=(10, 3))

ax[0].scatter(surprisal, deplen, c=order-0.5, cmap='summer')
ax[0].scatter([surprisalReal], [deplenReal])
ax[1].scatter(surprisal, deplen, c=loo_smoothed(k_x, k_y)-0.5, cmap='summer')
ax[1].scatter([surprisalReal], [deplenReal])

ax[2].scatter(torch.FloatTensor(grid).view(-1, 2)[:,0], torch.FloatTensor(grid).view(-1, 2)[:,1], c=smoothed_grid.view(-1)-0.5, cmap='viridis')
ax[2].scatter(corners[:,0], corners[:,1], cmap='viridis')
ax[2].scatter([surprisalReal], [deplenReal])

ax[3].scatter(torch.FloatTensor(grid).view(-1, 2)[:,0], torch.FloatTensor(grid).view(-1, 2)[:,1], c=smoothed_grid.view(-1)-0.5, cmap='viridis')
ax[3].scatter([surprisalReal], [deplenReal])
ax[3].scatter(grid_frontier_[:,0], grid_frontier_[:,1], cmap='viridis') # c=10*smoothed_frontier_-0.5, 
ax[3].scatter([surprisalReal], [deplenReal])


ax[4].scatter(torch.FloatTensor(grid).view(-1, 2)[:,0], torch.FloatTensor(grid).view(-1, 2)[:,1], c=smoothed_grid.view(-1)-0.5, cmap='viridis')
#ax[0].scatter(surprisal, deplen, c=order-0.5, cmap='summer')
ax[4].scatter(xnew, ynew) # c=10*smoothed_frontier_-0.5, 
#ax[4].scatter(grid_frontier_[:,0], grid_frontier_[:,1], cmap='viridis') # c=10*smoothed_frontier_-0.5, 
ax[4].scatter([surprisalReal], [deplenReal])



plt.show()



print("Smoothed Real", smoothed(surprisalReal, deplenReal), language)

quit()
#with open(f"output/{__file__}.tsv", "a") as outFile:
#   print("\t".join([str(q) for q in [language, float(smoothed(surprisalReal, deplenReal))]]), file=outFile)



