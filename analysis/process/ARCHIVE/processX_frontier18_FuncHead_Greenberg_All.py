import sys
script = sys.argv[1]
import subprocess
import random
from ud_languages_28 import languages
#random.shuffle(languages)
languages = sorted(languages)
for lang in languages:
    subprocess.call(["python3", f"process{script}_frontier18_FuncHead_Greenberg.py", lang])
