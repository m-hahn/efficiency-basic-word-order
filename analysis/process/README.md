# Analyze Efficiency Planes

## Interpolating and visualizing

These scripts implement the procedures described in SI Section 3.

* [Script 2](process9_frontier18.py) Used in [these scripts](change/visualize/)
* [Script 3](process9_frontier18_Revised.py) Used in [these scripts](change/)

## Optimality
Used by [figures/perLanguage/together/optimality.R](this downstream script).
* [Summarise](process9_frontier19.py)

## Visualization
* [Visualiation](process9_frontier18_Viz.py)
* [Visualization for baselines](process9_frontier18_Baselines_Viz.py)


## Using Fitted Grammars
* [Processing for fitted grammars](process9_frontier18_Fitted.py)

## Neural Estimators
* [AUC](process9_frontier18_Neural_AUC.py)
* [AUC (Visualization)](process9_frontier18_Neural_AUC_Viz.py)
* [I1](process9_frontier18_Neural.py)
* [I1 (Visualization)](process9_frontier18_Neural_Viz.py)

## Subset (SI Section S27)
* [Compute frontier for subsets](process9_frontier18_SUBSET.py)
* [Create per-language visualizations](process9_frontier18_SUBSET_Viz.py)

