# Phylogenetic Analyses

* [Simple Brownian Model](brownian-stepping-stone)
* [Including areal convergence](frontier_geographic_historical)
* [Model with latent variables computed explicitly](frontier_historical)
* [Model with latent variables marginalized out](frontier_noLatent_historical)
* [Visuaalization scripts](visualize)
