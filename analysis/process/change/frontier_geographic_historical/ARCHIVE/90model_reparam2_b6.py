# reparam2
# NO DIVERGENCES


import pystan
from math import sqrt, log
import math
import sys

group = "process9_frontier18_Revised" #sys.argv[1]
#variable = sys.argv[2]

with open("../geolocations.tsv", "r") as inFile:
  geolocations = [x.split("\t") for x in inFile.read().strip().split("\n")][1:]
print(geolocations)
languages = [x[1] for x in geolocations]
latitudes = [float(x[4]) for x in geolocations]
longitudes = [float(x[5]) for x in geolocations]
latitudes = dict(list(zip(languages, latitudes)))
longitudes = dict(list(zip(languages, longitudes)))

latitudes["PARTITION-Icelandic_modern"] = latitudes["Icelandic_2.8"]
longitudes["PARTITION-Icelandic_modern"] = longitudes["Icelandic_2.8"]

with open("trees2_2.8.tsv", "r") as inFile:
  trees = [x.split("\t") for x in inFile.read().strip().split("\n")][1:]
print(trees)

observedLangs = set()
allLangs = set()
parents = {}
for line in trees:
   observedLangs.add(line[0])
   allLangs.add(line[0])
   for i in range(len(line)-1):
       child = line[i]
       parent = line[i+1]
       if child in parents:
         assert parents[child] == parent, (parents[child], line, child, i)
       parents[child] = parent
       allLangs.add(parent)

assert "ISWOC_Old_English" in observedLangs
with open("groups2_2.8.tsv", "r") as inFile:
  dates = [x.split("\t") for x in inFile.read().strip().split("\n")][1:]
print([len(x) for x in dates])
dates = dict(dates)
dates["_ROOT_"] = -50000
print(dates)
for x in allLangs:
  if x not in observedLangs:
    if x not in dates:
       print(x)

import glob
assert group.startswith("process")
pareto_distance_x  = {x[0] : float(x[2]) for x in [x.split(" ") for x in open(f"../../output/{group}.py.tsv", "r").read().strip().split("\n")] if x[1] == "Distance_Surprisal_ComparedFrontier" }
pareto_distance_y  = {x[0] : float(x[2]) for x in [x.split(" ") for x in open(f"../../output/{group}.py.tsv", "r").read().strip().split("\n")] if x[1] == "Distance_DepLen_ComparedFrontier" }
average_f  = {x[0] : float(x[2]) for x in [x.split(" ") for x in open(f"../../output/{group}.py.tsv", "r").read().strip().split("\n")] if x[1] == "AverageFAngleSpace" }


fOnFrontier = {}
for f in glob.glob(f"../../output_frontier/{group}.py_*_fOnFrontierDiscretizedByAngle.tsv"):
  with open(f, "r") as inFile:
    func = [x.split(" ") for x in inFile.read().strip().split("\n")]
    assert len(func) == 10
    func = [float(x[1]) for x in func]
    language = f[f.index(".py")+4:f.rfind("_")]
    print(language)
    fOnFrontier[language] = func
print(fOnFrontier)

with open("../../../../languages/extract_congruence/output/evaluateCongruence_POS_NoSplit.py.tsv", "r") as inFile:
   data = [x.replace('"', '').split("\t") for x in inFile.read().strip().split("\n")]
header = data[0]
header = dict(list(zip(header, range(len(header)))))
data = data[1:]
print(header)
valueByLanguage = {}
for line in data:
   print(line)
   language = line[header["Language"]]
 
   #assert language in parents, language
   print(line)
   if language not in fOnFrontier:
     print("MISSING LANGUAGE", language)
     continue
   x = float('nan')
   y = float('nan')
#   assert y == round(y)
   z = float(line[header["Congruence_All"]])
   valueByLanguage[language] = [x,y,z] 

#valueByLanguage["ISWOC_Old_English"] = [0, 0, 0.49]
#valueByLanguage["Archaic_Greek"] = [0, 0, 0.56]
#valueByLanguage["Classical_Greek"] = [0, 0, 0.52]
#valueByLanguage["Koine_Greek"] = [0, 0, 0.47]

print(valueByLanguage)

print(observedLangs)
print(allLangs)
hiddenLangs = sorted([x for x in allLangs if x not in observedLangs and x != "_ROOT_"])
print(hiddenLangs)

#observedLanguages = [x for x in list(observedLangs) if parents[x] not in observedLangs] # This is for understanding what the model does on only synchronic data


with open("case_marking_revised_2.8.tsv", "r") as inFile:
   case_marking = [x.replace('"', "").split(",") for x in inFile.read().strip().split("\n")]
case_marking = {x[2] : True if x[3] == "TRUE" else False for x in case_marking[1:]}

with open("case_marking_groups_2.8.tsv", "r") as inFile:
   for line in inFile:
      x, y = line.strip().split("\t")
      assert x not in case_marking
      case_marking[x] = True if y == "TRUE" else False

def caseMarking(lang):
    if lang in case_marking:
      return case_marking[lang]
    if "_Greek" in lang:
      return True
    assert False, lang


itos_categorical = ["Case", "No Case"]
print(itos_categorical)
stoi_categorical = dict(list(zip(itos_categorical, range(len(itos_categorical)))))

observedLanguages = sorted([x for x in list(observedLangs) if x in valueByLanguage])
hiddenLanguages = hiddenLangs
totalLanguages = ["_ROOT_"] + hiddenLanguages + observedLanguages
lang2Code = dict(list(zip(totalLanguages, range(len(totalLanguages)))))
lang2Observed = dict(list(zip(observedLanguages, range(len(observedLanguages)))))
assert "ISWOC_Old_English" in observedLanguages, observedLanguages
assert "ISWOC_Old_English" in totalLanguages, totalLanguages


assert 'Latin_2.8' in valueByLanguage, valueByLanguage
assert 'Latin_2.8' in totalLanguages
assert 'Latin_2.8' in lang2Code

distanceToParent = {}
for language in allLangs:
   parent = parents.get(language, "_ROOT_")
   if language in observedLangs and parent == "_ROOT_":
     print("ROOT", language)
   dateLang = dates.get(language, 2000)
   dateParent = dates[parent]
   distanceToParent[language] = (float(dateLang)-float(dateParent))/1000
print(distanceToParent)
print(parents.get("Classical_Chinese_2.8"))
assert "Classical_Chinese_2.8" in observedLangs
#quit()

from collections import defaultdict
fromParentsToDescendants = defaultdict(list)
for lang, parent in parents.items():
   fromParentsToDescendants[parent].append(lang)
fromParentsToDescendants["_ROOT_"] = []
for lang in fromParentsToDescendants:
    if lang not in parents and lang != "_ROOT_":
        fromParentsToDescendants["_ROOT_"].append(lang)
print(fromParentsToDescendants)

def mean(x):
    return sum(x)/len(x)

done = set()
def getGeolocation(lang):
   if lang in done:
       return
   for lang2 in fromParentsToDescendants[lang]:
       assert lang2 != lang, lang
       getGeolocation(lang2)
   if lang not in latitudes:
     print(lang, fromParentsToDescendants[lang])
     latitudes[lang] = mean([latitudes[x] for x in fromParentsToDescendants[lang]])
     longitudes[lang] = mean([longitudes[x] for x in fromParentsToDescendants[lang]])
   done.add(lang)

getGeolocation("_ROOT_")
print(latitudes)
print(longitudes)

totalLanguages = ["_ROOT_"] + sorted(hiddenLanguages) + sorted(observedLanguages)

import geopy.distance


kernelTime = [[0 for _ in range(len(totalLanguages))] for _ in range(len(totalLanguages))]
for i in range(len(kernelTime)):
   for j in range(i):
     l1 = totalLanguages[i]
     l2 = totalLanguages[j]
     d1 = int(dates.get(l1, 2000))
     d2 = int(dates.get(l2, 2000))
     kernelTime[i][j] = abs(d1-d2)/1000.0
     kernelTime[j][i] = abs(d1-d2)/1000.0

kernel = [[0 for _ in range(len(totalLanguages))] for _ in range(len(totalLanguages))]
for i in range(len(kernel)):
   for j in range(i):
     l1 = totalLanguages[i]
     l2 = totalLanguages[j]
     lat1, long1 = latitudes[l1], longitudes[l1]
     lat2, long2 = latitudes[l2], longitudes[l2]
     distance = geopy.distance.geodesic((lat1, long1), (lat2, long2)).km/10000
#     print(lat1, long1, lat2, long2, l1, l2, geopy.distance.geodesic((lat1, long1), (lat2, long2)).km/10000)
     kernel[i][j] = distance
     kernel[j][i] = distance
     if distance == 0 and kernelTime[i][j] < 0.1:
        print(l1, l2, kernelTime[i][j], int(dates.get(l1, 2000)), int(dates.get(l2, 2000)), abs(int(dates.get(l1, 2000)) -  int(dates.get(l2, 2000)))/1000.0)
#quit()
print(kernel[5][5])
print(kernel[8][8])
dat = {}

assert len(set(observedLanguages)) == len(observedLanguages)

assert (all(["Greek" in x for x in observedLanguages if x not in pareto_distance_y])), [x for x in observedLanguages if x not in pareto_distance_y]

dat["ObservedN"] = len(observedLanguages)
#dat["LogitsObserved"] = [float(valueByLanguage[x][0])*2-1 for x in observedLanguages]
#dat["TraitObserved"] = [valueByLanguage[x][2]*2-1 for x in observedLanguages]
dat["FFrontierObserved"] = [[valueByLanguage[x][2]*2-1, average_f[x]*2-1] for x in observedLanguages]
#for x in dat["FFrontierObserved"]:
#   print(x)
#import torch
#print(torch.FloatTensor(dat["FFrontierObserved"]).size())
#quit()
dat["HiddenN"] = len(hiddenLanguages)+1
dat["TotalN"] = dat["ObservedN"] + dat["HiddenN"]
assert dat["TotalN"] == len(totalLanguages)
dat["IsHidden"] = [1]*dat["HiddenN"] + [0]*dat["ObservedN"]
dat["ParentIndex"] = [0] + [1+lang2Code[parents.get(x, "_ROOT_")] for x in hiddenLanguages+observedLanguages]
dat["Total2Observed"] = [0]*dat["HiddenN"] + list(range(1,1+len(observedLanguages)))
dat["Total2Hidden"] = [1] + list(range(2,2+len(hiddenLanguages))) + [0 for _ in observedLanguages]
dat["ParentDistance"] = [0] + [distanceToParent[x] for x in hiddenLanguages+observedLanguages]
dat["prior_only"] = 0
dat["Components"] = 2
dat["NumberOfDimensions"] = len(dat["FFrontierObserved"][0])

print(dat)
dat["DistanceMatrix"] = kernel
dat["DistanceMatrixTime"] = kernelTime

print(hiddenLanguages)

sm = pystan.StanModel(file=f'{__file__[:-3]}.stan')


fit = sm.sampling(data=dat, iter=100000, chains=1, control={"adapt_delta" : 0.85, "max_treedepth" : 12})
la = fit.extract(permuted=True)  # return a dictionary of arrays


import pickle
with open(f"/u/scr/mhahn/POSTERIORS_PHYLOGENETIC/{__file__}.pkl", "wb") as f:
   pickle.dump(la, f)

#with open("/u/scr/mhahn/POSTERIORS_PHYLOGENETIC/{__file__}.pkl", "rb") as f:
#   la = pickle.load(f)

#0    radius
#1    angle
#2    congruence
#3-6  segments

# Correlation between angle and congruence

import numpy as np
# Basic Word Order reflects tension between IL and DL

import matplotlib.pyplot as plt

for j in range(la["Omega"].shape[1]):
  for i in range(j):
    correlation = (la["Omega"][:,i,j] / np.sqrt(la["Omega"][:,i,i] * la["Omega"][:,j,j]))
    
    fig, ax = plt.subplots(figsize =(6, 2))
    counts, _, _ = ax.hist(correlation)
    ax.set_xlim(-1,1)
    ax.set(yticklabels= [])
    ax.plot([0,0], [0,counts.max()], color="gray")
    plt.savefig(f"fits/figures/{__file__[:-3]}_{i}_{j}.pdf")
 

#
#import numpy as np
with open(f"fits/{__file__}_{group}.txt", "w") as outFile:
   print(fit, file=outFile)
   print("HIDDEN_LANGUAGES", " ".join(hiddenLanguages), file=outFile)
   print("OBSERVED", " ".join(observedLanguages), file=outFile)


   CorrelationFFrontierCongruence = (la["Omega"][:,0,1] / np.sqrt(la["Omega"][:,0,0] * la["Omega"][:,1,1]))
   print("CorrelationFFrontierCongruence.mean()", CorrelationFFrontierCongruence.mean(), file=outFile)
   print(np.quantile(CorrelationFFrontierCongruence, 0.025), file=outFile)
   print(np.quantile(CorrelationFFrontierCongruence, 0.5), file=outFile)
   print(np.quantile(CorrelationFFrontierCongruence, 0.975), file=outFile)
   print(np.mean(CorrelationFFrontierCongruence > 0), file=outFile)
#   
#   CorrelationDepLenCongruence = (la["Omega"][:,1,2] / np.sqrt(la["Omega"][:,1,1] * la["Omega"][:,2,2]))
#   print("CorrelationDepLenCongruence.mean()", CorrelationDepLenCongruence.mean(), file=outFile)
#   print(np.quantile(CorrelationDepLenCongruence, 0.025), file=outFile)
#   print(np.quantile(CorrelationDepLenCongruence, 0.975), file=outFile)
#   print(np.mean(CorrelationDepLenCongruence > 0), file=outFile)
#
#   CorrelationSurprisalCongruence = (la["Omega"][:,0,2] / np.sqrt(la["Omega"][:,0,0] * la["Omega"][:,2,2]))
#   print("CorrelationSurprisalCongruence.mean()", CorrelationSurprisalCongruence.mean(), file=outFile)
#   print(np.quantile(CorrelationSurprisalCongruence, 0.025), file=outFile)
#   print(np.quantile(CorrelationSurprisalCongruence, 0.975), file=outFile)
#   print(np.mean(CorrelationSurprisalCongruence > 0), file=outFile)
#





#print((la["Lrescor_Sigma"] > 0).mean(axis=0))
#print((la["Sigma"] > 0).mean(axis=0))
#print((la["Omega"] > 0).mean(axis=0))
## Correlation
##print(la["Sigma"][1,2] / (la["Sigma"][1,1].sqrt() * la["Sigma"][2,2].sqrt()))
#with open(f"fits/CORR_Sigma_{__file__}_{group}.txt", "w") as outFile:
#  for x in la["Sigma"][:,0,1] / np.sqrt(la["Sigma"][:,0,0] * la["Sigma"][:,1,1]):
#      print(round(float(x), 4), file=outFile)
#print(la["Omega"].shape)
#with open(f"fits/stationary_fit_{__file__}_{group}.txt", "w") as outFile:
# print("Group", "Mean1", "Mean2", "Cov11", "Cov12", "Cov22", file=outFile)
# for i, cat in enumerate(stoi_categorical):
#   print(cat, la["alpha"][:,i,0].mean(), la["alpha"][:,i,1].mean(), la["Omega"][:,i,0,0].mean(), la["Omega"][:,i,0,1].mean(), la["Omega"][:,i,1,1].mean(), file=outFile)
