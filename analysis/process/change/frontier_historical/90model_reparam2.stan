functions {
}
data {
  int<lower=1> NumberOfDimensions;  // number of dimensions of the Ornstein-Uhlenbeck process
  int<lower=1> ObservedN;  // number of observations (i.e., observed languages)
  matrix<lower=-20, upper=20>[ObservedN, NumberOfDimensions] FFrontierObserved; // state of the observed languages
  int<lower=1> HiddenN; // number of unobserved languages (inner nodes without historical corpus)
  int<lower=1> TotalN; // overall number of languages
  int IsHidden[TotalN]; // indicates, for each language, whether it is unobserved or observed
  int ParentIndex[TotalN]; // for each language indicates its parent language in the phylogenetic tree
  int Total2Observed[TotalN]; // for each language, indicates its index within only the observed languages
  int Total2Hidden[TotalN]; // for each language, indicates its index within only the unobserved languages
  vector<lower=0>[TotalN] ParentDistance; // time distance to the parent node
  int prior_only;  // should the likelihood be ignored? (not used)
  int Components; // not used
  int NumberOfCategories; // not used
  int<lower=0, upper=NumberOfCategories> OrderCategory[TotalN]; // not used
}
parameters {
  matrix<lower=-20, upper=20>[HiddenN, NumberOfDimensions] FFrontierHidden; // states of unobserved languages (latent variables)
  vector<lower=-10, upper=10>[NumberOfDimensions] alpha; // the mean of the process (referred to as mu in Materials&Methods)
  vector<lower=0.01, upper=20>[NumberOfDimensions] sigma_B; // the diagonal entries of the drift matrix (referred to as Gamma in Materials&Methods)

  cholesky_factor_corr[NumberOfDimensions] Lrescor_Sigma;  // Cholesky factor of correlation component of Sigma (covariance of instantaneous changes, as defined in Materials&Methods). Referred to as U in SI Appendix Section S7.2.
  vector<lower=0.01, upper=20>[NumberOfDimensions] sigma_Sigma; // standard deviation component of Sigma. Referred to as D in SI Appendix Section S7.2.
}
transformed parameters {
  // intermediate steps
 matrix[NumberOfDimensions, NumberOfDimensions] Sigma_chol; // Referred to as DU in SI Appendix Section S7.2.
 matrix[NumberOfDimensions, NumberOfDimensions] Sigma; // covariance of instantaneous changes, as defined in Materials&Methods

 matrix[NumberOfDimensions,NumberOfDimensions] B; // drift matrix: referred to as Gamma in Materials&Methods
 matrix[NumberOfDimensions,NumberOfDimensions] Omega; // covariance of stationary distribution, as defined in Materials&Methods

  // Fill the drift matrix 
  for(i in 1:NumberOfDimensions) {
     for(j in 1:NumberOfDimensions) {
       if(i==j) {
         B[i,j] = sigma_B[i];
       } else {
         B[i,j]= 0;
       }
     }
  }
//
 Sigma_chol = diag_pre_multiply(sigma_Sigma, Lrescor_Sigma); // referred to as DU in SI Appendix Section S7.2.
 Sigma = multiply_lower_tri_self_transpose(Sigma_chol); // compute Sigma

   
     // Now calculate Omega, the stationary covariance, using formula from Materials&Methods (SI Appendix, Equation (19))
     for(q in 1:NumberOfDimensions) {
       for(r in 1:NumberOfDimensions) {
         Omega[q,r] = Sigma[q,r] / (B[q,q] + B[r,r]);
       };
     }
  


}
model {
// Priors, as defined in SI Appendix, Section S7.2.
  target += normal_lpdf(sigma_B | 0, 1); // Prior for drift matrix
  target += normal_lpdf(sigma_Sigma | 0, 1); // Prior for standard deviation component
  target += normal_lpdf(alpha | 0, 1); // Prior for mean
  target += lkj_corr_cholesky_lpdf(Lrescor_Sigma | 1); // Prior for correlation component

// Compute likelihood for both unobserved and observed languages
  for (n in 2:TotalN) { // Start at 2 because 1 is the root of the phylogenetic tree (not an actual language)
     vector[NumberOfDimensions] reference_overall; // the state of the parent node
     vector[NumberOfDimensions] own_overall; // the state of the current node
     vector[NumberOfDimensions] alphaHere; // the mean of the process

     // Obtain the state of the parent language, depending on whether it is observed or unnobserved
     if (IsHidden[ParentIndex[n]]) {
         reference_overall = FFrontierHidden[Total2Hidden[ParentIndex[n]]]';
     } else {
         reference_overall = FFrontierObserved[Total2Observed[ParentIndex[n]]]';

     }
    
     // Obtain the state of the current language, depending on whether it is observed or unnobserved
     if (IsHidden[n]) {
        own_overall = FFrontierHidden[Total2Hidden[n]]';
     } else {
        own_overall = FFrontierObserved[Total2Observed[n]]';
     }

    alphaHere = alpha;
    if (ParentIndex[n] == 1) {
        target += multi_normal_lpdf(own_overall | alphaHere, Omega); // if the parent is the root (i.e., there is no parent language in the tree), the state is a draw from the stationary distribution
     } else {
        matrix[NumberOfDimensions, NumberOfDimensions] exp1 = matrix_exp(-B * ParentDistance[n]);
        target += multi_normal_lpdf(own_overall | alphaHere + exp1 * (reference_overall - alphaHere), Omega - exp1 * Omega * exp1'); // else, use SI Appendix, Equation (21)
     }
  }
}
generated quantities {
}

