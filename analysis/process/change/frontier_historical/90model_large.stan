functions {
}
data {
  int<lower=1> NumberOfDimensions;  // number of observations
  int<lower=1> ObservedN;  // number of observations
//  vector<lower=-1, upper=1>[ObservedN] TraitObserved;  // population-level design matrix
//  real<lower=-1, upper=1> LogitsObserved[ObservedN];
  matrix<lower=-20, upper=20>[ObservedN, NumberOfDimensions] FFrontierObserved;
  int<lower=1> HiddenN;
  int<lower=1> TotalN;
  int IsHidden[TotalN];
  int ParentIndex[TotalN];
  int Total2Observed[TotalN];
  int Total2Hidden[TotalN];
  vector<lower=0>[TotalN] ParentDistance;
  int prior_only;  // should the likelihood be ignored?
  int Components;
  int NumberOfCategories;
  int<lower=0, upper=NumberOfCategories> OrderCategory[TotalN];
}
parameters {
//  vector<lower=-1, upper=1>[HiddenN] TraitHidden;
  matrix<lower=-20, upper=20>[HiddenN, NumberOfDimensions] FFrontierHidden;
  vector<lower=-10, upper=10>[NumberOfDimensions] alpha; // the mean of the process
  vector<lower=0.01, upper=20>[NumberOfDimensions] sigma_B;

  cholesky_factor_corr[NumberOfDimensions] Lrescor_Sigma; 
  vector<lower=0.01, upper=20>[NumberOfDimensions] sigma_Sigma;
}
transformed parameters {
  // intermediate steps
  matrix[NumberOfDimensions, NumberOfDimensions] Lrescor_B;
 matrix[NumberOfDimensions, NumberOfDimensions] Sigma_chol;
 matrix[NumberOfDimensions, NumberOfDimensions] Sigma;

 matrix[NumberOfDimensions,NumberOfDimensions] B;
 matrix[NumberOfDimensions,NumberOfDimensions] Omega;
     matrix[NumberOfDimensions, NumberOfDimensions] B_chol;

  for(i in 1:NumberOfDimensions) {
     for(j in 1:NumberOfDimensions) {
       if(i==j) {
         Lrescor_B[i,j] = 1;
       } else {
         Lrescor_B[i,j] = 0;
       }
     }
  }
//
 Sigma_chol = diag_pre_multiply(sigma_Sigma, Lrescor_Sigma);
 Sigma = multiply_lower_tri_self_transpose(Sigma_chol);

     B_chol = diag_pre_multiply(sigma_B, Lrescor_B);
   //
     B = multiply_lower_tri_self_transpose(B_chol);
   // Sigma = instantaneous covariance
   // B = drift matrix (here assumed to be positive definite & symmetric for simplicity)
   
     // Now calculate Omega, the stationary covariance
     for(q in 1:NumberOfDimensions) {
       for(r in 1:NumberOfDimensions) {
         Omega[q,r] = Sigma[q,r] / (B[q,q] + B[r,r]);
       };
     }
  


}
model {
     target += student_t_lpdf(sigma_B | 3, 0, 2.5);
  target += student_t_lpdf(sigma_Sigma | 3, 0, 2.5);
     target += normal_lpdf(alpha | 0, 1);
  target += lkj_corr_cholesky_lpdf(Lrescor_Sigma | 1);
  for (n in 2:TotalN) {
     vector[NumberOfDimensions] reference_overall;
     vector[NumberOfDimensions] own_overall;
     vector[NumberOfDimensions] alphaHere;

     if (IsHidden[ParentIndex[n]]) {
         reference_overall = FFrontierHidden[Total2Hidden[ParentIndex[n]]]';
     } else {
         reference_overall = FFrontierObserved[Total2Observed[ParentIndex[n]]]';

     }
    
     if (IsHidden[n]) {
        own_overall = FFrontierHidden[Total2Hidden[n]]';
     } else {
        own_overall = FFrontierObserved[Total2Observed[n]]';
     }

    alphaHere = alpha;
    if (ParentIndex[n] == 1) {
        target += multi_normal_lpdf(own_overall | alphaHere, Omega);
     } else {
        matrix[NumberOfDimensions, NumberOfDimensions] exp1 = matrix_exp(-B * ParentDistance[n]);
        target += multi_normal_lpdf(own_overall | alphaHere + exp1 * (reference_overall - alphaHere), Omega - exp1 * Omega * exp1');
     }
  }
}
generated quantities {
}

