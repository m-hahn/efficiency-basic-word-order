# Phylogenetic analysis including inner nodes as latent variables.

- [Analysis (including inner nodes, see SI Appendix, Section S7.2)](90model_reparam2.py). Refer to the [corresponding Stan script](90model_reparam2.stan) for extensive comments that also apply to the other scripts
- [Analysis with case marking](90model_reparam2_Case.py) 
- [Case marking (inner nodes)](case_marking_groups_2.8.tsv)
- [Case marking (attested languages)](case_marking_revised_2.8.tsv)
- [Results](fits)
- [Dating](groups2_2.8.tsv)
- [Phylogenetic tree](trees2_2.8.tsv)
