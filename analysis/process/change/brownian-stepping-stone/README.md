# Simple Brownian Model
* Fitting Model: [Python Script](35model.py), [Stan Script](35model.stan)
* Fitting Model (only coadaptation): [Python Script](37model.py), [Stan Script](37model.stan)
* Marginal likelihood (only coadaptation): [Python Script](38model.py), [Stan Script](38model.stan)
* [Results (Fits)](fits)
* [Results (Marginal Likelihood)](marginal_likelihood)

