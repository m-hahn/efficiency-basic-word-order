import pystan
from math import sqrt, log
import math


import sys

group = "process9_frontier18" #sys.argv[1]

timeDepth = int(sys.argv[1])
assert timeDepth < 0

with open("trees2_2.8.tsv", "r") as inFile:
  trees = [x.split("\t") for x in inFile.read().strip().split("\n")][1:]
print(trees)

observedLangs = set()
allLangs = set()
parents = {}
for line in trees:
   observedLangs.add(line[0])
   allLangs.add(line[0])
   for i in range(len(line)-1):
       child = line[i]
       parent = line[i+1]
       if child in parents:
         assert parents[child] == parent, (parents[child], line, child, i)
       parents[child] = parent
       allLangs.add(parent)

with open("groups2_2.8.tsv", "r") as inFile:
  dates = [x.split("\t") for x in inFile.read().strip().split("\n")][1:]
print([len(x) for x in dates])
dates = dict(dates)
dates["_ROOT_"] = timeDepth
print(dates)
for x in allLangs:
  if x not in observedLangs:
    if x not in dates:
       print(x)

import glob
assert group.startswith("process")
pareto_distance_x  = {x[0] : float(x[2]) for x in [x.split(" ") for x in open(f"../../output/{group}.py.tsv", "r").read().strip().split("\n")] if x[1] == "Distance_Surprisal_ComparedFrontier" }
pareto_distance_y  = {x[0] : float(x[2]) for x in [x.split(" ") for x in open(f"../../output/{group}.py.tsv", "r").read().strip().split("\n")] if x[1] == "Distance_DepLen_ComparedFrontier" }
average_f  = {x[0] : float(x[2]) for x in [x.split(" ") for x in open(f"../../output/{group}.py.tsv", "r").read().strip().split("\n")] if x[1] == "AverageFAngleSpace" }


fOnFrontier = {}
for f in glob.glob(f"../../output_frontier/{group}.py_*_fOnFrontierDiscretizedByAngle.tsv"):
  with open(f, "r") as inFile:
    func = [x.split(" ") for x in inFile.read().strip().split("\n")]
    assert len(func) == 10
    func = [float(x[1]) for x in func]
    language = f[f.index(".py")+4:f.rfind("_")]
    print(language)
    fOnFrontier[language] = func
print(fOnFrontier)

with open("../../../../languages/extract_congruence/output/evaluateCongruence_POS_NoSplit.py.tsv", "r") as inFile:
   data = [x.replace('"', '').split("\t") for x in inFile.read().strip().split("\n")]
header = data[0]
header = header
header = dict(list(zip(header, range(len(header)))))
data = data[1:]
print(header)
valueByLanguage = {}
for line in data:
   print(line)
   language = line[header["Language"]]
 
   #assert language in parents, language
   print(line)
   if language not in fOnFrontier:
     print("MISSING LANGUAGE", language)
     continue
   x = float('nan')
   y = float('nan')
#   assert y == round(y)
   z = float(line[header["Congruence_All"]])
   valueByLanguage[language] = [x,y,z] 

#valueByLanguage["ISWOC_Old_English"] = [0, 0, 0.49]
#valueByLanguage["Archaic_Greek"] = [0, 0, 0.56]
#valueByLanguage["Classical_Greek"] = [0, 0, 0.52]
#valueByLanguage["Koine_Greek"] = [0, 0, 0.47]

print(valueByLanguage)

print(observedLangs)
print(allLangs)
hiddenLangs = sorted([x for x in allLangs if x not in observedLangs and x != "_ROOT_"])
print(hiddenLangs)

#observedLanguages = [x for x in list(observedLangs) if parents[x] not in observedLangs] # This is for understanding what the model does on only synchronic data
observedLanguages = [x for x in list(observedLangs) if x in valueByLanguage]
hiddenLanguages = hiddenLangs
totalLanguages = ["_ROOT_"] + hiddenLanguages + observedLanguages
lang2Code = dict(list(zip(totalLanguages, range(len(totalLanguages)))))
lang2Observed = dict(list(zip(observedLanguages, range(len(observedLanguages)))))


distanceToParent = {}
for language in allLangs:
   parent = parents.get(language, "_ROOT_")
   if language in observedLangs and parent == "_ROOT_":
     print("ROOT", language)
   dateLang = dates.get(language, 2000)
   dateParent = dates[parent]
   distanceToParent[language] = (float(dateLang)-float(dateParent))/1000
print(distanceToParent)
print(parents.get("Classical_Chinese_2.8"))
assert "Classical_Chinese_2.8" in observedLangs
#quit()

from collections import defaultdict


listOfAllAncestors = defaultdict(list)

def processDescendants(l, desc):
   listOfAllAncestors[desc].append(l)
   if l != "_ROOT_":
      parent = parents.get(l, "_ROOT_")
      processDescendants(parent, desc)
   
for language in totalLanguages:
   processDescendants(language, language)
print(listOfAllAncestors)




covarianceMatrix = [[None for _ in range(len(observedLanguages))] for _ in range(len(observedLanguages))]
for i in range(len(observedLanguages)):
   for j in range(i+1):
      l1 = observedLanguages[i]
      l2 = observedLanguages[j]
      print("----------")
      print([l for l in listOfAllAncestors[l1] if l in listOfAllAncestors[l2]])
      print([int(dates.get(l, 2000)) for l in listOfAllAncestors[l1] if l in listOfAllAncestors[l2]])
      commonAncestors = max([int(dates.get(l, 2000)) for l in listOfAllAncestors[l1] if l in listOfAllAncestors[l2]])
      print((commonAncestors - (timeDepth))/1000)

      covarianceMatrix[i][j] = (commonAncestors - (timeDepth))/1000
      covarianceMatrix[j][i] = (commonAncestors - (timeDepth))/1000


dat = {}

dat["ObservedN"] = len(observedLanguages)
dat["FFrontierObserved"] = [[ valueByLanguage[x][2]*2-1, average_f[x]*2-1] for x in observedLanguages]
dat["HiddenN"] = len(hiddenLanguages)+1
dat["TotalN"] = dat["ObservedN"] + dat["HiddenN"]
dat["IsHidden"] = [1]*dat["HiddenN"] + [0]*dat["ObservedN"]
dat["ParentIndex"] = [0] + [1+lang2Code[parents.get(x, "_ROOT_")] for x in hiddenLanguages+observedLanguages]
dat["Total2Observed"] = [0]*dat["HiddenN"] + list(range(1,1+len(observedLanguages)))
dat["Total2Hidden"] = [1] + list(range(2,2+len(hiddenLanguages))) + [0 for _ in observedLanguages]
dat["ParentDistance"] = [0] + [distanceToParent[x] for x in hiddenLanguages+observedLanguages]
dat["CovarianceMatrix"] = covarianceMatrix
dat["prior_only"] = 0
dat["Components"] = 2
dat["NumberOfDimensions"] = len(dat["FFrontierObserved"][0])

print(dat)

print(hiddenLanguages)

sm = pystan.StanModel(file=f'{__file__[:-3]}.stan')


dat["stepping"] = 1


fit = sm.sampling(data=dat, iter=10000, chains=4, control=dict(adapt_delta=0.95))
#print("TREE DEPTH", stan_utility.check_treedepth(fit))
#print("E-BFMI", stan_utility.check_energy(fit))

la = fit.extract(permuted=True)  # return a dictionary of arrays
import numpy as np

import matplotlib.pyplot as plt

for j in range(la["Sigma"].shape[1]):
  for i in range(j):
    correlation = (la["Sigma"][:,i,j] / np.sqrt(la["Sigma"][:,i,i] * la["Sigma"][:,j,j]))
    
    fig, ax = plt.subplots(figsize =(6, 2))
    counts, _, _ = ax.hist(correlation)
    ax.set_xlim(-1,1)
    ax.set(yticklabels= [])
    ax.plot([0,0], [0,counts.max()], color="gray")
    plt.savefig(f"fits/figures/{__file__[:-3]}_{i}_{j}.pdf")
 

with open(f"fits/{__file__}.txt", "w") as outFile:
   print(fit, file=outFile)
   print("HIDDEN_LANGUAGES", " ".join(hiddenLanguages), file=outFile)


   CorrelationFFrontierCongruence = (la["Sigma"][:,0,1] / np.sqrt(la["Sigma"][:,0,0] * la["Sigma"][:,1,1]))
   print("CorrelationFFrontierCongruence.mean()", CorrelationFFrontierCongruence.mean(), file=outFile)
   print(np.quantile(CorrelationFFrontierCongruence, 0.025), file=outFile)
   print(np.quantile(CorrelationFFrontierCongruence, 0.5), file=outFile)
   print(np.quantile(CorrelationFFrontierCongruence, 0.975), file=outFile)
   print(np.mean(CorrelationFFrontierCongruence > 0), file=outFile)
  
