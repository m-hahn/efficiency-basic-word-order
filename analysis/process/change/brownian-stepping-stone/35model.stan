functions {
}
data {
  int<lower=1> NumberOfDimensions;  // number of observations
  int<lower=1> ObservedN;  // number of observations
  matrix<lower=-20, upper=20>[ObservedN, NumberOfDimensions] FFrontierObserved;
  int<lower=1> HiddenN;
  int<lower=1> TotalN;
  int IsHidden[TotalN];
  int ParentIndex[TotalN];
  int Total2Observed[TotalN];
  int Total2Hidden[TotalN];
  vector<lower=0>[TotalN] ParentDistance;
  int prior_only;  // should the likelihood be ignored?
  int Components;
  matrix[ObservedN, ObservedN] CovarianceMatrix;
  real stepping;
}
parameters {
  vector[NumberOfDimensions] alpha; // the mean of the process
  vector<lower=0>[NumberOfDimensions] sd_1; 
  cholesky_factor_corr[NumberOfDimensions] Lrescor; 

}
transformed parameters {

  matrix[NumberOfDimensions, NumberOfDimensions] Sigma ;
  real targetPrior = 0;
  real targetLikelihood = 0;

  
  { ////////////////////
  vector[NumberOfDimensions*ObservedN] own_overall;
  vector[NumberOfDimensions*ObservedN] fullMeanVector;
  matrix[NumberOfDimensions*ObservedN, NumberOfDimensions*ObservedN] fullCovMat;

  matrix[NumberOfDimensions, NumberOfDimensions] LSigma = diag_pre_multiply(sd_1, Lrescor);
  Sigma = multiply_lower_tri_self_transpose(LSigma);

  for(i in 1:ObservedN) {
     for(j in 1:ObservedN) {
       for(u in 1:NumberOfDimensions) {
        for(v in 1:NumberOfDimensions) {
           fullCovMat[NumberOfDimensions*(i-1)+u, NumberOfDimensions*(j-1)+v] = CovarianceMatrix[i,j] * Sigma[u,v];
        }
       }
     }
  }
  targetPrior += normal_lpdf(alpha | 0, 1);

  targetPrior += student_t_lpdf(sd_1 | 3, 0, 2.5);
  targetPrior += lkj_corr_cholesky_lpdf(Lrescor | 1);
  
  for(i in 1:ObservedN) {
   for(v in 1:NumberOfDimensions) {
     own_overall[NumberOfDimensions*(i-1)+v] = FFrontierObserved[i,v];
     fullMeanVector[NumberOfDimensions*(i-1)+v] = alpha[v];
   }
  }
  targetLikelihood += multi_normal_lpdf(own_overall | fullMeanVector, fullCovMat);

  }
}
model {
  target += stepping * targetLikelihood + targetPrior;
}
generated quantities {
}

