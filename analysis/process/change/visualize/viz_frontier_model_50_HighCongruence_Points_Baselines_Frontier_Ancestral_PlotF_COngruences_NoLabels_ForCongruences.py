import matplotlib.pyplot as plt
import torch



real_congruence = {}
with open("../../../../languages/extract_congruence/output/evaluateCongruence_POS_NoSplit.py.tsv", "r") as inFile:
    data = [x.split("\t") for x in inFile.read().strip().replace('"', "").split("\n")]
    header = data[0]
    for line in data[1:]:
        real_congruence[line[header.index("Language")]] = float(line[header.index("Congruence_All")])
print(real_congruence)
        

#with open("../../../ud_languages_2.8.txt", "r") as inFile:
#    languages = inFile.read().strip().split("\n")
languages = list(set(real_congruence))
N_LANG = len(languages)

summary_data = {}
with open("../../output/process9_frontier18.py.tsv", "r") as inFile:
    for line in inFile:
        line = line.strip().split(" ")
        summary_data[(line[0], line[1])] = float(line[2])
print(summary_data)

x_coordinates = {}
y_coordinates = {}
for language in languages:
 with open(f"../../output_frontier/process9_frontier18.py_{language}_FrontierDiscretizedByAngle.tsv", "r") as inFile:
    frontier = [float(x.split(" ")[2]) for x in inFile.read().strip().split("\n")]
    x = frontier[::2]
    y = frontier[1::2]
    x_coordinates[language] = x
    y_coordinates[language] = y

fOnFrontier = {}
fInterior = {}
interiorCoordinates = None
for language in languages:
  with open(f"../../output_frontier/process9_frontier18.py_{language}_fOnFrontierDiscretizedByAngle.tsv", "r") as inFile:
     fOnFrontier[language] = [float(x.split(" ")[1]) for x in inFile.read().strip().split("\n")]
  with open(f"../../output_frontier/process9_frontier18.py_{language}_fArea.tsv", "r") as inFile:
     inFileRead = inFile.read().strip().split("\n")
     fInterior[language] = [float(x.split(" ")[2]) for x in inFileRead]
     interiorCoordinates = torch.FloatTensor([(float(x.split(" ")[0]), float(x.split(" ")[1])) for x in inFileRead])
fOnFrontier = torch.FloatTensor([fOnFrontier[x] for x in languages])

fInterior = torch.FloatTensor([fInterior[x] for x in languages])



print(x_coordinates)

x_coordinates = torch.FloatTensor([x_coordinates[x] for x in languages])
y_coordinates = torch.FloatTensor([y_coordinates[y] for y in languages])
print(x_coordinates.mean(dim=0))
print(y_coordinates.mean(dim=0))


x_positions_langs = torch.FloatTensor([summary_data[(x,"Distance_Surprisal_ComparedFrontier")] for x in languages])
#y_positions_langs = torch.FloatTensor([summary_data[(y,"Distance_DepLen_ComparedFrontier")] for y in languages])
y_positions_langs = torch.FloatTensor([summary_data[(y,"AverageFAngleSpace")] for y in languages])


def sd(tensor, dim):
    return (tensor.pow(2).mean(dim=dim) - tensor.mean(dim=dim).pow(2)).sqrt()

families = {}
for q in ["../../../../languages/families_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        families[line[0]] = line[1]
families["PARTITION-Icelandic_early"] = "Germanic"
families["PARTITION-Icelandic_middle"] = "Germanic"
families["PARTITION-Icelandic_modern"] = "Germanic"
families["ISWOC_Old_English"] = "Germanic"
families["ISWOC_Portuguese"] = "Latin_Romance"
families["ISWOC_Spanish"] = "Latin_Romance"
families["PARTITION-Ancient_Greek_archaic"] = "Greek"
families["PARTITION-Ancient_Greek_classical"] = "Greek"
families["PARTITION-Ancient_Greek_koine"] = "Greek"

categorical = {}
for q in ["../../../../languages/categorical_order/categorical_order_2.8.tsv", "../../../../languages/categorical_order/categoricalOrderAdditional_2.8.tsv", "../../../../languages/categorical_order/categoricalOrder_groups_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        categorical[line[0]] = line[1]

COLORS = {"SOV" : "lightgreen", "SVO" : "blue", "No dominant order" : "gray", "VSO" : "lightgreen", "VOS" : "orange"}



def getShape(x, names):
    names = [[int(q)-1 for q in y[y.index("[")+1:-1].split(",")] for y in names]
    N = len(names[0])
    size = [max([q[i] for q in names])+1 for i in range(N)]
    result = torch.zeros(size)
    for i in range(len(names)):
        result[tuple(names[i])] = x[i]
    return result

orders = ['No dominant order', 'SOV', 'SVO', 'VOS', 'VSO']


ancestral = {}
with open("../frontier_historical/fits/90model.py_process9_frontier18_Revised.txt", "r") as inFile:
     fit = [[q for q in x.split(" ") if len(q) > 0] for x in inFile.read().strip().split("\n")]

FFrontierHidden_fit = [x for x in fit if len(x) > 0 and x[0].startswith("FFrontierHidden")]
print(FFrontierHidden_fit)
FFrontierHidden = torch.FloatTensor([float(x[1]) for x in FFrontierHidden_fit])
print(FFrontierHidden)
FFrontierHidden_names = [x[0] for x in FFrontierHidden_fit]
print(FFrontierHidden_names)

FFrontierHidden_frontier_shape = getShape(FFrontierHidden, FFrontierHidden_names)
print(FFrontierHidden_frontier_shape)

ancestralStates = [x for x in fit if len(x) > 0 and x[0] == "HIDDEN_LANGUAGES"][0][1:]
print(ancestralStates)
print(len(ancestralStates))
print(FFrontierHidden_frontier_shape.size())
assert len(ancestralStates)+1 == FFrontierHidden_frontier_shape.size()[0]

FFrontierHidden_frontier_shape[:,2] = (0.5*FFrontierHidden_frontier_shape[:,2])+0.5
print(FFrontierHidden_frontier_shape)
for i in range(len(ancestralStates)):
    ancestral[ancestralStates[i]] = FFrontierHidden_frontier_shape[i+1]
print(ancestral)
 


grid = torch.FloatTensor([[((x)/20-1.5,(y)/20-1.5) for y in range(60)] for x in range(60)]).view(-1, 2)

x_variance_langs = torch.FloatTensor([summary_data[(x,"Baseline_Variance_X_ComparedToFrontier")] for x in languages])
y_variance_langs = torch.FloatTensor([summary_data[(x,"Baseline_Variance_Y_ComparedToFrontier")] for x in languages])
xy_covariance_langs = torch.FloatTensor([summary_data[(x,"Baseline_Covariance_XY_ComparedToFrontier")] for x in languages])

baselines_covariance = torch.stack([x_variance_langs, xy_covariance_langs, xy_covariance_langs, y_variance_langs], dim=1).view(N_LANG, 2,2)

print(grid.size(), x_variance_langs.size())
print(baselines_covariance.size())
baseline_density = []
for i in range(len(languages)):
  baseline_density.append(torch.exp(-(grid.t() * torch.matmul(baselines_covariance[i].inverse(), grid.t())).sum(dim=0)))

baseline_densities = torch.stack(baseline_density, dim=0)
baseline_density = torch.stack(baseline_density, dim=0).mean(dim=0)


def drawLine(axis1, axis2, old, new):
    if old in ancestral:
        x1 = float(ancestral[old][2])/2+0.5
        y1 = float(ancestral[old][3])/2+0.5
    else:
        x1 = real_congruence[old]
        y1 = y_positions_langs[languages.index(old)]
    if new in ancestral:
        x2 = float(ancestral[new][2])/2+0.5
        y2 = float(ancestral[new][3])/2+0.5
    else:
        x2 = real_congruence[new]
        y2 = y_positions_langs[languages.index(new)]
    axis1.plot([x1, x2], [y1, y2], c="gray", alpha=0.5)
    axis2.plot([x1, x2], [y1, y2], c="gray", alpha=0.5)


def plotLang(axis1, axis2, lang):

    if lang in ancestral:
        x2 = float(ancestral[lang][2])/2+0.5
        y2 = float(ancestral[lang][3])/2+0.5
        axis1.scatter([x2], [y2], c=viridis(float(ancestral[lang][2])), alpha=(0.2 if lang in ancestral else 1))
        axis2.text(x2, y2, s=lang, c=viridis(float(ancestral[lang][2])))

    else:
        x2 = real_congruence[lang]
        y2 = y_positions_langs[languages.index(lang)]
        axis1.scatter([x2], [y2], c=viridis(real_congruence[lang]), alpha=(0.2 if lang in ancestral else 1), s=50)
        axis2.text(x2, y2, s=lang, c=viridis(real_congruence[lang]))
#    axis.text(x2, y2, lang[:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[lang]], alpha=(0.2 if lang in ancestral else 1))





import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

viridis = cm.get_cmap('viridis', 8)

import math

from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})


fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)

fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
#
#fig1.tight_layout()
#fig2.tight_layout()

ax1 = [ax1]
ax2 = [ax2]

print(fOnFrontier.size())
print(x_coordinates.size())

FACET=0

ax1[FACET].set_xlim(0,1)
ax1[FACET].set_ylim(0,1)
ax2[FACET].set_xlim(0,1)
ax2[FACET].set_ylim(0,1)
#ax[FACET].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] == "Latin_Romance" else 0 for x in languages])
for i in range(9):
    ax1[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)
    ax2[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

meanFrontier = torch.stack([-x_coordinates[indices].mean(dim=0), -y_coordinates[indices].mean(dim=0)], dim=1)
inner_points = torch.logical_not((interiorCoordinates.unsqueeze(1) < meanFrontier.unsqueeze(0)).all(dim=2).any(dim=1))
ax1[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)
ax2[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)


#ax[FACET].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[FACET].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[FACET].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["French_2.8", "Old_French_2.8", "Italian_2.8", "Spanish_2.8", "Portuguese_2.8", "Latin_2.8", "Romanian_2.8", "Galician_2.8", "Catalan_2.8", "ISWOC_Spanish", "ISWOC_Portuguese"]]:
   plotLang(ax1[FACET], ax2[FACET], languages[i])
#   ax[FACET].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
for hidden in ["Italo_Western_Romance", "Western_Romance", "Iberian_Romance", "West_Iberian"]:
    plotLang(ax1[FACET], ax2[FACET], hidden)
drawLine(ax1[FACET], ax2[FACET], "Latin_2.8", "Italo_Western_Romance")
drawLine(ax1[FACET], ax2[FACET], "Italo_Western_Romance", "Italian_2.8")
drawLine(ax1[FACET], ax2[FACET], "Italo_Western_Romance", "Western_Romance")
drawLine(ax1[FACET], ax2[FACET], "Western_Romance", "Old_French_2.8")
drawLine(ax1[FACET], ax2[FACET], "Old_French_2.8", "French_2.8")
drawLine(ax1[FACET], ax2[FACET], "Western_Romance", "Iberian_Romance")
drawLine(ax1[FACET], ax2[FACET], "Iberian_Romance", "Catalan_2.8")
drawLine(ax1[FACET], ax2[FACET], "Iberian_Romance", "West_Iberian")
drawLine(ax1[FACET], ax2[FACET], "West_Iberian", "ISWOC_Spanish")
drawLine(ax1[FACET], ax2[FACET], "ISWOC_Spanish", "Spanish_2.8")
drawLine(ax1[FACET], ax2[FACET], "West_Iberian", "Galician_2.8")
drawLine(ax1[FACET], ax2[FACET], "West_Iberian", "ISWOC_Portuguese")
drawLine(ax1[FACET], ax2[FACET], "ISWOC_Portuguese", "Portuguese_2.8")

ax1[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")
ax2[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")

#ax1[FACET].set_xlabel("Information Locality")
#ax1[FACET].set_ylabel("Dependency Locality")
#ax2[FACET].set_xlabel("Information Locality")
#ax2[FACET].set_ylabel("Dependency Locality")

if True:
  plt.show()

fig1.set_size_inches(1.5,2.5)
fig1.savefig(f"figures/{__file__}_Romance.pdf")
fig2.set_size_inches(7,8)
fig2.savefig(f"figures/{__file__}_Romance_details.pdf")








fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
ax1 = [ax1]
ax2 = [ax2]




FACET=0

ax1[FACET].set_xlim(0, 1) 
ax1[FACET].set_ylim(0, 1) 
ax2[FACET].set_xlim(0, 1) 
ax2[FACET].set_ylim(0, 1) 
#ax[FACET].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] in ["Sino-Tibetan"] else 0 for x in languages])
for i in range(9):
    ax1[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)
    ax2[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

meanFrontier = torch.stack([-x_coordinates[indices].mean(dim=0), -y_coordinates[indices].mean(dim=0)], dim=1)
inner_points = torch.logical_not((interiorCoordinates.unsqueeze(1) < meanFrontier.unsqueeze(0)).all(dim=2).any(dim=1))
ax1[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)
ax2[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)


#ax[FACET].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[FACET].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[FACET].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["Classical_Chinese_2.8", "Chinese_2.8", "Cantonese_2.8"]]:
#   ax[FACET].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
   plotLang(ax1[FACET], ax2[FACET], languages[i])
#for hidden in ["Uralic", "Finnic"]:
#    plotLang(ax1[FACET], ax2[FACET], hidden)
drawLine(ax1[FACET], ax2[FACET], "Classical_Chinese_2.8", "Chinese_2.8")
drawLine(ax1[FACET], ax2[FACET], "Classical_Chinese_2.8", "Cantonese_2.8")

ax1[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")
ax2[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")

#ax1[FACET].set_xlabel("Information Locality")
#ax1[FACET].set_ylabel("Dependency Locality")
#ax2[FACET].set_xlabel("Information Locality")
#ax2[FACET].set_ylabel("Dependency Locality")

if True:
    plt.show()

fig1.set_size_inches(1.5,2.5)
fig1.savefig(f"figures/{__file__}_Sinitic.pdf")
fig2.set_size_inches(7,8)
fig2.savefig(f"figures/{__file__}_Sinitic_details.pdf")


#quit()

#plt.show()


fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
ax1 = [ax1]
ax2 = [ax2]




FACET=0

ax1[FACET].set_xlim(0, 1)
ax1[FACET].set_ylim(0, 1)
ax2[FACET].set_xlim(0, 1)
ax2[FACET].set_ylim(0, 1)
#ax[FACET].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] in ["Greek"] else 0 for x in languages])
for i in range(9):
    ax1[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)
    ax2[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

meanFrontier = torch.stack([-x_coordinates[indices].mean(dim=0), -y_coordinates[indices].mean(dim=0)], dim=1)
inner_points = torch.logical_not((interiorCoordinates.unsqueeze(1) < meanFrontier.unsqueeze(0)).all(dim=2).any(dim=1))
ax1[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)
ax2[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)


#ax[FACET].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[FACET].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[FACET].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["PARTITION-Ancient_Greek_archaic", "PARTITION-Ancient_Greek_classical", "PARTITION-Ancient_Greek_koine", "Greek_2.8"]]:
#   ax[FACET].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
   plotLang(ax1[FACET], ax2[FACET], languages[i])
for hidden in ["Common_Turkic"]:
    plotLang(ax1[FACET], ax2[FACET], hidden)
drawLine(ax1[FACET], ax2[FACET], "Common_Turkic", "Turkish_2.8")
drawLine(ax1[FACET], ax2[FACET], "Common_Turkic", "Uyghur_2.8")
drawLine(ax1[FACET], ax2[FACET], "Common_Turkic", "Kazakh_2.8")

ax1[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")
ax2[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")


#ax1[FACET].set_xlabel("Information Locality")
#ax1[FACET].set_ylabel("Dependency Locality")
#ax2[FACET].set_xlabel("Information Locality")
#ax2[FACET].set_ylabel("Dependency Locality")

if True:
    plt.show()

fig1.set_size_inches(1.5,2.5)
fig1.savefig(f"figures/{__file__}_Greek.pdf")
fig2.set_size_inches(7,8)
fig2.savefig(f"figures/{__file__}_Greek_details.pdf")





fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)

fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)

ax1 = [ax1]
ax2 = [ax2]



#quit()

ax1[FACET].set_xlim(0, 1)
ax1[FACET].set_ylim(0, 1)
ax2[FACET].set_xlim(0, 1)
ax2[FACET].set_ylim(0, 1)
#ax[FACET].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] == "Latin_Romance" else 0 for x in languages])
for i in range(9):
    ax1[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)
    ax2[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

meanFrontier = torch.stack([-x_coordinates[indices].mean(dim=0), -y_coordinates[indices].mean(dim=0)], dim=1)
inner_points = torch.logical_not((interiorCoordinates.unsqueeze(1) < meanFrontier.unsqueeze(0)).all(dim=2).any(dim=1))
ax1[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)
ax2[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)


#ax[FACET].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[FACET].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[FACET].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["Norwegian_2.8", "Swedish_2.8", "Danish_2.8", "PARTITION-Icelandic_early", "PARTITION-Icelandic_middle", "PARTITION-Icelandic_modern", "Faroese_2.8", "Gothic_2.8", "English_2.8", "Dutch_2.8", "Afrikaans_2.8", "German_2.8", "Naija_2.8"]]:
#   ax[FACET].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
    plotLang(ax1[FACET], ax2[FACET], languages[i])
for hidden in ["West_Scandinavian", "North_Germanic", "West_Germanic", "Germanic", "Icelandic_Faroese"]:
    plotLang(ax1[FACET], ax2[FACET], hidden)
drawLine(ax1[FACET], ax2[FACET], "Germanic", "Gothic_2.8")
drawLine(ax1[FACET], ax2[FACET], "Germanic", "West_Germanic")
drawLine(ax1[FACET], ax2[FACET], "Germanic", "North_Germanic")
drawLine(ax1[FACET], ax2[FACET], "West_Germanic", "German_2.8")
drawLine(ax1[FACET], ax2[FACET], "West_Germanic", "ISWOC_Old_English")
drawLine(ax1[FACET], ax2[FACET], "ISWOC_Old_English", "Macro_English")
drawLine(ax1[FACET], ax2[FACET], "Macro_English", "English_2.8")
drawLine(ax1[FACET], ax2[FACET], "Macro_English", "Naija_2.8")
drawLine(ax1[FACET], ax2[FACET], "West_Germanic", "Global_Dutch")
drawLine(ax1[FACET], ax2[FACET], "Global_Dutch", "Dutch_2.8")
drawLine(ax1[FACET], ax2[FACET], "Global_Dutch", "Afrikaans_2.8")
drawLine(ax1[FACET], ax2[FACET], "North_Germanic", "Danish_2.8")
drawLine(ax1[FACET], ax2[FACET], "North_Germanic", "Swedish_2.8")
drawLine(ax1[FACET], ax2[FACET], "North_Germanic", "West_Scandinavian")
drawLine(ax1[FACET], ax2[FACET], "West_Scandinavian", "Norwegian_2.8")
drawLine(ax1[FACET], ax2[FACET], "West_Scandinavian", "PARTITION-Icelandic_early")
drawLine(ax1[FACET], ax2[FACET], "PARTITION-Icelandic_early", "Faroese_2.8")
drawLine(ax1[FACET], ax2[FACET], "PARTITION-Icelandic_early", "PARTITION-Icelandic_middle")
drawLine(ax1[FACET], ax2[FACET], "PARTITION-Icelandic_middle", "PARTITION-Icelandic_modern")

ax1[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")
ax2[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")

#ax1[FACET].set_xlabel("Information Locality")
#ax1[FACET].set_ylabel("Dependency Locality")
#ax2[FACET].set_xlabel("Information Locality")
#ax2[FACET].set_ylabel("Dependency Locality")

if True:
    plt.show()

fig1.set_size_inches(1.5,2.5)
fig1.savefig(f"figures/{__file__}_Germanic.pdf")
fig2.set_size_inches(7,8)
fig2.savefig(f"figures/{__file__}_Germanic_details.pdf")



fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
ax1 = [ax1]
ax2 = [ax2]




ax1[FACET].set_xlim(0, 1)
ax1[FACET].set_ylim(0, 1)
ax2[FACET].set_xlim(0, 1)
ax2[FACET].set_ylim(0, 1)
#ax[FACET].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] == "Celtic" else 0 for x in languages])
for i in range(9):
    ax1[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)
    ax2[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

meanFrontier = torch.stack([-x_coordinates[indices].mean(dim=0), -y_coordinates[indices].mean(dim=0)], dim=1)
inner_points = torch.logical_not((interiorCoordinates.unsqueeze(1) < meanFrontier.unsqueeze(0)).all(dim=2).any(dim=1))
ax1[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)
ax2[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)


#ax[FACET].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[FACET].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[FACET].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["Irish_2.8", "Breton_2.8", "Scottish_Gaelic_2.8", "Welsh_2.8", "Manx_2.8"]]:
   plotLang(ax1[FACET], ax2[FACET], languages[i])
#   ax[FACET].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
for hidden in ["Insular_Celtic", "Brythonic", "Goidelic"]:
    plotLang(ax1[FACET], ax2[FACET], hidden)
drawLine(ax1[FACET], ax2[FACET], "Insular_Celtic", "Brythonic")
drawLine(ax1[FACET], ax2[FACET], "Insular_Celtic", "Goidelic")
drawLine(ax1[FACET], ax2[FACET], "Goidelic", "Irish_2.8")
drawLine(ax1[FACET], ax2[FACET], "Goidelic", "Scottish_Gaelic_2.8")
drawLine(ax1[FACET], ax2[FACET], "Goidelic", "Manx_2.8")
drawLine(ax1[FACET], ax2[FACET], "Brythonic", "Welsh_2.8")
drawLine(ax1[FACET], ax2[FACET], "Brythonic", "Breton_2.8")

ax1[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")
ax2[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")


#ax1[FACET].set_xlabel("Information Locality")
#ax1[FACET].set_ylabel("Dependency Locality")
#ax2[FACET].set_xlabel("Information Locality")
#ax2[FACET].set_ylabel("Dependency Locality")

if True:
    plt.show()

fig1.set_size_inches(1.5,2.5)
fig1.savefig(f"figures/{__file__}_Celtic.pdf")
fig2.set_size_inches(7,8)
fig2.savefig(f"figures/{__file__}_Celtic_details.pdf")



fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
ax1 = [ax1]
ax2 = [ax2]




ax1[FACET].set_xlim(0, 1)
ax1[FACET].set_ylim(0, 1)
ax2[FACET].set_xlim(0, 1)
ax2[FACET].set_ylim(0, 1)
#ax[FACET].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] in ["Baltic", "Slavic"] else 0 for x in languages])
for i in range(9):
    ax1[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)
    ax2[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

meanFrontier = torch.stack([-x_coordinates[indices].mean(dim=0), -y_coordinates[indices].mean(dim=0)], dim=1)
inner_points = torch.logical_not((interiorCoordinates.unsqueeze(1) < meanFrontier.unsqueeze(0)).all(dim=2).any(dim=1))
ax1[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)
ax2[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)


#ax[FACET].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[FACET].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[FACET].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])

drawLine(ax1[FACET], ax2[FACET], "Eastern_Baltic", "Lithuanian_2.8")
drawLine(ax1[FACET], ax2[FACET], "Eastern_Baltic", "Latvian_2.8")
drawLine(ax1[FACET], ax2[FACET], "Balto_Slavic", "Eastern_Baltic")
drawLine(ax1[FACET], ax2[FACET], "Balto_Slavic", "Slavic")
drawLine(ax1[FACET], ax2[FACET], "Slavic", "South_Slavic")
drawLine(ax1[FACET], ax2[FACET], "Slavic", "Old_East_Slavic_2.8")
drawLine(ax1[FACET], ax2[FACET], "Old_East_Slavic_2.8", "Russian_2.8")
drawLine(ax1[FACET], ax2[FACET], "Old_East_Slavic_2.8", "Belarusian_2.8")
drawLine(ax1[FACET], ax2[FACET], "Old_East_Slavic_2.8", "Ukrainian_2.8")
drawLine(ax1[FACET], ax2[FACET], "Slavic", "West_Slavic")
drawLine(ax1[FACET], ax2[FACET], "West_Slavic", "Polish_2.8")
drawLine(ax1[FACET], ax2[FACET], "West_Slavic", "Czech_2.8")
drawLine(ax1[FACET], ax2[FACET], "West_Slavic", "Slovak_2.8")
drawLine(ax1[FACET], ax2[FACET], "West_Slavic", "Upper_Sorbian_2.8")
drawLine(ax1[FACET], ax2[FACET], "South_Slavic", "Old_Church_Slavonic_2.8")
drawLine(ax1[FACET], ax2[FACET], "Old_Church_Slavonic_2.8", "Bulgarian_2.8")




for i in [languages.index(x) for x in ["Latvian_2.8", "Lithuanian_2.8", "Old_Church_Slavonic_2.8", "Bulgarian_2.8", "Russian_2.8", "Belarusian_2.8", "Ukrainian_2.8", "Serbian_2.8", "Croatian_2.8", "Polish_2.8", "Czech_2.8", "Slovak_2.8", "Slovenian_2.8", "Old_East_Slavic_2.8", "Upper_Sorbian_2.8"]]:
#   ax[FACET].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
    plotLang(ax1[FACET], ax2[FACET], languages[i])
for hidden in ["Balto_Slavic", "Eastern_Baltic", "Slavic", "Western_South_Slavic", "South_Slavic", "West_Slavic", "Serbo_Croatian"]:
    plotLang(ax1[FACET], ax2[FACET], hidden)

ax1[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")
ax2[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")


#ax1[FACET].set_xlabel("Information Locality")
#ax1[FACET].set_ylabel("Dependency Locality")
#ax2[FACET].set_xlabel("Information Locality")
#ax2[FACET].set_ylabel("Dependency Locality")

if True:
    plt.show()

fig1.set_size_inches(1.5,2.5)
fig1.savefig(f"figures/{__file__}_Slavic.pdf")
fig2.set_size_inches(7,8)
fig2.savefig(f"figures/{__file__}_Slavic_details.pdf")




fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
ax1 = [ax1]
ax2 = [ax2]



FACET=0

ax1[FACET].set_xlim(0, 1)
ax1[FACET].set_ylim(0, 1)
ax2[FACET].set_xlim(0, 1)
ax2[FACET].set_ylim(0, 1)
#ax[FACET].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] in ["Indic", "Iranian"] else 0 for x in languages])
for i in range(9):
    ax1[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)
    ax2[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

meanFrontier = torch.stack([-x_coordinates[indices].mean(dim=0), -y_coordinates[indices].mean(dim=0)], dim=1)
inner_points = torch.logical_not((interiorCoordinates.unsqueeze(1) < meanFrontier.unsqueeze(0)).all(dim=2).any(dim=1))
ax1[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)
ax2[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)


#ax[FACET].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[FACET].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[FACET].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["Sanskrit_2.8", "Hindi_2.8", "Urdu_2.8", "Persian_2.8", "Kurmanji_2.8"]]:
#   ax[FACET].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
   plotLang(ax1[FACET], ax2[FACET], languages[i])
for hidden in ["Indo_Iranian", "Iranian"]:
    plotLang(ax1[FACET], ax2[FACET], hidden)
drawLine(ax1[FACET], ax2[FACET], "Indo_Iranian", "Iranian")
drawLine(ax1[FACET], ax2[FACET], "Indo_Iranian", "Sanskrit_2.8")
drawLine(ax1[FACET], ax2[FACET], "Sanskrit_2.8", "Hindustani")
drawLine(ax1[FACET], ax2[FACET], "Hindustani", "Hindi_2.8")
drawLine(ax1[FACET], ax2[FACET], "Hindustani", "Urdu_2.8")
drawLine(ax1[FACET], ax2[FACET], "Iranian", "Persian_2.8")
drawLine(ax1[FACET], ax2[FACET], "Iranian", "Kurmanji_2.8")

ax1[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")
ax2[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")



#ax1[FACET].set_xlabel("Information Locality")
#ax1[FACET].set_ylabel("Dependency Locality")
#ax2[FACET].set_xlabel("Information Locality")
#ax2[FACET].set_ylabel("Dependency Locality")

if True:
    plt.show()

fig1.set_size_inches(1.5,2.5)
fig1.savefig(f"figures/{__file__}_IndoIranian.pdf")
fig2.set_size_inches(7,8)
fig2.savefig(f"figures/{__file__}_IndoIranian_details.pdf")



fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
ax1 = [ax1]
ax2 = [ax2]



FACET=0

ax1[FACET].set_xlim(0, 1)
ax1[FACET].set_ylim(0, 1)
ax2[FACET].set_xlim(0, 1)
ax2[FACET].set_ylim(0, 1)
#ax[FACET].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] in ["Semitic", "Egyptian"] else 0 for x in languages])
for i in range(9):
    ax1[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)
    ax2[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

meanFrontier = torch.stack([-x_coordinates[indices].mean(dim=0), -y_coordinates[indices].mean(dim=0)], dim=1)
inner_points = torch.logical_not((interiorCoordinates.unsqueeze(1) < meanFrontier.unsqueeze(0)).all(dim=2).any(dim=1))
ax1[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)
ax2[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)


#ax[FACET].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[FACET].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[FACET].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["Coptic_2.8", "Arabic_2.8", "Maltese_2.8", "Hebrew_2.8", "Akkadian_2.8"]]:
#   ax[FACET].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
    plotLang(ax1[FACET], ax2[FACET], languages[i])
for hidden in ["Afroasiatic", "West_Semitic"]:
    plotLang(ax1[FACET], ax2[FACET], hidden)
drawLine(ax1[FACET], ax2[FACET], "Afroasiatic", "Semitic")
drawLine(ax1[FACET], ax2[FACET], "Afroasiatic", "Coptic_2.8")
drawLine(ax1[FACET], ax2[FACET], "Semitic", "West_Semitic")
drawLine(ax1[FACET], ax2[FACET], "Semitic", "Akkadian_2.8")
drawLine(ax1[FACET], ax2[FACET], "West_Semitic", "Amharic_2.8")
drawLine(ax1[FACET], ax2[FACET], "West_Semitic", "Central_Semitic")
drawLine(ax1[FACET], ax2[FACET], "Central_Semitic", "Arabic")
drawLine(ax1[FACET], ax2[FACET], "Central_Semitic", "Hebrew_2.8")
drawLine(ax1[FACET], ax2[FACET], "Arabic", "Arabic_2.8")
drawLine(ax1[FACET], ax2[FACET], "Arabic", "Maltese_2.8")

ax1[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")
ax2[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")


#ax1[FACET].set_xlabel("Information Locality")
#ax1[FACET].set_ylabel("Dependency Locality")
#ax2[FACET].set_xlabel("Information Locality")
#ax2[FACET].set_ylabel("Dependency Locality")

if True:
    plt.show()

fig1.set_size_inches(1.5,2.5)
fig1.savefig(f"figures/{__file__}_AfroAsiatic.pdf")
fig2.set_size_inches(7,8)
fig2.savefig(f"figures/{__file__}_AfroAsiatic_details.pdf")



fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
ax1 = [ax1]
ax2 = [ax2]




FACET=0

ax1[FACET].set_xlim(0, 1)
ax1[FACET].set_ylim(0, 1)
ax2[FACET].set_xlim(0, 1)
ax2[FACET].set_ylim(0, 1)
#ax[FACET].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] in ["Turkic"] else 0 for x in languages])
for i in range(9):
    ax1[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)
    ax2[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

meanFrontier = torch.stack([-x_coordinates[indices].mean(dim=0), -y_coordinates[indices].mean(dim=0)], dim=1)
inner_points = torch.logical_not((interiorCoordinates.unsqueeze(1) < meanFrontier.unsqueeze(0)).all(dim=2).any(dim=1))
ax1[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)
ax2[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)


#ax[FACET].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[FACET].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[FACET].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["Turkish_2.8", "Uyghur_2.8", "Kazakh_2.8"]]:
#   ax[FACET].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
   plotLang(ax1[FACET], ax2[FACET], languages[i])
for hidden in ["Common_Turkic"]:
    plotLang(ax1[FACET], ax2[FACET], hidden)
drawLine(ax1[FACET], ax2[FACET], "Common_Turkic", "Turkish_2.8")
drawLine(ax1[FACET], ax2[FACET], "Common_Turkic", "Uyghur_2.8")
drawLine(ax1[FACET], ax2[FACET], "Common_Turkic", "Kazakh_2.8")

ax1[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")
ax2[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")


#ax1[FACET].set_xlabel("Information Locality")
#ax1[FACET].set_ylabel("Dependency Locality")
#ax2[FACET].set_xlabel("Information Locality")
#ax2[FACET].set_ylabel("Dependency Locality")

if True:
    plt.show()

fig1.set_size_inches(1.5,2.5)
fig1.savefig(f"figures/{__file__}_Turkic.pdf")
fig2.set_size_inches(7,8)
fig2.savefig(f"figures/{__file__}_Turkic_details.pdf")



fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=(6, 4))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)
ax1 = [ax1]
ax2 = [ax2]




FACET=0

ax1[FACET].set_xlim(0, 1) 
ax1[FACET].set_ylim(0, 1) 
ax2[FACET].set_xlim(0, 1) 
ax2[FACET].set_ylim(0, 1) 
#ax[FACET].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] in ["Finnic", "Ugric", "Permic", "Uralic"] else 0 for x in languages])
for i in range(9):
    ax1[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)
    ax2[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

meanFrontier = torch.stack([-x_coordinates[indices].mean(dim=0), -y_coordinates[indices].mean(dim=0)], dim=1)
inner_points = torch.logical_not((interiorCoordinates.unsqueeze(1) < meanFrontier.unsqueeze(0)).all(dim=2).any(dim=1))
ax1[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)
ax2[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)


#ax[FACET].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[FACET].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[FACET].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["Finnish_2.8", "North_Sami_2.8", "Hungarian_2.8", "Erzya_2.8", "Komi_Zyrian_2.8"]]:
#   ax[FACET].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
   plotLang(ax1[FACET], ax2[FACET], languages[i])
for hidden in ["Uralic", "Finnic"]:
    plotLang(ax1[FACET], ax2[FACET], hidden)
drawLine(ax1[FACET], ax2[FACET], "Uralic", "Finnic")
drawLine(ax1[FACET], ax2[FACET], "Uralic", "Erzya_2.8")
drawLine(ax1[FACET], ax2[FACET], "Uralic", "Hungarian_2.8")
drawLine(ax1[FACET], ax2[FACET], "Uralic", "North_Sami_2.8")
drawLine(ax1[FACET], ax2[FACET], "Finnic", "Finnish_2.8")
drawLine(ax1[FACET], ax2[FACET], "Finnic", "Estonian_2.8")
drawLine(ax1[FACET], ax2[FACET], "Uralic", "Komi_Zyrian_2.8")

ax1[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")
ax2[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")

#ax1[FACET].set_xlabel("Information Locality")
#ax1[FACET].set_ylabel("Dependency Locality")
#ax2[FACET].set_xlabel("Information Locality")
#ax2[FACET].set_ylabel("Dependency Locality")

if True:
    plt.show()

fig1.set_size_inches(1.5,2.5)
fig1.savefig(f"figures/{__file__}_Uralic.pdf")
fig2.set_size_inches(7,8)
fig2.savefig(f"figures/{__file__}_Uralic_details.pdf")




#
#FACET=7
#
#languagesHere = ["Korean_2.8", "Thai_2.8", "Basque_2.8", "Japanese_2.8","Indonesian_2.8", "Vietnamese_2.8", "Mbya_Guarani_2.8", "Buryat_2.8"]
#
#
#ax[FACET].set_xlim(-1.3, 0.2)
#ax[FACET].set_ylim(-1.2,0.2)
##ax[FACET].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)
#
#indices = torch.ByteTensor([1 if x in languagesHere else 0 for x in languages])
#for i in range(9):
#    ax[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)
#
#meanFrontier = torch.stack([-x_coordinates[indices].mean(dim=0), -y_coordinates[indices].mean(dim=0)], dim=1)
#inner_points = torch.logical_not((interiorCoordinates.unsqueeze(1) < meanFrontier.unsqueeze(0)).all(dim=2).any(dim=1))
#ax[FACET].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)
#
#
##ax[FACET].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
##ax[FACET].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
##ax[FACET].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
#for i in [languages.index(x) for x in languagesHere]:
##   ax[FACET].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
#   plotLang(ax[FACET], languages[i])
##for hidden in ["Indo_European", "Indo_Iranian", "Germanic", "Balto_Slavic", "Slavic"]:
# #   plotLang(ax[FACET], hidden)
##drawLine(ax[FACET], "Common_Turkic", "Turkish_2.8")
#
#ax[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")
#




#
#ax[FACET].set_xlim(-0, 1)
#ax[FACET].set_ylim(-0,1)
##ax[FACET].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
#plot_x = []
#plot_y = []
#for i in range(len(languages)):
#  if languages[i] in ["Latvian_2.8", "Lithuanian_2.8", "Old_Church_Slavonic_2.8", "Bulgarian_2.8", "Russian_2.8", "Belarusian_2.8", "Ukrainian_2.8", "Serbian_2.8", "Croatian_2.8", "Polish_2.8", "Czech_2.8", "Slovak_2.8", "Slovenian_2.8", "Old_East_Slavic_2.8", "Upper_Sorbian_2.8"]:
#   print(languages[i])
#   plot_x.append(real_congruence[languages[i]])
#   plot_y.append(summary_data[(languages[i], "AverageFAngleSpace")])
#   ax[FACET].text(plot_x[-1], plot_y[-1], languages[i][:3].lower(), c=COLORS[categorical[languages[i]]])
#for hidden in ["Balto_Slavic", "Eastern_Baltic", "Slavic"]:
#    print(ancestral[hidden])
#    print(categorical)
#    plot_x.append(ancestral[hidden][0])
#    plot_y.append(ancestral[hidden][1])
#    ax[FACET].text(plot_x[-1], plot_y[-1], hidden[:3].lower(), c=COLORS[categorical[hidden]], alpha=0.2)
#drawLine(ax[FACET], "Balto_Slavic", "Eastern_Baltic")
#drawLine(ax[FACET], "Balto_Slavic", "Slavic")
#drawLine(ax[FACET], "Slavic", "Old_Church_Slavonic_2.8")
#drawLine(ax[FACET], "Slavic", "Old_East_Slavic_2.8")
#plot_x = torch.FloatTensor(plot_x)
#plot_y = torch.FloatTensor(plot_y)
#correlation = float(((plot_x - plot_x.mean()) * (plot_y - plot_y.mean())).mean() / (sd(plot_x, 0) * sd(plot_y, 0)))
#print("CORRELATION", correlation)
#ax[FACET].text(0, 1.1, f"R={round(correlation,3)}")
#
#
#


if False:
    plt.show()


