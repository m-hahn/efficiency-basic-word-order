import matplotlib.pyplot as plt
import torch


def getShape(x, names):
    names = [[int(q)-1 for q in y[y.index("[")+1:-1].split(",")] for y in names]
    N = len(names[0])
    size = [max([q[i] for q in names])+1 for i in range(N)]
    result = torch.zeros(size)
    for i in range(len(names)):
        result[tuple(names[i])] = x[i]
    return result

orders = ['No dominant order', 'SOV', 'SVO', 'VOS', 'VSO']


with open("../frontier_categorical/fits/76model.py_process9_frontier15.txt", "r") as inFile:
    fit = [[q for q in x.split(" ") if len(q) > 0] for x in inFile.read().strip().split("\n")]
alpha_fit = [x for x in fit if len(x) > 0 and x[0].startswith("alpha")]
print(alpha_fit)
alpha = torch.FloatTensor([float(x[1]) for x in alpha_fit])
print(alpha)
alpha_names = [x[0] for x in alpha_fit]
print(alpha_names)

alpha_frontier_shape = getShape(alpha, alpha_names)
alpha_frontier_shape_x = alpha_frontier_shape[:,:4]
alpha_frontier_shape_y = alpha_frontier_shape[:,4:]





with open("../frontier_categorical/fits/71model.py_process9_frontier14.txt", "r") as inFile:
    fit = [[q for q in x.split(" ") if len(q) > 0] for x in inFile.read().strip().split("\n")]
alpha_fit = [x for x in fit if len(x) > 0 and x[0].startswith("alpha")]
print(alpha_fit)
alpha = torch.FloatTensor([float(x[1]) for x in alpha_fit])
print(alpha)
alpha_names = [x[0] for x in alpha_fit]
print(alpha_names)

alpha = getShape(alpha, alpha_names)


Omega_fit = [x for x in fit if len(x) > 0 and x[0].startswith("Omega")]
print(Omega_fit)
Omega = torch.FloatTensor([float(x[1]) for x in Omega_fit])
print(Omega)
Omega_names = [x[0] for x in Omega_fit]
print(Omega_names)

Omega = getShape(Omega, Omega_names)

grid = torch.FloatTensor([[((x)/20-0.5,(y)/20) for y in range(60)] for x in range(60)]).view(-1, 2)
print(grid)

print(alpha[1])

print(Omega[1])
print(alpha[1])

import math

fig, ax = plt.subplots(nrows=1, ncols=4, figsize=(12, 6))


density = (torch.exp(-(torch.matmul((grid - alpha[1,:2].unsqueeze(0)), Omega[1,:2,:2].inverse()) * (grid - alpha[1,:2].unsqueeze(0))).sum(dim=1)))
print(density)
ax[0].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60))
ax[0].set_xlim(-2, 0)
ax[0].set_ylim(-2,0)
ax[0].plot(-alpha_frontier_shape_x[1], -alpha_frontier_shape_y[1])



density = (torch.exp(-(torch.matmul((grid - alpha[2,:2].unsqueeze(0)), Omega[2,:2,:2].inverse()) * (grid - alpha[2,:2].unsqueeze(0))).sum(dim=1)))
ax[1].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60))
ax[1].set_xlim(-2, 0)
ax[1].set_ylim(-2,0)
ax[1].plot(-alpha_frontier_shape_x[2], -alpha_frontier_shape_y[2])


density = (torch.exp(-(torch.matmul((grid - alpha[4,:2].unsqueeze(0)), Omega[4,:2,:2].inverse()) * (grid - alpha[4,:2].unsqueeze(0))).sum(dim=1)))
ax[2].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60), colors='b')
ax[2].set_xlim(-2, 0)
ax[2].set_ylim(-2,0)
ax[2].plot(-alpha_frontier_shape_x[4], -alpha_frontier_shape_y[4])

density = (torch.exp(-(torch.matmul((grid - alpha[0,:2].unsqueeze(0)), Omega[0,:2,:2].inverse()) * (grid - alpha[0,:2].unsqueeze(0))).sum(dim=1)))
ax[3].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60), colors='r')
ax[3].set_xlim(-2, 0)
ax[3].set_ylim(-2,0)
ax[3].plot(-alpha_frontier_shape_x[0], -alpha_frontier_shape_y[0])




#ax[1].contour(grid_transformed_x.view(20, 7*20), grid_transformed_y.view(20, 7*20), density_transformed.view(20, 7*20))
#ax[1].scatter(circle[:,0], circle[:,1], c=color_circle.numpy().tolist(), cmap='viridis', vmin=0, vmax=1)
#ax[1].set_xlim(-1.5, 1)
#ax[1].set_ylim(-1.5,1)

#print(color_circle)


plt.show()



