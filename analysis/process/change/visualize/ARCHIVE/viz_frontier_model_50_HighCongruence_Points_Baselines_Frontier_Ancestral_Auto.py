import matplotlib.pyplot as plt
import torch

with open("../../../ud_languages_2.8.txt", "r") as inFile:
    languages = inFile.read().strip().split("\n")


summary_data = {}
with open("../../output/process9_frontier18.py.tsv", "r") as inFile:
    for line in inFile:
        line = line.strip().split(" ")
        summary_data[(line[0], line[1])] = float(line[2])
print(summary_data)

x_coordinates = {}
y_coordinates = {}
for language in languages:
 with open(f"../../output_frontier/process9_frontier18.py_{language}_FrontierDiscretizedByAngle.tsv", "r") as inFile:
    frontier = [float(x.split(" ")[2]) for x in inFile.read().strip().split("\n")]
    x = frontier[::2]
    y = frontier[1::2]
    x_coordinates[language] = x
    y_coordinates[language] = y

fOnFrontier = {}
for language in languages:
  with open(f"../../output_frontier/process9_frontier18.py_{language}_fOnFrontierDiscretizedByAngle.tsv", "r") as inFile:
     fOnFrontier[language] = [float(x.split(" ")[1]) for x in inFile.read().strip().split("\n")]

fOnFrontier = torch.FloatTensor([fOnFrontier[x] for x in languages])
      


print(x_coordinates)

x_coordinates = torch.FloatTensor([x_coordinates[x] for x in languages])
y_coordinates = torch.FloatTensor([y_coordinates[y] for y in languages])
print(x_coordinates.mean(dim=0))
print(y_coordinates.mean(dim=0))


x_positions_langs = torch.FloatTensor([summary_data[(x,"Distance_Surprisal_ComparedFrontier")] for x in languages])
y_positions_langs = torch.FloatTensor([summary_data[(y,"Distance_DepLen_ComparedFrontier")] for y in languages])


def sd(tensor, dim):
    return (tensor.pow(2).mean(dim=dim) - tensor.mean(dim=dim).pow(2)).sqrt()

families = {}
for q in ["../../../../languages/families_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        families[line[0]] = line[1]


categorical = {}
for q in ["../../../../languages/categorical_order/categorical_order_2.8.tsv", "../../../../languages/categorical_order/categoricalOrderAdditional_2.8.tsv", "../../../../languages/categorical_order/categoricalOrder_groups_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        categorical[line[0]] = line[1]

COLORS = {"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}



def getShape(x, names):
    names = [[int(q)-1 for q in y[y.index("[")+1:-1].split(",")] for y in names]
    N = len(names[0])
    size = [max([q[i] for q in names])+1 for i in range(N)]
    result = torch.zeros(size)
    for i in range(len(names)):
        result[tuple(names[i])] = x[i]
    return result

orders = ['No dominant order', 'SOV', 'SVO', 'VOS', 'VSO']


ancestral = {}
with open("../frontier_historical/fits/90model.py_process9_frontier18_Revised.txt", "r") as inFile:
     fit = [[q for q in x.split(" ") if len(q) > 0] for x in inFile.read().strip().split("\n")]

FFrontierHidden_fit = [x for x in fit if len(x) > 0 and x[0].startswith("FFrontierHidden")]
print(FFrontierHidden_fit)
FFrontierHidden = torch.FloatTensor([float(x[1]) for x in FFrontierHidden_fit])
print(FFrontierHidden)
FFrontierHidden_names = [x[0] for x in FFrontierHidden_fit]
print(FFrontierHidden_names)

FFrontierHidden_frontier_shape = getShape(FFrontierHidden, FFrontierHidden_names)
print(FFrontierHidden_frontier_shape)

ancestralStates = [x for x in fit if len(x) > 0 and x[0] == "HIDDEN_LANGUAGES"][0][1:]
print(ancestralStates)
print(len(ancestralStates))
print(FFrontierHidden_frontier_shape.size())
assert len(ancestralStates)+1 == FFrontierHidden_frontier_shape.size()[0]

FFrontierHidden_frontier_shape[:,2] = (0.5*FFrontierHidden_frontier_shape[:,2])+0.5
print(FFrontierHidden_frontier_shape)
for i in range(len(ancestralStates)):
    ancestral[ancestralStates[i]] = FFrontierHidden_frontier_shape[i+1]
print(ancestral)
 


grid = torch.FloatTensor([[((x)/20-1.5,(y)/20-1.5) for y in range(60)] for x in range(60)]).view(-1, 2)

x_variance_langs = torch.FloatTensor([summary_data[(x,"Baseline_Variance_X_ComparedToFrontier")] for x in languages])
y_variance_langs = torch.FloatTensor([summary_data[(x,"Baseline_Variance_Y_ComparedToFrontier")] for x in languages])
xy_covariance_langs = torch.FloatTensor([summary_data[(x,"Baseline_Covariance_XY_ComparedToFrontier")] for x in languages])

baselines_covariance = torch.stack([x_variance_langs, xy_covariance_langs, xy_covariance_langs, y_variance_langs], dim=1).view(80, 2,2)

print(grid.size(), x_variance_langs.size())
print(baselines_covariance.size())
baseline_density = []
for i in range(len(languages)):
  baseline_density.append(torch.exp(-(grid.t() * torch.matmul(baselines_covariance[i].inverse(), grid.t())).sum(dim=0)))

baseline_densities = torch.stack(baseline_density, dim=0)
baseline_density = torch.stack(baseline_density, dim=0).mean(dim=0)


def drawLine(axis, old, new):
    if old in ancestral:
        x1 = -float(ancestral[old][0])
        y1 = -float(ancestral[old][1])
    else:
        x1 = -x_positions_langs[languages.index(old)]
        y1 = -y_positions_langs[languages.index(old)]
    if new in ancestral:
        x2 = -float(ancestral[new][0])
        y2 = -float(ancestral[new][1])
    else:
        x2 = -x_positions_langs[languages.index(new)]
        y2 = -y_positions_langs[languages.index(new)]
    axis.plot([x1, x2], [y1, y2], c="gray", alpha=0.5)
#    axis.arrow(x1, y1, dx=x2-x1, dy=y2-y1, color="gray", alpha=0.5, head_width = 0.04, length_includes_head=True)


def plotLang(axis, lang):

    if lang in ancestral:
        x2 = -float(ancestral[lang][0])
        y2 = -float(ancestral[lang][1])
        axis.scatter([x2], [y2], c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[lang]], alpha=(0.2 if lang in ancestral else 1))

    else:
        x2 = -x_positions_langs[languages.index(lang)]
        y2 = -y_positions_langs[languages.index(lang)]
        axis.text(x2, y2, lang[:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[lang]], alpha=(0.2 if lang in ancestral else 1))













with open("../frontier/trees2_2.8.tsv", "r") as inFile:
  trees = [x.split("\t") for x in inFile.read().strip().split("\n")][1:]
print(trees)

observedLangs = set()
allLangs = set()
parents = {}
for line in trees:
   observedLangs.add(line[0])
   allLangs.add(line[0])
   for i in range(len(line)-1):
       child = line[i]
       parent = line[i+1]
       if child in parents:
         assert parents[child] == parent, (parents[child], line, child, i)
       parents[child] = parent
       allLangs.add(parent)

with open("../frontier/groups2_2.8.tsv", "r") as inFile:
  dates = [x.split("\t") for x in inFile.read().strip().split("\n")][1:]
print([len(x) for x in dates])
dates = dict(dates)
dates["_ROOT_"] = -50000
print(dates)
for x in allLangs:
  if x not in observedLangs:
    if x not in dates:
       print(x)


#print(observedLangs)
#print(allLangs)
#hiddenLangs = sorted([x for x in allLangs if x not in observedLangs and x != "_ROOT_"])
#print(hiddenLangs)
#
##observedLanguages = [x for x in list(observedLangs) if parents[x] not in observedLangs] # This is for understanding what the model does on only synchronic data
#observedLanguages = [x for x in list(observedLangs) if x in valueByLanguage]
#hiddenLanguages = hiddenLangs
#totalLanguages = ["_ROOT_"] + hiddenLanguages + observedLanguages
#lang2Code = dict(list(zip(totalLanguages, range(len(totalLanguages)))))
#lang2Observed = dict(list(zip(observedLanguages, range(len(observedLanguages)))))
#
#
#assert 'Latin_2.8' in valueByLanguage, valueByLanguage
#assert 'Latin_2.8' in totalLanguages
#assert 'Latin_2.8' in lang2Code
#
#distanceToParent = {}
#for language in allLangs:
#   parent = parents.get(language, "_ROOT_")
#   if language in observedLangs and parent == "_ROOT_":
#     print("ROOT", language)
#   dateLang = dates.get(language, 2000)
#   dateParent = dates[parent]
#   distanceToParent[language] = (float(dateLang)-float(dateParent))/1000
#print(distanceToParent)
#print(parents.get("Classical_Chinese_2.8"))
#assert "Classical_Chinese_2.8" in observedLangs
##quit()
#
#from collections import defaultdict
#
#
#listOfAllAncestors = defaultdict(list)
#
#def processDescendants(l, desc):
#   listOfAllAncestors[desc].append(l)
#   if l != "_ROOT_":
#      parent = parents.get(l, "_ROOT_")
#      processDescendants(parent, desc)
#   
#for language in totalLanguages:
#   processDescendants(language, language)
#print(listOfAllAncestors)
#
#
#
#
#covarianceMatrix = [[None for _ in range(len(observedLanguages))] for _ in range(len(observedLanguages))]
#for i in range(len(observedLanguages)):
#   for j in range(i+1):
#      l1 = observedLanguages[i]
#      l2 = observedLanguages[j]
##      print("----------")
# #     print([l for l in listOfAllAncestors[l1] if l in listOfAllAncestors[l2]])
#  #    print([int(dates.get(l, 2000)) for l in listOfAllAncestors[l1] if l in listOfAllAncestors[l2]])
#      commonAncestorsDates = [int(dates.get(l, 2000)) for l in listOfAllAncestors[l1] if l in listOfAllAncestors[l2] and l != "_ROOT_"]
#      if len(commonAncestorsDates) > 0:
#         commonTime = max(commonAncestorsDates)
#      else:
#         commonTime = -50000
##      print(l1, l2, "Common time:", commonTime)
#      separateTime1 = (int(dates.get(l1, 2000)) - commonTime)
#      separateTime2 = (int(dates.get(l2, 2000)) - commonTime)
#      if commonTime > -50000: # and False:
#         print(l1, l2, separateTime1, separateTime2)
#      else:
#         separateTime1 = 1000000
#         separateTime2 = 1000000
#      covarianceMatrix[i][j] = (separateTime1)/1000
#      covarianceMatrix[j][i] = (separateTime2)/1000
#
#families = defaultdict(list)
#for language in observedLanguages:
#  ancestors = listOfAllAncestors[language]
#  print(ancestors)
#  assert ancestors[-1] == "_ROOT_"
#  families[([language] + ancestors)[-2]].append(language)
#print(families)
#familiesLists = [[observedLanguages.index(x)+1 for x in z] for _, z in families.items()]
#print(familiesLists)
#familiesListsMaxLen = max([len(x) for x in familiesLists])
#familiesLists = [x+[0 for _ in range(familiesListsMaxLen-len(x))] for x in familiesLists]
#print(familiesLists)
#
#
#
#
#
#
#
#
#
#








import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

viridis = cm.get_cmap('viridis', 8)

import math

fig, ax = plt.subplots(nrows=1, ncols=8, figsize=(12, 6))


print(fOnFrontier.size())
print(x_coordinates.size())

ax[0].set_xlim(-1.3, 0.2)
ax[0].set_ylim(-1.2,0.2)
#ax[0].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] == "Latin_Romance" else 0 for x in languages])
for i in range(9):
    ax[0].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

#ax[0].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[0].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["French_2.8", "Old_French_2.8", "Italian_2.8", "Spanish_2.8", "Portuguese_2.8", "Latin_2.8", "Romanian_2.8", "Galician_2.8", "Catalan_2.8"]]:
#   ax[0].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
#   ax[0].plot([-x_positions_langs[i]], [-y_positions_langs[i]], 'o', color=viridis(summary_data[(languages[i], "AverageFAngleSpace")]), linewidth=5)
   ax[0].scatter([-x_positions_langs[i]], [-y_positions_langs[i]], c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
for hidden in ["Italo_Western_Romance", "Western_Romance", "Iberian_Romance", "West_Iberian"]:
    plotLang(ax[0], hidden)
drawLine(ax[0], "Latin_2.8", "Italo_Western_Romance")
drawLine(ax[0], "Italo_Western_Romance", "Italian_2.8")
drawLine(ax[0], "Italo_Western_Romance", "Western_Romance")
drawLine(ax[0], "Western_Romance", "Old_French_2.8")
drawLine(ax[0], "Old_French_2.8", "French_2.8")
drawLine(ax[0], "Western_Romance", "Iberian_Romance")
drawLine(ax[0], "Iberian_Romance", "Catalan_2.8")
drawLine(ax[0], "Iberian_Romance", "West_Iberian")
drawLine(ax[0], "West_Iberian", "Spanish_2.8")
drawLine(ax[0], "West_Iberian", "Galician_2.8")
drawLine(ax[0], "West_Iberian", "Portuguese_2.8")

ax[0].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")







ax[1].set_xlim(-1.3, 0.2)
ax[1].set_ylim(-1.2,0.2)
#ax[0].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] == "Latin_Romance" else 0 for x in languages])
for i in range(9):
    ax[1].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

#ax[0].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[0].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["Norwegian_2.8", "Swedish_2.8", "Danish_2.8", "Icelandic_2.8", "Faroese_2.8", "Gothic_2.8", "English_2.8", "Dutch_2.8", "Afrikaans_2.8", "German_2.8", "Naija_2.8"]]:
   ax[1].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
for hidden in ["West_Scandinavian", "North_Germanic", "West_Germanic", "Germanic", "Icelandic_Faroese"]:
    plotLang(ax[1], hidden)
drawLine(ax[1], "Germanic", "Gothic_2.8")
drawLine(ax[1], "Germanic", "West_Germanic")
drawLine(ax[1], "Germanic", "North_Germanic")
drawLine(ax[1], "West_Germanic", "German_2.8")
drawLine(ax[1], "West_Germanic", "Macro_English")
drawLine(ax[1], "Macro_English", "English_2.8")
drawLine(ax[1], "Macro_English", "Naija_2.8")
drawLine(ax[1], "West_Germanic", "Global_Dutch")
drawLine(ax[1], "Global_Dutch", "Dutch_2.8")
drawLine(ax[1], "Global_Dutch", "Afrikaans_2.8")
drawLine(ax[1], "North_Germanic", "Danish_2.8")
drawLine(ax[1], "North_Germanic", "Swedish_2.8")
drawLine(ax[1], "North_Germanic", "West_Scandinavian")
drawLine(ax[1], "West_Scandinavian", "Norwegian_2.8")
drawLine(ax[1], "West_Scandinavian", "Icelandic_2.8")
drawLine(ax[1], "West_Scandinavian", "Faroese_2.8")

ax[1].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")






ax[2].set_xlim(-1.3, 0.2)
ax[2].set_ylim(-1.2,0.2)
#ax[0].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] == "Celtic" else 0 for x in languages])
for i in range(9):
    ax[2].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

#ax[0].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[0].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["Irish_2.8", "Breton_2.8", "Scottish_Gaelic_2.8", "Welsh_2.8", "Manx_2.8"]]:
   ax[2].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
for hidden in ["Insular_Celtic", "Brythonic", "Goidelic"]:
    plotLang(ax[2], hidden)
drawLine(ax[2], "Insular_Celtic", "Brythonic")
drawLine(ax[2], "Insular_Celtic", "Goidelic")
drawLine(ax[2], "Goidelic", "Irish_2.8")
drawLine(ax[2], "Goidelic", "Scottish_Gaelic_2.8")
drawLine(ax[2], "Goidelic", "Manx_2.8")
drawLine(ax[2], "Brythonic", "Welsh_2.8")
drawLine(ax[2], "Brythonic", "Breton_2.8")

ax[2].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")




ax[3].set_xlim(-1.3, 0.2)
ax[3].set_ylim(-1.2,0.2)
#ax[0].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] in ["Baltic", "Slavic"] else 0 for x in languages])
for i in range(9):
    ax[3].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

#ax[0].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[0].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["Latvian_2.8", "Lithuanian_2.8", "Old_Church_Slavonic_2.8", "Bulgarian_2.8", "Russian_2.8", "Belarusian_2.8", "Ukrainian_2.8", "Serbian_2.8", "Croatian_2.8", "Polish_2.8", "Czech_2.8", "Slovak_2.8", "Slovenian_2.8", "Old_East_Slavic_2.8", "Upper_Sorbian_2.8"]]:
   ax[3].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
for hidden in ["Balto_Slavic", "Eastern_Baltic", "Slavic", "Western_South_Slavic", "South_Slavic", "West_Slavic", "Serbo_Croatian"]:
    plotLang(ax[3], hidden)
drawLine(ax[3], "Eastern_Baltic", "Lithuanian_2.8")
drawLine(ax[3], "Eastern_Baltic", "Latvian_2.8")
drawLine(ax[3], "Balto_Slavic", "Eastern_Baltic")
drawLine(ax[3], "Balto_Slavic", "Slavic")
drawLine(ax[3], "Slavic", "South_Slavic")
drawLine(ax[3], "Slavic", "Old_East_Slavic_2.8")
drawLine(ax[3], "Old_East_Slavic_2.8", "Russian_2.8")
drawLine(ax[3], "Old_East_Slavic_2.8", "Belarusian_2.8")
drawLine(ax[3], "Old_East_Slavic_2.8", "Ukrainian_2.8")
drawLine(ax[3], "Slavic", "West_Slavic")
drawLine(ax[3], "West_Slavic", "Polish_2.8")
drawLine(ax[3], "West_Slavic", "Czech_2.8")
drawLine(ax[3], "West_Slavic", "Slovak_2.8")
drawLine(ax[3], "West_Slavic", "Upper_Sorbian_2.8")
drawLine(ax[3], "South_Slavic", "Old_Church_Slavonic_2.8")
drawLine(ax[3], "Old_Church_Slavonic_2.8", "Bulgarian_2.8")


ax[3].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")



FACET=4

ax[FACET].set_xlim(-1.3, 0.2)
ax[FACET].set_ylim(-1.2,0.2)
#ax[0].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] in ["Indic", "Iranian"] else 0 for x in languages])
for i in range(9):
    ax[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

#ax[0].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[0].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["Sanskrit_2.8", "Hindi_2.8", "Urdu_2.8", "Persian_2.8", "Kurmanji_2.8"]]:
   ax[FACET].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
for hidden in ["Indo_Iranian", "Iranian"]:
    plotLang(ax[FACET], hidden)
drawLine(ax[FACET], "Indo_Iranian", "Iranian")
drawLine(ax[FACET], "Indo_Iranian", "Sanskrit_2.8")
drawLine(ax[FACET], "Sanskrit_2.8", "Hindustani")
drawLine(ax[FACET], "Hindustani", "Hindi_2.8")
drawLine(ax[FACET], "Hindustani", "Urdu_2.8")
drawLine(ax[FACET], "Iranian", "Persian_2.8")
drawLine(ax[FACET], "Iranian", "Kurmanji_2.8")

ax[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")




FACET=5

ax[FACET].set_xlim(-1.3, 0.2)
ax[FACET].set_ylim(-1.2,0.2)
#ax[0].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] in ["Semitic", "Egyptian"] else 0 for x in languages])
for i in range(9):
    ax[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

#ax[0].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[0].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["Coptic_2.8", "Arabic_2.8", "Maltese_2.8", "Hebrew_2.8", "Akkadian_2.8", "Amharic_2.8"]]:
   ax[FACET].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
for hidden in ["Afroasiatic", "West_Semitic"]:
    plotLang(ax[FACET], hidden)
drawLine(ax[FACET], "Afroasiatic", "Semitic")
drawLine(ax[FACET], "Afroasiatic", "Coptic_2.8")
drawLine(ax[FACET], "Semitic", "West_Semitic")
drawLine(ax[FACET], "Semitic", "Akkadian_2.8")
drawLine(ax[FACET], "West_Semitic", "Amharic_2.8")

ax[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")





FACET=6

ax[FACET].set_xlim(-1.3, 0.2)
ax[FACET].set_ylim(-1.2,0.2)
#ax[0].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] in ["Turkic"] else 0 for x in languages])
for i in range(9):
    ax[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

#ax[0].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[0].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["Turkish_2.8", "Uyghur_2.8", "Kazakh_2.8"]]:
   ax[FACET].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
for hidden in ["Common_Turkic"]:
    plotLang(ax[FACET], hidden)
drawLine(ax[FACET], "Common_Turkic", "Turkish_2.8")

ax[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")



FACET=7

ax[FACET].set_xlim(-1.3, 0.2)
ax[FACET].set_ylim(-1.2,0.2)
#ax[0].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] in ["Turkic"] else 0 for x in languages])
for i in range(9):
    ax[FACET].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)

#ax[0].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[0].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in [languages.index(x) for x in ["Sanskrit_2.8", "Latin_2.8", "Ancient_Greek_2.8", "Gothic_2.8", "Old_Church_Slavonic_2.8"]]:
   ax[FACET].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
for hidden in ["Indo_European", "Indo_Iranian", "Germanic", "Balto_Slavic", "Slavic"]:
    plotLang(ax[FACET], hidden)
#drawLine(ax[FACET], "Common_Turkic", "Turkish_2.8")

ax[FACET].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")





#
#ax[3].set_xlim(-0, 1)
#ax[3].set_ylim(-0,1)
##ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
#plot_x = []
#plot_y = []
#for i in range(len(languages)):
#  if languages[i] in ["Latvian_2.8", "Lithuanian_2.8", "Old_Church_Slavonic_2.8", "Bulgarian_2.8", "Russian_2.8", "Belarusian_2.8", "Ukrainian_2.8", "Serbian_2.8", "Croatian_2.8", "Polish_2.8", "Czech_2.8", "Slovak_2.8", "Slovenian_2.8", "Old_East_Slavic_2.8", "Upper_Sorbian_2.8"]:
#   print(languages[i])
#   plot_x.append(real_congruence[languages[i]])
#   plot_y.append(summary_data[(languages[i], "AverageFAngleSpace")])
#   ax[3].text(plot_x[-1], plot_y[-1], languages[i][:3].lower(), c=COLORS[categorical[languages[i]]])
#for hidden in ["Balto_Slavic", "Eastern_Baltic", "Slavic"]:
#    print(ancestral[hidden])
#    print(categorical)
#    plot_x.append(ancestral[hidden][0])
#    plot_y.append(ancestral[hidden][1])
#    ax[3].text(plot_x[-1], plot_y[-1], hidden[:3].lower(), c=COLORS[categorical[hidden]], alpha=0.2)
#drawLine(ax[3], "Balto_Slavic", "Eastern_Baltic")
#drawLine(ax[3], "Balto_Slavic", "Slavic")
#drawLine(ax[3], "Slavic", "Old_Church_Slavonic_2.8")
#drawLine(ax[3], "Slavic", "Old_East_Slavic_2.8")
#plot_x = torch.FloatTensor(plot_x)
#plot_y = torch.FloatTensor(plot_y)
#correlation = float(((plot_x - plot_x.mean()) * (plot_y - plot_y.mean())).mean() / (sd(plot_x, 0) * sd(plot_y, 0)))
#print("CORRELATION", correlation)
#ax[3].text(0, 1.1, f"R={round(correlation,3)}")
#
#
#


plt.show()


