import matplotlib.pyplot as plt
import torch

with open("../../../ud_languages_2.8.txt", "r") as inFile:
    languages = inFile.read().strip().split("\n")

real_congruence = {}
with open("../../../landscapes_noPOS_2.8_new.R.tsv", "r") as inFile:
    data = [x.split(" ") for x in inFile.read().strip().replace('"', "").split("\n")]
    header = data[0]
    for line in data[1:]:
        real_congruence[line[1+header.index("Language")]] = float(line[1+header.index("Congruence_All")])
print(real_congruence)
        


summary_data = {}
with open("../../output/process9_frontier18.py.tsv", "r") as inFile:
    for line in inFile:
        line = line.strip().split(" ")
        summary_data[(line[0], line[1])] = float(line[2])
print(summary_data)

x_coordinates = {}
y_coordinates = {}
for language in languages:
 with open(f"../../output_frontier/process9_frontier18.py_{language}_FrontierDiscretizedByAngle.tsv", "r") as inFile:
    frontier = [float(x.split(" ")[2]) for x in inFile.read().strip().split("\n")]
    x = frontier[::2]
    y = frontier[1::2]
    x_coordinates[language] = x
    y_coordinates[language] = y

fOnFrontier = {}
for language in languages:
  with open(f"../../output_frontier/process9_frontier18.py_{language}_fOnFrontierDiscretizedByAngle.tsv", "r") as inFile:
     fOnFrontier[language] = [float(x.split(" ")[1]) for x in inFile.read().strip().split("\n")]

fOnFrontier = torch.FloatTensor([fOnFrontier[x] for x in languages])
      


print(x_coordinates)

x_coordinates = torch.FloatTensor([x_coordinates[x] for x in languages])
y_coordinates = torch.FloatTensor([y_coordinates[y] for y in languages])
print(x_coordinates.mean(dim=0))
print(y_coordinates.mean(dim=0))


x_positions_langs = torch.FloatTensor([summary_data[(x,"Distance_Surprisal_ComparedFrontier")] for x in languages])
y_positions_langs = torch.FloatTensor([summary_data[(y,"Distance_DepLen_ComparedFrontier")] for y in languages])


def sd(tensor, dim):
    return (tensor.pow(2).mean(dim=dim) - tensor.mean(dim=dim).pow(2)).sqrt()

families = {}
for q in ["../../../../languages/families_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        families[line[0]] = line[1]


categorical = {}
for q in ["../../../../languages/categorical_order/categorical_order_2.8.tsv", "../../../../languages/categorical_order/categoricalOrderAdditional_2.8.tsv", "../../../../languages/categorical_order/categoricalOrder_groups_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        categorical[line[0]] = line[1]

COLORS = {"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}



def getShape(x, names):
    names = [[int(q)-1 for q in y[y.index("[")+1:-1].split(",")] for y in names]
    N = len(names[0])
    size = [max([q[i] for q in names])+1 for i in range(N)]
    result = torch.zeros(size)
    for i in range(len(names)):
        result[tuple(names[i])] = x[i]
    return result

orders = ['No dominant order', 'SOV', 'SVO', 'VOS', 'VSO']






ancestral = {}
with open("../frontier/fits/91model.py_process9_frontier18.txt", "r") as inFile:
     fit = [[q for q in x.split(" ") if len(q) > 0] for x in inFile.read().strip().split("\n")]

FFrontierHidden_fit = [x for x in fit if len(x) > 0 and x[0].startswith("FFrontierHidden")]
print(FFrontierHidden_fit)
FFrontierHidden = torch.FloatTensor([float(x[1]) for x in FFrontierHidden_fit])
print(FFrontierHidden)
FFrontierHidden_names = [x[0] for x in FFrontierHidden_fit]
print(FFrontierHidden_names)

FFrontierHidden_frontier_shape = getShape(FFrontierHidden, FFrontierHidden_names)
print(FFrontierHidden_frontier_shape)

ancestralStates = [x for x in fit if len(x) > 0 and x[0] == "HIDDEN_LANGUAGES"][0][1:]
print(ancestralStates)
print(len(ancestralStates))
print(FFrontierHidden_frontier_shape.size())
assert len(ancestralStates)+1 == FFrontierHidden_frontier_shape.size()[0]

FFrontierHidden_frontier_shape[:,0] = (0.5*FFrontierHidden_frontier_shape[:,0])+0.5
print(FFrontierHidden_frontier_shape)
for i in range(len(ancestralStates)):
    ancestral[ancestralStates[i]] = FFrontierHidden_frontier_shape[i+1]
print(ancestral)
 

import math

fig, ax = plt.subplots(nrows=1, ncols=5, figsize=(12, 6))


ax[0].set_xlim(-0.2, 1.2)
ax[0].set_ylim(-0.2,1.2)
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
plot_x = []
plot_y = []
ax[0].plot([0,1], [0,1])
for i in range(len(languages)):
   plot_x.append(real_congruence[languages[i]])
   plot_y.append(summary_data[(languages[i], "AverageFAngleSpace")])
   ax[0].text(plot_x[i], plot_y[i], languages[i][:3].lower(), c=COLORS[categorical[languages[i]]])
plot_x = torch.FloatTensor(plot_x)
plot_y = torch.FloatTensor(plot_y)
correlation = float(((plot_x - plot_x.mean()) * (plot_y - plot_y.mean())).mean() / (sd(plot_x, 0) * sd(plot_y, 0)))
print("CORRELATION", correlation)
ax[0].text(0, 1.1, f"R={round(correlation,3)}")


def drawLine(axis, old, new):
    if old in ancestral:
        x1 = float(ancestral[old][0])
        y1 = float(ancestral[old][1])
    else:
        x1 = real_congruence[old]
        y1 = summary_data[(old, "AverageFAngleSpace")]
    if new in ancestral:
        x2 = float(ancestral[new][0])
        y2 = float(ancestral[new][1])
    else:
        x2 = real_congruence[new]
        y2 = summary_data[(new, "AverageFAngleSpace")]
    axis.plot([x1, x2], [y1, y2], c="gray", alpha=0.5)




ax[1].set_xlim(-0.2, 1.2)
ax[1].set_ylim(-0.2,1.2)
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
plot_x = []
plot_y = []
for i in range(len(languages)):
 if languages[i] in ["Norwegian_2.8", "Swedish_2.8", "Danish_2.8", "Icelandic_2.8", "Faroese_2.8", "Gothic_2.8", "English_2.8", "Dutch_2.8", "Afrikaans_2.8", "German_2.8", "Naija_2.8"]:
   print(languages[i])
   plot_x.append(real_congruence[languages[i]])
   plot_y.append(summary_data[(languages[i], "AverageFAngleSpace")])
   ax[1].text(plot_x[-1], plot_y[-1], languages[i][:3].lower(), c=COLORS[categorical[languages[i]]])
for hidden in ["West_Scandinavian", "North_Germanic", "West_Germanic", "Germanic", "Icelandic_Faroese"]:
    print(ancestral[hidden])
    print(categorical)
    plot_x.append(ancestral[hidden][0])
    plot_y.append(ancestral[hidden][1])
    ax[1].text(plot_x[-1], plot_y[-1], hidden[:3].lower(), c=COLORS[categorical[hidden]], alpha=0.2)
drawLine(ax[1], "Germanic", "Gothic_2.8")
drawLine(ax[1], "Germanic", "West_Germanic")
drawLine(ax[1], "Germanic", "North_Germanic")
drawLine(ax[1], "West_Germanic", "German_2.8")
drawLine(ax[1], "West_Germanic", "Macro_English")
drawLine(ax[1], "Macro_English", "English_2.8")
drawLine(ax[1], "Macro_English", "Naija_2.8")
drawLine(ax[1], "West_Germanic", "Global_Dutch")
drawLine(ax[1], "Global_Dutch", "Dutch_2.8")
drawLine(ax[1], "Global_Dutch", "Afrikaans_2.8")
drawLine(ax[1], "North_Germanic", "Danish_2.8")
drawLine(ax[1], "North_Germanic", "Swedish_2.8")
drawLine(ax[1], "North_Germanic", "West_Scandinavian")
drawLine(ax[1], "West_Scandinavian", "Norwegian_2.8")
drawLine(ax[1], "West_Scandinavian", "Icelandic_2.8")
drawLine(ax[1], "West_Scandinavian", "Faroese_2.8")

plot_x = torch.FloatTensor(plot_x)
plot_y = torch.FloatTensor(plot_y)
correlation = float(((plot_x - plot_x.mean()) * (plot_y - plot_y.mean())).mean() / (sd(plot_x, 0) * sd(plot_y, 0)))
print("CORRELATION", correlation)
ax[1].text(0, 1.1, f"R={round(correlation,3)}")



ax[2].set_xlim(-0, 1)
ax[2].set_ylim(-0,1)
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
plot_x = []
plot_y = []
for i in range(len(languages)):
 if languages[i] in ["French_2.8", "Old_French_2.8", "Italian_2.8", "Spanish_2.8", "Portuguese_2.8", "Latin_2.8", "Romanian_2.8", "Galician_2.8", "Catalan_2.8"]:
   print(languages[i])
   plot_x.append(real_congruence[languages[i]])
   plot_y.append(summary_data[(languages[i], "AverageFAngleSpace")])
   ax[2].text(plot_x[-1], plot_y[-1], languages[i][:3].lower(), c=COLORS[categorical[languages[i]]])
for hidden in ["Italo_Western_Romance", "Western_Romance", "Iberian_Romance", "West_Iberian"]:
    print(ancestral[hidden])
    print(categorical)
    plot_x.append(ancestral[hidden][0])
    plot_y.append(ancestral[hidden][1])
    ax[2].text(plot_x[-1], plot_y[-1], hidden[:3].lower(), c=COLORS[categorical[hidden]], alpha=0.2)
drawLine(ax[2], "Latin_2.8", "Italo_Western_Romance")
drawLine(ax[2], "Italo_Western_Romance", "Italian_2.8")
drawLine(ax[2], "Italo_Western_Romance", "Western_Romance")
drawLine(ax[2], "Western_Romance", "Old_French_2.8")
drawLine(ax[2], "Old_French_2.8", "French_2.8")
drawLine(ax[2], "Western_Romance", "Iberian_Romance")
drawLine(ax[2], "Iberian_Romance", "Catalan_2.8")
drawLine(ax[2], "Iberian_Romance", "West_Iberian")
drawLine(ax[2], "West_Iberian", "Spanish_2.8")
drawLine(ax[2], "West_Iberian", "Galician_2.8")
drawLine(ax[2], "West_Iberian", "Portuguese_2.8")
plot_x = torch.FloatTensor(plot_x)
plot_y = torch.FloatTensor(plot_y)
correlation = float(((plot_x - plot_x.mean()) * (plot_y - plot_y.mean())).mean() / (sd(plot_x, 0) * sd(plot_y, 0)))
print("CORRELATION", correlation)
ax[2].text(0, 1.1, f"R={round(correlation,3)}")



ax[3].set_xlim(-0, 1)
ax[3].set_ylim(-0,1)
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
plot_x = []
plot_y = []
for i in range(len(languages)):
  if languages[i] in ["Latvian_2.8", "Lithuanian_2.8", "Old_Church_Slavonic_2.8", "Bulgarian_2.8", "Russian_2.8", "Belarusian_2.8", "Ukrainian_2.8", "Serbian_2.8", "Croatian_2.8", "Polish_2.8", "Czech_2.8", "Slovak_2.8", "Slovenian_2.8", "Old_East_Slavic_2.8", "Upper_Sorbian_2.8"]:
   print(languages[i])
   plot_x.append(real_congruence[languages[i]])
   plot_y.append(summary_data[(languages[i], "AverageFAngleSpace")])
   ax[3].text(plot_x[-1], plot_y[-1], languages[i][:3].lower(), c=COLORS[categorical[languages[i]]])
for hidden in ["Balto_Slavic", "Eastern_Baltic", "Slavic"]:
    print(ancestral[hidden])
    print(categorical)
    plot_x.append(ancestral[hidden][0])
    plot_y.append(ancestral[hidden][1])
    ax[3].text(plot_x[-1], plot_y[-1], hidden[:3].lower(), c=COLORS[categorical[hidden]], alpha=0.2)
drawLine(ax[3], "Balto_Slavic", "Eastern_Baltic")
drawLine(ax[3], "Balto_Slavic", "Slavic")
drawLine(ax[3], "Slavic", "Old_Church_Slavonic_2.8")
drawLine(ax[3], "Slavic", "Old_East_Slavic_2.8")
plot_x = torch.FloatTensor(plot_x)
plot_y = torch.FloatTensor(plot_y)
correlation = float(((plot_x - plot_x.mean()) * (plot_y - plot_y.mean())).mean() / (sd(plot_x, 0) * sd(plot_y, 0)))
print("CORRELATION", correlation)
ax[3].text(0, 1.1, f"R={round(correlation,3)}")






plt.show()


quit()




def getShape(x, names):
    names = [[int(q)-1 for q in y[y.index("[")+1:-1].split(",")] for y in names]
    N = len(names[0])
    size = [max([q[i] for q in names])+1 for i in range(N)]
    result = torch.zeros(size)
    for i in range(len(names)):
        result[tuple(names[i])] = x[i]
    return result

orders = ['No dominant order', 'SOV', 'SVO', 'VOS', 'VSO']


with open("../frontier_categorical/fits/76model.py_process9_frontier18.txt", "r") as inFile:
    fit = [[q for q in x.split(" ") if len(q) > 0] for x in inFile.read().strip().split("\n")]
alpha_fit = [x for x in fit if len(x) > 0 and x[0].startswith("alpha")]
print(alpha_fit)
alpha = torch.FloatTensor([float(x[1]) for x in alpha_fit])
print(alpha)
alpha_names = [x[0] for x in alpha_fit]
print(alpha_names)

alpha_frontier_shape = getShape(alpha, alpha_names)
alpha_frontier_shape_x = alpha_frontier_shape[:,:4]
alpha_frontier_shape_y = alpha_frontier_shape[:,4:]

with open("../frontier_categorical/fits/71model.py_process9_frontier14.txt", "r") as inFile:
    fit = [[q for q in x.split(" ") if len(q) > 0] for x in inFile.read().strip().split("\n")]
alpha_fit = [x for x in fit if len(x) > 0 and x[0].startswith("alpha")]
print(alpha_fit)
alpha = torch.FloatTensor([float(x[1]) for x in alpha_fit])
print(alpha)
alpha_names = [x[0] for x in alpha_fit]
print(alpha_names)

alpha = getShape(alpha, alpha_names)


Omega_fit = [x for x in fit if len(x) > 0 and x[0].startswith("Omega")]
print(Omega_fit)
Omega = torch.FloatTensor([float(x[1]) for x in Omega_fit])
print(Omega)
Omega_names = [x[0] for x in Omega_fit]
print(Omega_names)

Omega = getShape(Omega, Omega_names)

grid = torch.FloatTensor([[((x)/20-0.5,(y)/20) for y in range(60)] for x in range(60)]).view(-1, 2)
print(grid)

print(alpha[1])

print(Omega[1])
print(alpha[1])

import math

fig, ax = plt.subplots(nrows=1, ncols=4, figsize=(12, 6))


density = (torch.exp(-(torch.matmul((grid - alpha[1,:2].unsqueeze(0)), Omega[1,:2,:2].inverse()) * (grid - alpha[1,:2].unsqueeze(0))).sum(dim=1)))
print(density)
ax[0].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60))
ax[0].set_xlim(-2, 0)
ax[0].set_ylim(-2,0)
ax[0].plot(-alpha_frontier_shape_x[1], -alpha_frontier_shape_y[1])



density = (torch.exp(-(torch.matmul((grid - alpha[2,:2].unsqueeze(0)), Omega[2,:2,:2].inverse()) * (grid - alpha[2,:2].unsqueeze(0))).sum(dim=1)))
ax[1].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60))
ax[1].set_xlim(-2, 0)
ax[1].set_ylim(-2,0)
ax[1].plot(-alpha_frontier_shape_x[2], -alpha_frontier_shape_y[2])


density = (torch.exp(-(torch.matmul((grid - alpha[4,:2].unsqueeze(0)), Omega[4,:2,:2].inverse()) * (grid - alpha[4,:2].unsqueeze(0))).sum(dim=1)))
ax[2].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60), colors='b')
ax[2].set_xlim(-2, 0)
ax[2].set_ylim(-2,0)
ax[2].plot(-alpha_frontier_shape_x[4], -alpha_frontier_shape_y[4])

density = (torch.exp(-(torch.matmul((grid - alpha[0,:2].unsqueeze(0)), Omega[0,:2,:2].inverse()) * (grid - alpha[0,:2].unsqueeze(0))).sum(dim=1)))
ax[3].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60), colors='r')
ax[3].set_xlim(-2, 0)
ax[3].set_ylim(-2,0)
ax[3].plot(-alpha_frontier_shape_x[0], -alpha_frontier_shape_y[0])




#ax[1].contour(grid_transformed_x.view(20, 7*20), grid_transformed_y.view(20, 7*20), density_transformed.view(20, 7*20))
#ax[1].scatter(circle[:,0], circle[:,1], c=color_circle.numpy().tolist(), cmap='viridis', vmin=0, vmax=1)
#ax[1].set_xlim(-1.5, 1)
#ax[1].set_ylim(-1.5,1)

#print(color_circle)


plt.show()



