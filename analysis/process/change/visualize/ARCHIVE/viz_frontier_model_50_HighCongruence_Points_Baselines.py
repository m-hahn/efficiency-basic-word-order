import matplotlib.pyplot as plt
import torch

with open("../../../ud_languages_2.8.txt", "r") as inFile:
    languages = inFile.read().strip().split("\n")


summary_data = {}
with open("../../output/process9_frontier15.py.tsv", "r") as inFile:
    for line in inFile:
        line = line.strip().split(" ")
        summary_data[(line[0], line[1])] = float(line[2])
print(summary_data)

x_coordinates = {}
y_coordinates = {}
for language in languages:
 with open(f"../../output_frontier/process9_frontier15.py_{language}_FrontierDiscretizedByAngle.tsv", "r") as inFile:
    frontier = [float(x.split(" ")[2]) for x in inFile.read().strip().split("\n")]
    x = frontier[::2]
    y = frontier[1::2]
    x_coordinates[language] = x
    y_coordinates[language] = y



print(x_coordinates)

x_coordinates = torch.FloatTensor([x_coordinates[x] for x in languages])
y_coordinates = torch.FloatTensor([y_coordinates[y] for y in languages])
print(x_coordinates.mean(dim=0))
print(y_coordinates.mean(dim=0))


x_positions_langs = torch.FloatTensor([summary_data[(x,"Distance_Surprisal_ComparedFrontier")] for x in languages])
y_positions_langs = torch.FloatTensor([summary_data[(y,"Distance_DepLen_ComparedFrontier")] for y in languages])


def sd(tensor, dim):
    return (tensor.pow(2).mean(dim=dim) - tensor.mean(dim=dim).pow(2)).sqrt()

families = {}
for q in ["../../../../languages/families_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        families[line[0]] = line[1]


categorical = {}
for q in ["../../../../languages/categorical_order/categorical_order_2.8.tsv", "../../../../languages/categorical_order/categoricalOrderAdditional_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        categorical[line[0]] = line[1]

grid = torch.FloatTensor([[((x)/20-1.5,(y)/20-1.5) for y in range(60)] for x in range(60)]).view(-1, 2)

x_variance_langs = torch.FloatTensor([summary_data[(x,"Baseline_Variance_X_ComparedToFrontier")] for x in languages])
y_variance_langs = torch.FloatTensor([summary_data[(x,"Baseline_Variance_Y_ComparedToFrontier")] for x in languages])
xy_covariance_langs = torch.FloatTensor([summary_data[(x,"Baseline_Covariance_XY_ComparedToFrontier")] for x in languages])

baselines_covariance = torch.stack([x_variance_langs, xy_covariance_langs, xy_covariance_langs, y_variance_langs], dim=1).view(78, 2,2)

print(grid.size(), x_variance_langs.size())
print(baselines_covariance.size())
baseline_density = []
for i in range(len(languages)):
  baseline_density.append(torch.exp(-(grid.t() * torch.matmul(baselines_covariance[i].inverse(), grid.t())).sum(dim=0)))

baseline_density = torch.stack(baseline_density, dim=0).mean(dim=0)


import math

fig, ax = plt.subplots(nrows=1, ncols=6, figsize=(12, 6))


ax[0].set_xlim(-1.3, 0.2)
ax[0].set_ylim(-1.2,0.2)
ax[0].plot(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c="black")
ax[0].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
ax[0].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in range(len(languages)):
   ax[0].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])
ax[0].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")



ax[1].set_xlim(-1.3, 0.2)
ax[1].set_ylim(-1.2,0.2)
ax[1].plot(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c="black")
ax[1].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
ax[1].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in range(len(languages)):
  if families[languages[i]] == "Latin_Romance":
   ax[1].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])



ax[2].set_xlim(-1.3, 0.2)
ax[2].set_ylim(-1.2,0.2)
ax[2].plot(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c="black")
ax[2].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
ax[2].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in range(len(languages)):
  if families[languages[i]] == "Slavic":
   ax[2].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])



ax[3].set_xlim(-1.3, 0.2)
ax[3].set_ylim(-1.2,0.2)
ax[3].plot(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c="black")
ax[3].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
ax[3].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in range(len(languages)):
  if families[languages[i]] == "Turkic":
   ax[3].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])


ax[4].set_xlim(-1.3, 0.2)
ax[4].set_ylim(-1.2,0.2)
ax[4].plot(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c="black")
ax[4].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
ax[4].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in range(len(languages)):
  if families[languages[i]] == "Germanic":
   ax[4].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])



ax[5].set_xlim(-1.3, 0.2)
ax[5].set_ylim(-1.2,0.2)
ax[5].plot(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c="black")
ax[5].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
ax[5].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
for i in range(len(languages)):
  if families[languages[i]] == "Semitic":
   ax[5].text(-x_positions_langs[i], -y_positions_langs[i], languages[i][:3].lower(), c={"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[languages[i]]])







plt.show()


quit()




def getShape(x, names):
    names = [[int(q)-1 for q in y[y.index("[")+1:-1].split(",")] for y in names]
    N = len(names[0])
    size = [max([q[i] for q in names])+1 for i in range(N)]
    result = torch.zeros(size)
    for i in range(len(names)):
        result[tuple(names[i])] = x[i]
    return result

orders = ['No dominant order', 'SOV', 'SVO', 'VOS', 'VSO']


with open("../frontier_categorical/fits/76model.py_process9_frontier15.txt", "r") as inFile:
    fit = [[q for q in x.split(" ") if len(q) > 0] for x in inFile.read().strip().split("\n")]
alpha_fit = [x for x in fit if len(x) > 0 and x[0].startswith("alpha")]
print(alpha_fit)
alpha = torch.FloatTensor([float(x[1]) for x in alpha_fit])
print(alpha)
alpha_names = [x[0] for x in alpha_fit]
print(alpha_names)

alpha_frontier_shape = getShape(alpha, alpha_names)
alpha_frontier_shape_x = alpha_frontier_shape[:,:4]
alpha_frontier_shape_y = alpha_frontier_shape[:,4:]

with open("../frontier_categorical/fits/71model.py_process9_frontier14.txt", "r") as inFile:
    fit = [[q for q in x.split(" ") if len(q) > 0] for x in inFile.read().strip().split("\n")]
alpha_fit = [x for x in fit if len(x) > 0 and x[0].startswith("alpha")]
print(alpha_fit)
alpha = torch.FloatTensor([float(x[1]) for x in alpha_fit])
print(alpha)
alpha_names = [x[0] for x in alpha_fit]
print(alpha_names)

alpha = getShape(alpha, alpha_names)


Omega_fit = [x for x in fit if len(x) > 0 and x[0].startswith("Omega")]
print(Omega_fit)
Omega = torch.FloatTensor([float(x[1]) for x in Omega_fit])
print(Omega)
Omega_names = [x[0] for x in Omega_fit]
print(Omega_names)

Omega = getShape(Omega, Omega_names)

grid = torch.FloatTensor([[((x)/20-0.5,(y)/20) for y in range(60)] for x in range(60)]).view(-1, 2)
print(grid)

print(alpha[1])

print(Omega[1])
print(alpha[1])

import math

fig, ax = plt.subplots(nrows=1, ncols=4, figsize=(12, 6))


density = (torch.exp(-(torch.matmul((grid - alpha[1,:2].unsqueeze(0)), Omega[1,:2,:2].inverse()) * (grid - alpha[1,:2].unsqueeze(0))).sum(dim=1)))
print(density)
ax[0].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60))
ax[0].set_xlim(-2, 0)
ax[0].set_ylim(-2,0)
ax[0].plot(-alpha_frontier_shape_x[1], -alpha_frontier_shape_y[1])



density = (torch.exp(-(torch.matmul((grid - alpha[2,:2].unsqueeze(0)), Omega[2,:2,:2].inverse()) * (grid - alpha[2,:2].unsqueeze(0))).sum(dim=1)))
ax[1].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60))
ax[1].set_xlim(-2, 0)
ax[1].set_ylim(-2,0)
ax[1].plot(-alpha_frontier_shape_x[2], -alpha_frontier_shape_y[2])


density = (torch.exp(-(torch.matmul((grid - alpha[4,:2].unsqueeze(0)), Omega[4,:2,:2].inverse()) * (grid - alpha[4,:2].unsqueeze(0))).sum(dim=1)))
ax[2].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60), colors='b')
ax[2].set_xlim(-2, 0)
ax[2].set_ylim(-2,0)
ax[2].plot(-alpha_frontier_shape_x[4], -alpha_frontier_shape_y[4])

density = (torch.exp(-(torch.matmul((grid - alpha[0,:2].unsqueeze(0)), Omega[0,:2,:2].inverse()) * (grid - alpha[0,:2].unsqueeze(0))).sum(dim=1)))
ax[3].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60), colors='r')
ax[3].set_xlim(-2, 0)
ax[3].set_ylim(-2,0)
ax[3].plot(-alpha_frontier_shape_x[0], -alpha_frontier_shape_y[0])




#ax[1].contour(grid_transformed_x.view(20, 7*20), grid_transformed_y.view(20, 7*20), density_transformed.view(20, 7*20))
#ax[1].scatter(circle[:,0], circle[:,1], c=color_circle.numpy().tolist(), cmap='viridis', vmin=0, vmax=1)
#ax[1].set_xlim(-1.5, 1)
#ax[1].set_ylim(-1.5,1)

#print(color_circle)


plt.show()



