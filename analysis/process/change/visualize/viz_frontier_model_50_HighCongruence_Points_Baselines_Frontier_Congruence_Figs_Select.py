import matplotlib.pyplot as plt
import torch

with open("../../../ud_languages_2.8.txt", "r") as inFile:
    languages = inFile.read().strip().split("\n")

N_LANG = len(languages)

with open("../../../../languages/languages-iso639-3.tsv", "r") as inFile:
    isoCodes = dict([x.strip().split("\t") for x in inFile.read().strip().split("\n")])

real_congruence = {}
with open("../../../../languages/extract_congruence/output/evaluateCongruence_POS_NoSplit.py.tsv", "r") as inFile:
    data = [x.split("\t") for x in inFile.read().strip().replace('"', "").split("\n")]
    header = data[0]
    for line in data[1:]:
        real_congruence[line[header.index("Language")]] = float(line[header.index("Congruence_All")])
print(real_congruence)
        
assert "Tamil_2.8" in real_congruence

summary_data = {}
with open("../../output/process9_frontier18.py.tsv", "r") as inFile:
    for line in inFile:
        line = line.strip().split(" ")
        summary_data[(line[0], line[1])] = float(line[2])
print(summary_data)

x_coordinates = {}
y_coordinates = {}
for language in languages:
 with open(f"../../output_frontier/process9_frontier18.py_{language}_FrontierDiscretizedByAngle.tsv", "r") as inFile:
    frontier = [float(x.split(" ")[2]) for x in inFile.read().strip().split("\n")]
    x = frontier[::2]
    y = frontier[1::2]
    x_coordinates[language] = x
    y_coordinates[language] = y

fOnFrontier = {}
fInterior = {}
interiorCoordinates = None
for language in languages:
  with open(f"../../output_frontier/process9_frontier18.py_{language}_fOnFrontierDiscretizedByAngle.tsv", "r") as inFile:
     fOnFrontier[language] = [float(x.split(" ")[1]) for x in inFile.read().strip().split("\n")]
  with open(f"../../output_frontier/process9_frontier18.py_{language}_fArea.tsv", "r") as inFile:
     inFileRead = inFile.read().strip().split("\n")
     fInterior[language] = [float(x.split(" ")[2]) for x in inFileRead]
     interiorCoordinates = torch.FloatTensor([(float(x.split(" ")[0]), float(x.split(" ")[1])) for x in inFileRead])
fOnFrontier = torch.FloatTensor([fOnFrontier[x] for x in languages])

fInterior = torch.FloatTensor([fInterior[x] for x in languages])



print(x_coordinates)

x_coordinates = torch.FloatTensor([x_coordinates[x] for x in languages])
y_coordinates = torch.FloatTensor([y_coordinates[y] for y in languages])
print(x_coordinates.mean(dim=0))
print(y_coordinates.mean(dim=0))


x_positions_langs = torch.FloatTensor([summary_data[(x,"Distance_Surprisal_ComparedFrontier")] for x in languages])
y_positions_langs = torch.FloatTensor([summary_data[(y,"Distance_DepLen_ComparedFrontier")] for y in languages])


def sd(tensor, dim):
    return (tensor.pow(2).mean(dim=dim) - tensor.mean(dim=dim).pow(2)).sqrt()

families = {}
for q in ["../../../../languages/families_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        families[line[0]] = line[1]


categorical = {}
for q in ["../../../../languages/categorical_order/categorical_order_2.8.tsv", "../../../../languages/categorical_order/categoricalOrderAdditional_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        categorical[line[0]] = line[1]

grid = torch.FloatTensor([[((x)/20-1.5,(y)/20-1.5) for y in range(60)] for x in range(60)]).view(-1, 2)

x_variance_langs = torch.FloatTensor([summary_data[(x,"Baseline_Variance_X_ComparedToFrontier")] for x in languages])
y_variance_langs = torch.FloatTensor([summary_data[(x,"Baseline_Variance_Y_ComparedToFrontier")] for x in languages])
xy_covariance_langs = torch.FloatTensor([summary_data[(x,"Baseline_Covariance_XY_ComparedToFrontier")] for x in languages])

baselines_covariance = torch.stack([x_variance_langs, xy_covariance_langs, xy_covariance_langs, y_variance_langs], dim=1).view(N_LANG, 2,2)

print(grid.size(), x_variance_langs.size())
print(baselines_covariance.size())
baseline_density = []
for i in range(len(languages)):
  baseline_density.append(torch.exp(-(grid.t() * torch.matmul(baselines_covariance[i].inverse(), grid.t())).sum(dim=0)))

baseline_densities = torch.stack(baseline_density, dim=0)
baseline_density = torch.stack(baseline_density, dim=0).mean(dim=0)


import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

viridis = cm.get_cmap('viridis', 8)

import math



print(fOnFrontier.size())
print(x_coordinates.size())

#ax[0].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

assert "Tamil_2.8" in languages

#ax[0].plot(-x_coordinates.mean(dim=0) - sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) - sd(y_coordinates, dim=0), c="black")
#ax[0].plot(-x_coordinates.mean(dim=0) + sd(x_coordinates, dim=0), -y_coordinates.mean(dim=0) + sd(y_coordinates, dim=0), c="black")
#ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])


from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6, 6))
#plt.tick_params(left=False,
#                bottom=False,
#                labelleft=False,
#                labelbottom=False)

ax.set_xlim(-1.3, 0.2)
ax.set_ylim(-1.2,0.2)

for i in range(9):
    ax.plot(-x_coordinates.mean(dim=0)[i:i+2], -y_coordinates.mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier.mean(dim=0)[i])), linewidth=4)


for i in range(len(languages)):
   if languages[i] in ["Japanese_2.8", "English_2.8", "Turkish_2.8", "Russian_2.8", "Persian_2.8", "Indonesian_2.8", "German_2.8", "Basque_2.8", "Finnish_2.8", "Chinese_2.8", "Wolof_2.8", "Armenian_2.8", "Buryat_2.8"]:
#      ax.text(-x_positions_langs[i], -y_positions_langs[i], isoCodes[languages[i].replace("_2.9", "").replace("_2.8", "")], c=viridis(real_congruence[languages[i]]))
      ax.text(-x_positions_langs[i], -y_positions_langs[i], languages[i].replace("_2.9", "").replace("_2.8", ""), c=viridis(real_congruence[languages[i]])) #, bbox=dict(fc="white", ec="white", lw=0))
   else:
      ax.scatter(-x_positions_langs[i], -y_positions_langs[i], c=viridis(real_congruence[languages[i]]), alpha=0.2)

ax.contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey")

#i = languages.index("Japanese_2.8")
#ax.text(-1.2, -0.05, "Japanese")
#ax.arrow(-1.0, -0.05, -x_positions_langs[i]+1.1, -y_positions_langs[i]+0.12, length_includes_head=True, head_width=0.03)
#i = languages.index("English_2.8")
#ax.text(-0.52, -1.1, "English")
#ax.arrow(-0.53, -1.1, -x_positions_langs[i]+0.58, -y_positions_langs[i]+1.1, length_includes_head=True, head_width=0.03)
#i = languages.index("Turkish_2.8")
#ax.text(-0.7, -0, "Turkish")
#ax.arrow(-0.7, 0, -x_positions_langs[i]+0.75, -y_positions_langs[i]+0.05, length_includes_head=True, head_width=0.03)

fig.set_size_inches(4,4)
ax.set_xlabel("(High)        ←  Information Locality        (Low)")
ax.set_ylabel("(High)        ←  Dependency Locality        (Low)")
plt.show()
fig.savefig(f"figures/{__file__.replace('.','_')}_overall.pdf")


