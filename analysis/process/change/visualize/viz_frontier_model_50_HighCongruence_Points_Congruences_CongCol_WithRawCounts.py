import matplotlib.pyplot as plt
import torch

with open("../../../ud_languages_2.8.txt", "r") as inFile:
    languages = inFile.read().strip().split("\n")

real_congruence = {}
with open("../../../../languages/extract_congruence/output/evaluateCongruence_POS_NoSplit.py.tsv", "r") as inFile:
    data = [x.split("\t") for x in inFile.read().strip().replace('"', "").split("\n")]
    header = data[0]
    for line in data[1:]:
        real_congruence[line[header.index("Language")]] = float(line[header.index("Congruence_All")])
print(real_congruence)
       

congHigh = ["SOV", "VSO", "OSV"]
congLow = ["SVO"]


forIL = {language : [0,0] for language in languages}
with open("../../../../optimization/output/readGrammars.py.tsv", "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if True:
           if line[0] == "Japanese-GSD_2.8":
                line[0] = "Japanese_2.8"
           elif line[0] == "Czech-PDT_2.8":
               line[0] = "Czech_2.8"
           elif line[0] == "German-GSD_2.8":
               line[0] = "German_2.8"
           elif line[0] not in languages:
               continue
           forIL[line[0]][1 if line[2] in congHigh else 0] += 1.0
print(forIL)


forDL = {language : [0,0] for language in languages}
with open("../../../../optimization/output/readGrammars_AUC_OnlyDLM.py.tsv", "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if True:
           if line[0] == "Japanese-GSD_2.8":
                line[0] = "Japanese_2.8"
           elif line[0] == "Czech-PDT_2.8":
               line[0] = "Czech_2.8"
           elif line[0] == "German-GSD_2.8":
               line[0] = "German_2.8"
           elif line[0] not in languages:
               continue
#           print(line[2])
           forDL[line[0]][1 if line[2] in congHigh else 0] += 1.0
#quit()

with open("../../../../optimization/output/forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RecordBasic.py.tsv", "r") as inFile:
    forDL2 = [x.split("\t") for x in inFile.read().strip().split("\n")]
    for line in forDL2:
        if line[1] == "DLM_MEMORY_OPTIMIZED/locality_optimized_dlm/manual_output_funchead_fine_depl_nopos":
           if line[0] == "Japanese-GSD_2.8":
                line[0] = "Japanese_2.8"
           elif line[0] == "Czech-PDT_2.8":
               line[0] = "Czech_2.8"
           elif line[0] == "German-GSD_2.8":
               line[0] = "German_2.8"
           elif line[0] not in languages:
               continue
           assert line[3] in congHigh+congLow
           forDL[line[0]][1 if line[3] in congHigh else 0] += 1
print(forDL)
for language in languages:
    print(language)
    forIL[language] = forIL[language][1] / sum(forIL[language])
    forDL[language] = forDL[language][1] / sum(forDL[language])
print(forDL)
#quit()

summary_data = {}
with open("../../output/process9_frontier18.py.tsv", "r") as inFile:
    for line in inFile:
        line = line.strip().split(" ")
        summary_data[(line[0], line[1])] = float(line[2])
print(summary_data)

x_coordinates = {}
y_coordinates = {}
for language in languages:
 with open(f"../../output_frontier/process9_frontier18.py_{language}_FrontierDiscretizedByAngle.tsv", "r") as inFile:
    frontier = [float(x.split(" ")[2]) for x in inFile.read().strip().split("\n")]
    x = frontier[::2]
    y = frontier[1::2]
    x_coordinates[language] = x
    y_coordinates[language] = y

fOnFrontier = {}
for language in languages:
  with open(f"../../output_frontier/process9_frontier18.py_{language}_fOnFrontierDiscretizedByAngle.tsv", "r") as inFile:
     fOnFrontier[language] = [float(x.split(" ")[1]) for x in inFile.read().strip().split("\n")]

fOnFrontier = torch.FloatTensor([fOnFrontier[x] for x in languages])
      


print(x_coordinates)

x_coordinates = torch.FloatTensor([x_coordinates[x] for x in languages])
y_coordinates = torch.FloatTensor([y_coordinates[y] for y in languages])
print(x_coordinates.mean(dim=0))
print(y_coordinates.mean(dim=0))


x_positions_langs = torch.FloatTensor([summary_data[(x,"Distance_Surprisal_ComparedFrontier")] for x in languages])
y_positions_langs = torch.FloatTensor([summary_data[(y,"Distance_DepLen_ComparedFrontier")] for y in languages])


def sd(tensor, dim):
    return (tensor.pow(2).mean(dim=dim) - tensor.mean(dim=dim).pow(2)).sqrt()

families = {}
for q in ["../../../../languages/families_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        families[line[0]] = line[1]


categorical = {}
for q in ["../../../../languages/categorical_order/categorical_order_2.8.tsv", "../../../../languages/categorical_order/categoricalOrderAdditional_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        categorical[line[0]] = line[1]

COLORS = {"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}


import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

viridis = cm.get_cmap('viridis', 8)


def getSpearman(plot_x, plot_y):
  plot_x = sorted(list(enumerate(plot_x.numpy().tolist())), key=lambda x:x[1])
  plot_y = sorted(list(enumerate(plot_y.numpy().tolist())), key=lambda y:y[1])
  rank_x = torch.FloatTensor([0 for _ in plot_x])
  rank_y = torch.FloatTensor([0 for _ in plot_y])
  for i in range(len(plot_x)):
      rank_x[plot_x[i][0]] = i
      rank_y[plot_y[i][0]] = i
  return float(((rank_x - rank_x.mean()) * (rank_y - rank_y.mean())).mean() / (sd(rank_x, 0) * sd(rank_y, 0)))


from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

import math

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(12, 6))


ax.set_xlim(-0.2, 1.2)
ax.set_ylim(-0.2,1.2)
#ax.scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
plot_x = []
plot_y = []
ax.plot([0,1], [0,1])
for i in range(len(languages)):
   plot_x.append(real_congruence[languages[i]])
   plot_y.append((forIL[languages[i]] + forDL[languages[i]])/2)
#   ax.text(plot_x[i], plot_y[i], languages[i][:3].lower(), c=viridis(plot_x[i]))
   ax.scatter(plot_x[i], plot_y[i], c=viridis(plot_x[i]))
plot_x = torch.FloatTensor(plot_x)
plot_y = torch.FloatTensor(plot_y)
correlation = float(((plot_x - plot_x.mean()) * (plot_y - plot_y.mean())).mean() / (sd(plot_x, 0) * sd(plot_y, 0)))
correlation_spearman = getSpearman(plot_x, plot_y)
print("CORRELATION", correlation)
print("CORRELATION (Spearman)", correlation_spearman)
ax.text(0, 1.1, f"R={round(correlation,3)}")
ax.text(0.6, 1.1, f"rho={round(correlation_spearman,3)}")

ax.set_xlabel("Attested Subject-Object Position Congruence")
ax.set_ylabel("Optimized Subject-Object Position Congruence")


print("SAVING FIGURE")
fig.set_size_inches(3,4)
fig.savefig(f"figures/{__file__.replace('.','_')}_panel0.pdf")


plt.show()




fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(12, 6))


ax.set_xlim(-0.2, 1.2)
ax.set_ylim(-0.2,1.2)
#ax.scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
plot_x = []
plot_y = []
ax.plot([0,1], [0,1])
for i in range(len(languages)):
   plot_x.append(real_congruence[languages[i]])
   plot_y.append(forIL[languages[i]])
#   ax.text(plot_x[i], plot_y[i], languages[i][:3].lower(), c=viridis(plot_x[i]))
   ax.scatter(plot_x[i], plot_y[i], c=viridis(plot_x[i]))
plot_x = torch.FloatTensor(plot_x)
plot_y = torch.FloatTensor(plot_y)

correlation = float(((plot_x - plot_x.mean()) * (plot_y - plot_y.mean())).mean() / (sd(plot_x, 0) * sd(plot_y, 0)))
correlation_spearman = getSpearman(plot_x, plot_y)
print("CORRELATION", correlation)
print("CORRELATION (Spearman)", correlation_spearman)
ax.text(0, 1.1, f"R={round(correlation,3)}")
ax.text(0.6, 1.1, f"rho={round(correlation_spearman,3)}")

ax.set_xlabel("Attested Subject-Object Position Congruence")
ax.set_ylabel("Optimized Subject-Object Position Congruence")


print("SAVING FIGURE")
fig.set_size_inches(3,4)
fig.savefig(f"figures/{__file__.replace('.','_')}_panel1.pdf")


plt.show()



fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(12, 6))


ax.set_xlim(-0.2, 1.2)
ax.set_ylim(-0.2,1.2)
#ax.scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
plot_x = []
plot_y = []
ax.plot([0,1], [0,1])
for i in range(len(languages)):
   plot_x.append(real_congruence[languages[i]])
   plot_y.append(forDL[languages[i]])
#   ax.text(plot_x[i], plot_y[i], languages[i][:3].lower(), c=viridis(plot_x[i]))
   ax.scatter(plot_x[i], plot_y[i], c=viridis(plot_x[i]))
plot_x = torch.FloatTensor(plot_x)
plot_y = torch.FloatTensor(plot_y)

correlation = float(((plot_x - plot_x.mean()) * (plot_y - plot_y.mean())).mean() / (sd(plot_x, 0) * sd(plot_y, 0)))
correlation_spearman = getSpearman(plot_x, plot_y)
print("CORRELATION", correlation)
print("CORRELATION (Spearman)", correlation_spearman)
ax.text(0, 1.1, f"R={round(correlation,3)}")
ax.text(0.6, 1.1, f"rho={round(correlation_spearman,3)}")

ax.set_xlabel("Attested Subject-Object Position Congruence")
ax.set_ylabel("Optimized Subject-Object Position Congruence")


#plt.xlabel("Attested Subject-Object Position Congruence")
#plt.ylabel("Optimized Subject-Object Position Congruence")
print("SAVING FIGURE")
fig.set_size_inches(3,4)
fig.savefig(f"figures/{__file__.replace('.','_')}_panel2.pdf")


plt.show()


#quit()
#
#
#
#
#def getShape(x, names):
#    names = [[int(q)-1 for q in y[y.index("[")+1:-1].split(",")] for y in names]
#    N = len(names[0])
#    size = [max([q[i] for q in names])+1 for i in range(N)]
#    result = torch.zeros(size)
#    for i in range(len(names)):
#        result[tuple(names[i])] = x[i]
#    return result
#
#orders = ['No dominant order', 'SOV', 'SVO', 'VOS', 'VSO']
#
#
#with open("../frontier_categorical/fits/76model.py_process9_frontier18.txt", "r") as inFile:
#    fit = [[q for q in x.split(" ") if len(q) > 0] for x in inFile.read().strip().split("\n")]
#alpha_fit = [x for x in fit if len(x) > 0 and x[0].startswith("alpha")]
#print(alpha_fit)
#alpha = torch.FloatTensor([float(x[1]) for x in alpha_fit])
#print(alpha)
#alpha_names = [x[0] for x in alpha_fit]
#print(alpha_names)
#
#alpha_frontier_shape = getShape(alpha, alpha_names)
#alpha_frontier_shape_x = alpha_frontier_shape[:,:4]
#alpha_frontier_shape_y = alpha_frontier_shape[:,4:]
#
#with open("../frontier_categorical/fits/71model.py_process9_frontier14.txt", "r") as inFile:
#    fit = [[q for q in x.split(" ") if len(q) > 0] for x in inFile.read().strip().split("\n")]
#alpha_fit = [x for x in fit if len(x) > 0 and x[0].startswith("alpha")]
#print(alpha_fit)
#alpha = torch.FloatTensor([float(x[1]) for x in alpha_fit])
#print(alpha)
#alpha_names = [x[0] for x in alpha_fit]
#print(alpha_names)
#
#alpha = getShape(alpha, alpha_names)
#
#
#Omega_fit = [x for x in fit if len(x) > 0 and x[0].startswith("Omega")]
#print(Omega_fit)
#Omega = torch.FloatTensor([float(x[1]) for x in Omega_fit])
#print(Omega)
#Omega_names = [x[0] for x in Omega_fit]
#print(Omega_names)
#
#Omega = getShape(Omega, Omega_names)
#
#grid = torch.FloatTensor([[((x)/20-0.5,(y)/20) for y in range(60)] for x in range(60)]).view(-1, 2)
#print(grid)
#
#print(alpha[1])
#
#print(Omega[1])
#print(alpha[1])
#
#import math
#
#fig, ax = plt.subplots(nrows=1, ncols=4, figsize=(12, 6))
#
#
#density = (torch.exp(-(torch.matmul((grid - alpha[1,:2].unsqueeze(0)), Omega[1,:2,:2].inverse()) * (grid - alpha[1,:2].unsqueeze(0))).sum(dim=1)))
#print(density)
#ax.contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60))
#ax.set_xlim(-2, 0)
#ax.set_ylim(-2,0)
#ax.plot(-alpha_frontier_shape_x[1], -alpha_frontier_shape_y[1])
#
#
#
#density = (torch.exp(-(torch.matmul((grid - alpha[2,:2].unsqueeze(0)), Omega[2,:2,:2].inverse()) * (grid - alpha[2,:2].unsqueeze(0))).sum(dim=1)))
#ax.contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60))
#ax.set_xlim(-2, 0)
#ax.set_ylim(-2,0)
#ax.plot(-alpha_frontier_shape_x[2], -alpha_frontier_shape_y[2])
#
#
#density = (torch.exp(-(torch.matmul((grid - alpha[4,:2].unsqueeze(0)), Omega[4,:2,:2].inverse()) * (grid - alpha[4,:2].unsqueeze(0))).sum(dim=1)))
#ax.contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60), colors='b')
#ax.set_xlim(-2, 0)
#ax.set_ylim(-2,0)
#ax.plot(-alpha_frontier_shape_x[4], -alpha_frontier_shape_y[4])
#
#density = (torch.exp(-(torch.matmul((grid - alpha[0,:2].unsqueeze(0)), Omega[0,:2,:2].inverse()) * (grid - alpha[0,:2].unsqueeze(0))).sum(dim=1)))
#ax[3].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60), colors='r')
#ax[3].set_xlim(-2, 0)
#ax[3].set_ylim(-2,0)
#ax[3].plot(-alpha_frontier_shape_x[0], -alpha_frontier_shape_y[0])
#
#
#
#
##ax.contour(grid_transformed_x.view(20, 7*20), grid_transformed_y.view(20, 7*20), density_transformed.view(20, 7*20))
##ax.scatter(circle[:,0], circle[:,1], c=color_circle.numpy().tolist(), cmap='viridis', vmin=0, vmax=1)
##ax.set_xlim(-1.5, 1)
##ax.set_ylim(-1.5,1)
#
##print(color_circle)
#
#
#
#plt.show()
#
