import matplotlib.pyplot as plt
import torch


with open("../../../../languages/case_marking/case_marking_revised_2.8.tsv", "r") as inFile:
    caseMarking = [x.split(",") for x in inFile.read().strip().replace('"', '').split("\n")][1:]
    caseMarking = {x[2] : x[3] for x in caseMarking}

with open("../../../ud_languages_2.8.txt", "r") as inFile:
    languages = inFile.read().strip().split("\n")

real_congruence = {}
with open("../../../../languages/extract_congruence/output/evaluateCongruence_POS_NoSplit.py.tsv", "r") as inFile:
    data = [x.split("\t") for x in inFile.read().strip().replace('"', "").split("\n")]
    header = data[0]
    for line in data[1:]:
        real_congruence[line[header.index("Language")]] = float(line[header.index("Congruence_All")])
print(real_congruence)
        


summary_data = {}
with open("../../output/process9_frontier18.py.tsv", "r") as inFile:
    for line in inFile:
        line = line.strip().split(" ")
        summary_data[(line[0], line[1])] = float(line[2])
print(summary_data)

x_coordinates = {}
y_coordinates = {}
for language in languages:
 with open(f"../../output_frontier/process9_frontier18.py_{language}_FrontierDiscretizedByAngle.tsv", "r") as inFile:
    frontier = [float(x.split(" ")[2]) for x in inFile.read().strip().split("\n")]
    x = frontier[::2]
    y = frontier[1::2]
    x_coordinates[language] = x
    y_coordinates[language] = y

fOnFrontier = {}
for language in languages:
  with open(f"../../output_frontier/process9_frontier18.py_{language}_fOnFrontierDiscretizedByAngle.tsv", "r") as inFile:
     fOnFrontier[language] = [float(x.split(" ")[1]) for x in inFile.read().strip().split("\n")]

fOnFrontier = torch.FloatTensor([fOnFrontier[x] for x in languages])
      


print(x_coordinates)

x_coordinates = torch.FloatTensor([x_coordinates[x] for x in languages])
y_coordinates = torch.FloatTensor([y_coordinates[y] for y in languages])
print(x_coordinates.mean(dim=0))
print(y_coordinates.mean(dim=0))


x_positions_langs = torch.FloatTensor([summary_data[(x,"Distance_Surprisal_ComparedFrontier")] for x in languages])
y_positions_langs = torch.FloatTensor([summary_data[(y,"Distance_DepLen_ComparedFrontier")] for y in languages])


def sd(tensor, dim):
    return (tensor.pow(2).mean(dim=dim) - tensor.mean(dim=dim).pow(2)).sqrt()

families = {}
for q in ["../../../../languages/families_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        families[line[0]] = line[1]


categorical = {}
for q in ["../../../../languages/categorical_order/categorical_order_2.8.tsv", "../../../../languages/categorical_order/categoricalOrderAdditional_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        categorical[line[0]] = line[1]

COLORS = {"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}


import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

viridis = cm.get_cmap('viridis', 8)


def getSpearman(plot_x, plot_y):
  plot_x = sorted(list(enumerate(plot_x.numpy().tolist())), key=lambda x:x[1])
  plot_y = sorted(list(enumerate(plot_y.numpy().tolist())), key=lambda y:y[1])
  rank_x = torch.FloatTensor([0 for _ in plot_x])
  rank_y = torch.FloatTensor([0 for _ in plot_y])
  for i in range(len(plot_x)):
      rank_x[plot_x[i][0]] = i
      rank_y[plot_y[i][0]] = i
  return float(((rank_x - rank_x.mean()) * (rank_y - rank_y.mean())).mean() / (sd(rank_x, 0) * sd(rank_y, 0)))

import math
#
#fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(12, 6))
#
#
#ax[0].set_xlim(-0.2, 1.2)
#ax[0].set_ylim(-0.2,1.2)
##ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
#plot_x = []
#plot_y = []
#ax[0].plot([0,1], [0,1])
#for i in range(len(languages)):
#   plot_x.append(real_congruence[languages[i]])
#   plot_y.append(summary_data[(languages[i], "AverageFAngleSpace")])
##   ax[0].text(plot_x[i], plot_y[i], languages[i][:3].lower(), c=viridis(plot_x[i]))
#   ax[0].scatter(plot_x[i], plot_y[i], color=("green" if caseMarking[languages[i]] == "TRUE" else "red"))
#plot_x = torch.FloatTensor(plot_x)
#plot_y = torch.FloatTensor(plot_y)
#correlation = float(((plot_x - plot_x.mean()) * (plot_y - plot_y.mean())).mean() / (sd(plot_x, 0) * sd(plot_y, 0)))
#correlation_spearman = getSpearman(plot_x, plot_y)
#print("CORRELATION", correlation)
#print("CORRELATION (Spearman)", correlation_spearman)
#ax[0].text(0, 1.1, f"R={round(correlation,3)}")
#ax[0].text(0.6, 1.1, f"rho={round(correlation_spearman,3)}")
#
#
#ax[1].set_xlim(-0.2, 1.2)
#ax[1].set_ylim(-0.2,1.2)
##ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
#plot_x = []
#plot_y = []
#ax[1].plot([0,1], [0,1])
#for i in range(len(languages)):
#   plot_x.append(real_congruence[languages[i]])
#   plot_y.append(fOnFrontier[i,0])
##   ax[1].text(plot_x[i], plot_y[i], languages[i][:3].lower(), c=viridis(plot_x[i]))
#   ax[1].scatter(plot_x[i], plot_y[i], color=("green" if caseMarking[languages[i]] == "TRUE" else "red"))
#plot_x = torch.FloatTensor(plot_x)
#plot_y = torch.FloatTensor(plot_y)
#
#correlation = float(((plot_x - plot_x.mean()) * (plot_y - plot_y.mean())).mean() / (sd(plot_x, 0) * sd(plot_y, 0)))
#correlation_spearman = getSpearman(plot_x, plot_y)
#print("CORRELATION", correlation)
#print("CORRELATION (Spearman)", correlation_spearman)
#ax[1].text(0, 1.1, f"R={round(correlation,3)}")
#ax[1].text(0.6, 1.1, f"rho={round(correlation_spearman,3)}")
#
#
##plt.xlabel("Attested Subject-Object Position Congruence")
##plt.ylabel("Optimized Subject-Object Position Congruence")
#
#
#
#ax[2].set_xlim(-0.2, 1.2)
#ax[2].set_ylim(-0.2,1.2)
##ax[0].scatter(-x_positions_langs, -y_positions_langs, c=[{"SOV" : "red", "SVO" : "green", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[x]] for x in languages])
#plot_x = []
#plot_y = []
#ax[2].plot([0,1], [0,1])
#for i in range(len(languages)):
#   plot_x.append(real_congruence[languages[i]])
#   plot_y.append(fOnFrontier[i,-1])
##   ax[2].text(plot_x[i], plot_y[i], languages[i][:3].lower(), c=viridis(plot_x[i]))
#   ax[2].scatter(plot_x[i], plot_y[i], color=("green" if caseMarking[languages[i]] == "TRUE" else "red"))
#plot_x = torch.FloatTensor(plot_x)
#plot_y = torch.FloatTensor(plot_y)
#
#correlation = float(((plot_x - plot_x.mean()) * (plot_y - plot_y.mean())).mean() / (sd(plot_x, 0) * sd(plot_y, 0)))
#correlation_spearman = getSpearman(plot_x, plot_y)
#print("CORRELATION", correlation)
#print("CORRELATION (Spearman)", correlation_spearman)
#ax[2].text(0, 1.1, f"R={round(correlation,3)}")
#ax[2].text(0.6, 1.1, f"rho={round(correlation_spearman,3)}")
#
#
##plt.xlabel("Attested Subject-Object Position Congruence")
##plt.ylabel("Optimized Subject-Object Position Congruence")
#print("SAVING FIGURE")
#fig.set_size_inches(9,4)
#fig.savefig(f"figures/{__file__.replace('.','_')}.pdf")
#
#
#plt.show()
#
#
#quit()
#
#
#
#
def getShape(x, names):
    names = [[int(q)-1 for q in y[y.index("[")+1:-1].split(",")] for y in names]
    N = len(names[0])
    size = [max([q[i] for q in names])+1 for i in range(N)]
    result = torch.zeros(size)
    for i in range(len(names)):
        result[tuple(names[i])] = x[i]
    return result

def getByName(name):
   alpha_fit = [x for x in fit if len(x) > 0 and x[0].startswith(name+"[")]
   alpha = torch.FloatTensor([float(x[6]) for x in alpha_fit])
   alpha_names = [x[0] for x in alpha_fit]
   alpha_frontier_shape = getShape(alpha, alpha_names)
   return    alpha_frontier_shape


orders = ['No dominant order', 'SOV', 'SVO', 'VOS', 'VSO']


with open("../frontier_historical/fits/90model.py_process9_frontier18_Revised.txt", "r") as inFile:
    fit = [[q for q in x.split(" ") if len(q) > 0] for x in inFile.read().strip().split("\n")]
alpha_fit = [x for x in fit if len(x) > 0 and x[0].startswith("alpha")]
print(alpha_fit)
alpha = torch.FloatTensor([float(x[1]) for x in alpha_fit])
print(alpha)
alpha_names = [x[0] for x in alpha_fit]
print(alpha_names)

alpha_frontier_shape = getShape(alpha, alpha_names)

print(getByName("alpha"))

Omega = getByName("Omega")

import math

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6, 6))
grid = torch.FloatTensor([[((x)/20-0.5,(y)/20) for y in range(60)] for x in range(60)]).view(-1, 2)
N = 40
grid = torch.FloatTensor([[((x)/20-1,(y)/20-1) for y in range(40)] for x in range(40)]).view(-1, 2)
density = (torch.exp(-(torch.matmul((grid - alpha[2:].unsqueeze(0)), Omega[2:,2:].inverse()) * (grid - alpha[2:].unsqueeze(0))).sum(dim=1)))
ax.contour(-grid[:,0].view(N, N), -grid[:,1].view(N, N), density.view(N, N), colors="gray")
ax.set_xlim(-1, 1)
ax.set_ylim(-1,1)
#plt.show()

                                                                                                                                          
plt.savefig(f"figures/{__file__[:-3]}_coadaptation.pdf")
                                                                   


fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6, 6))
grid = torch.FloatTensor([[((x)/20-0.5,(y)/20) for y in range(60)] for x in range(60)]).view(-1, 2)
N = 40
grid = torch.FloatTensor([[((x)/20-1,(y)/20-1) for y in range(40)] for x in range(40)]).view(-1, 2)
density = (torch.exp(-(torch.matmul((grid - alpha[:2].unsqueeze(0)), Omega[:2,:2].inverse()) * (grid - alpha[:2].unsqueeze(0))).sum(dim=1)))
ax.contour(-grid[:,0].view(N, N), -grid[:,1].view(N, N), density.view(N, N), colors="gray")
ax.set_xlim(-1, 1)
ax.set_ylim(-1,1)
plt.show()




#
#with open("../frontier_categorical/fits/71model.py_process9_frontier14.txt", "r") as inFile:
#    fit = [[q for q in x.split(" ") if len(q) > 0] for x in inFile.read().strip().split("\n")]
#alpha_fit = [x for x in fit if len(x) > 0 and x[0].startswith("alpha")]
#print(alpha_fit)
#alpha = torch.FloatTensor([float(x[1]) for x in alpha_fit])
#print(alpha)
#alpha_names = [x[0] for x in alpha_fit]
#print(alpha_names)
#
#alpha = getShape(alpha, alpha_names)
#
#
#Omega_fit = [x for x in fit if len(x) > 0 and x[0].startswith("Omega")]
#print(Omega_fit)
#Omega = torch.FloatTensor([float(x[1]) for x in Omega_fit])
#print(Omega)
#Omega_names = [x[0] for x in Omega_fit]
#print(Omega_names)
#
#Omega = getShape(Omega, Omega_names)
#
#grid = torch.FloatTensor([[((x)/20-0.5,(y)/20) for y in range(60)] for x in range(60)]).view(-1, 2)
#print(grid)
#
#print(alpha[1])
#
#print(Omega[1])
#print(alpha[1])
#
#import math
#
#fig, ax = plt.subplots(nrows=1, ncols=4, figsize=(12, 6))
#
#
#density = (torch.exp(-(torch.matmul((grid - alpha[1,:2].unsqueeze(0)), Omega[1,:2,:2].inverse()) * (grid - alpha[1,:2].unsqueeze(0))).sum(dim=1)))
#print(density)
#ax[0].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60))
#ax[0].set_xlim(-2, 0)
#ax[0].set_ylim(-2,0)
#ax[0].plot(-alpha_frontier_shape_x[1], -alpha_frontier_shape_y[1])
#
#
#
#density = (torch.exp(-(torch.matmul((grid - alpha[2,:2].unsqueeze(0)), Omega[2,:2,:2].inverse()) * (grid - alpha[2,:2].unsqueeze(0))).sum(dim=1)))
#ax[1].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60))
#ax[1].set_xlim(-2, 0)
#ax[1].set_ylim(-2,0)
#ax[1].plot(-alpha_frontier_shape_x[2], -alpha_frontier_shape_y[2])
#
#
#density = (torch.exp(-(torch.matmul((grid - alpha[4,:2].unsqueeze(0)), Omega[4,:2,:2].inverse()) * (grid - alpha[4,:2].unsqueeze(0))).sum(dim=1)))
#ax[2].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60), colors='b')
#ax[2].set_xlim(-2, 0)
#ax[2].set_ylim(-2,0)
#ax[2].plot(-alpha_frontier_shape_x[4], -alpha_frontier_shape_y[4])
#
#density = (torch.exp(-(torch.matmul((grid - alpha[0,:2].unsqueeze(0)), Omega[0,:2,:2].inverse()) * (grid - alpha[0,:2].unsqueeze(0))).sum(dim=1)))
#ax[3].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), density.view(60, 60), colors='r')
#ax[3].set_xlim(-2, 0)
#ax[3].set_ylim(-2,0)
#ax[3].plot(-alpha_frontier_shape_x[0], -alpha_frontier_shape_y[0])
#
#
#
#
##ax[1].contour(grid_transformed_x.view(20, 7*20), grid_transformed_y.view(20, 7*20), density_transformed.view(20, 7*20))
##ax[1].scatter(circle[:,0], circle[:,1], c=color_circle.numpy().tolist(), cmap='viridis', vmin=0, vmax=1)
##ax[1].set_xlim(-1.5, 1)
##ax[1].set_ylim(-1.5,1)
#
##print(color_circle)
#
#
#
#plt.show()
#
