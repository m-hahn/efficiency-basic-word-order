import glob
import matplotlib.pyplot as plt
import torch


scaling_summary_data = {}
with open("../../output/process9_frontier18.py.tsv", "r") as inFile:
    for line in inFile:
        line = line.strip().split(" ")
        scaling_summary_data[(line[0], line[1])] = float(line[2])
print(scaling_summary_data)


with open("../../../ud_languages_2.8.txt", "r") as inFile:
    languages = inFile.read().strip().split("\n")

N_LANG = len(languages)



def getShape(x, names):
    names = [[int(q)-1 for q in y[y.index("[")+1:-1].split(",")] for y in names]
    N = len(names[0])
    size = [max([q[i] for q in names])+1 for i in range(N)]
    result = torch.zeros(size)
    for i in range(len(names)):
        result[tuple(names[i])] = x[i]
    return result

orders = ['No dominant order', 'SOV', 'SVO', 'VOS', 'VSO']


def getTensor(fit, name):
   alpha_fit = [x for x in fit if len(x) > 0 and x[0].startswith(f"{name}[")]
   alpha = torch.FloatTensor([float(x[6]) for x in alpha_fit]) # 6th element is the posterior median
   alpha_names = [x[0] for x in alpha_fit]
   alpha = getShape(alpha, alpha_names)
   return alpha




chains = []

chains_files = glob.glob("../../../../optimization/hillClimbing/output/forWords_OptimizeOrder_Coarse_FineSurprisal_Stoch_Adap_DLM_RunChain_Multiple.py*.tsv")
for chain in chains_files:
    print(chain)
    with open(chain, "r") as inFile:
        for line in inFile:
            if len(line) > 2:
               line = line.strip().split("\t")
#               print(line)
               language = line[0]
               if language not in languages:
                   continue
               if language in ["Japanese_2.8", "German_2.8", "Czech_2.8"]:
                   continue
               surprisals = torch.FloatTensor([float(x.split("_")[1]) for x in line[3::2]])
               deplens = torch.FloatTensor([float(x) for x in line[4::2]])
               try:
#                 print(surprisals)
 #                print((surprisals - scaling_summary_data[(language, "surprisalRange_min")]))
  #               print((scaling_summary_data[(language, "mean_x")]))
                 surprisals = ((surprisals - scaling_summary_data[(language, "surprisalRange_min")]) / (scaling_summary_data[(language, "surprisalRange_max")] - scaling_summary_data[(language, "surprisalRange_min")]) - (scaling_summary_data[(language, "mean_x")])) / (scaling_summary_data[(language, "mean_x")])
                 deplens = ((deplens - scaling_summary_data[(language, "deplenRange_min")]) / (scaling_summary_data[(language, "deplenRange_max")] - scaling_summary_data[(language, "deplenRange_min")]) - (scaling_summary_data[(language, "mean_y")])) / (scaling_summary_data[(language, "mean_y")])
                 chains.append(torch.stack([surprisals, deplens], dim=1))
#                 if True:
#                   print(scaling_summary_data[(language, "surprisalRange_min")], scaling_summary_data[(language, "surprisalRange_max")])
#                   print(scaling_summary_data[(language, "mean_x")])
#                   print(scaling_summary_data[(language, "mean_y")])
#                   print(language)
#                   print("RAW")
#                   print(torch.FloatTensor([float(x.split("_")[1]) for x in line[3::2]]))
#                   print("PROCESSED")
#                   print(surprisals)
#                 quit()
               except KeyError:
                   print(language)
chains = torch.stack(chains, dim=0)
print(chains.size())

#chainsPoint1 = chains[(chains[:,0] - torch.FloatTensor([-0.5041, -0.9916])).abs().sum(dim=1) < 0.001]
print(chains)
chainsPoints = []
chainsPoints.append( chains[(chains[:,0] - torch.FloatTensor([-0.0679, -0.3158])).abs().sum(dim=1) < 0.001])
chainsPoints.append( chains[(chains[:,0] - torch.FloatTensor([-0.4965, -0.8195])).abs().sum(dim=1) < 0.001])
chainsPoints.append( chains[(chains[:,0] - torch.FloatTensor([-0.8626, -0.4649])).abs().sum(dim=1) < 0.001])
chainsPoints.append( chains[(chains[:,0] - torch.FloatTensor([ 0.1184,  0.0282])).abs().sum(dim=1) < 0.001])
chainsPoints.append( chains[(chains[:,0] - torch.FloatTensor([-0.6235, -0.7103])).abs().sum(dim=1) < 0.001])
chainsPoints.append( chains[(chains[:,0] - torch.FloatTensor([-9.7754e-01, -1.5362e-01])).abs().sum(dim=1) < 0.001])
#chainsPoints.append( chains[(chains[:,0] - torch.FloatTensor([-0.0259, -0.8309])).abs().sum(dim=1) < 0.1])
print(chains[(chains[:,0] - torch.FloatTensor([-0.0, -0.990])).abs().sum(dim=1) < 0.2])
#print([len(x) for x in chainsPoints])
#quit()
#chains = torch.cat([chainsPoint2, chainsPoint3, chainsPoint4], dim=0)
#quit()

with open("../frontier_historical/fits/90model_reparam2.py_process9_frontier18_Revised.txt", "r") as inFile:
    fit = [[q for q in x.split(" ") if len(q) > 0] for x in inFile.read().strip().split("\n")]

alpha = getTensor(fit, "alpha")
Omega = getTensor(fit, "Omega")
Sigma = getTensor(fit, "Sigma")
B = getTensor(fit, "B")

print(alpha)
print(Sigma)
print(B)

alpha = -alpha[:2]
Sigma = Sigma[:2, :2]
B = B[:2, :2]

simulated_chains = []


chains = chains[:,:20]

print(chains.size())
for chainsPoint in chainsPoints:
 simulatedChainPoint = []
 chain_state = chainsPoint[:,0]
 N = chain_state.size()[0]
 brownian_noise = torch.distributions.MultivariateNormal(torch.zeros(2).unsqueeze(0).expand(N,-1), 0.003 * Sigma.unsqueeze(0).expand(N, -1, -1))

 for i in range(1000):
    simulatedChainPoint.append(chain_state)
    noise = brownian_noise.sample()
    drift = -torch.matmul((chain_state - alpha.unsqueeze(0)), B)
#    print(noise[0], drift[0])
    chain_state = chain_state.clone() + noise+ 0.003 * drift

    print(i, float((chain_state - simulatedChainPoint[0]).pow(2).sum(dim=1).sqrt().mean()), float((chains[:,0] - chains[:,-1]).pow(2).sum(dim=1).sqrt().mean()), 0.003*i*1000, "years")
    if float((chain_state - simulatedChainPoint[0]).pow(2).sum(dim=1).sqrt().mean()) >= float((chains[:,0] - chains[:,-1]).pow(2).sum(dim=1).sqrt().mean()):
        break
 
 simulated_chains.append(torch.stack(simulatedChainPoint, dim=1))
 print(simulated_chains[-1].size())
#quit()
YEARS = 0.003*i*1000
print("Iterations", i)
#quit()
#    print(chain_state[0])
 #   quit()
    
#quit()

assert len(chainsPoints) == len(simulated_chains)


summary_data = {}
with open("../../output/process9_frontier18.py.tsv", "r") as inFile:
    for line in inFile:
        line = line.strip().split(" ")
        summary_data[(line[0], line[1])] = float(line[2])
print(summary_data)

x_coordinates = {}
y_coordinates = {}
for language in languages:
 with open(f"../../output_frontier/process9_frontier18.py_{language}_FrontierDiscretizedByAngle.tsv", "r") as inFile:
    frontier = [float(x.split(" ")[2]) for x in inFile.read().strip().split("\n")]
    x = frontier[::2]
    y = frontier[1::2]
    x_coordinates[language] = x
    y_coordinates[language] = y

fOnFrontier = {}
for language in languages:
  with open(f"../../output_frontier/process9_frontier18.py_{language}_fOnFrontierDiscretizedByAngle.tsv", "r") as inFile:
     fOnFrontier[language] = [float(x.split(" ")[1]) for x in inFile.read().strip().split("\n")]

fOnFrontier = torch.FloatTensor([fOnFrontier[x] for x in languages])
      


print(x_coordinates)

x_coordinates = torch.FloatTensor([x_coordinates[x] for x in languages])
y_coordinates = torch.FloatTensor([y_coordinates[y] for y in languages])
print(x_coordinates.mean(dim=0))
print(y_coordinates.mean(dim=0))


x_positions_langs = torch.FloatTensor([summary_data[(x,"Distance_Surprisal_ComparedFrontier")] for x in languages])
y_positions_langs = torch.FloatTensor([summary_data[(y,"Distance_DepLen_ComparedFrontier")] for y in languages])


def sd(tensor, dim):
    return (tensor.pow(2).mean(dim=dim) - tensor.mean(dim=dim).pow(2)).sqrt()

families = {}
for q in ["../../../../languages/families_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        families[line[0]] = line[1]


categorical = {}
for q in ["../../../../languages/categorical_order/categorical_order_2.8.tsv", "../../../../languages/categorical_order/categoricalOrderAdditional_2.8.tsv", "../../../../languages/categorical_order/categoricalOrder_groups_2.8.tsv"]:
  with open(q, "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if len(line) < 2:
            print(line)
            continue
        categorical[line[0]] = line[1]

COLORS = {"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}



def getShape(x, names):
    names = [[int(q)-1 for q in y[y.index("[")+1:-1].split(",")] for y in names]
    N = len(names[0])
    size = [max([q[i] for q in names])+1 for i in range(N)]
    result = torch.zeros(size)
    for i in range(len(names)):
        result[tuple(names[i])] = x[i]
    return result

orders = ['No dominant order', 'SOV', 'SVO', 'VOS', 'VSO']


ancestral = {}
with open("../frontier_historical/fits/90model_reparam2.py_process9_frontier18_Revised.txt", "r") as inFile:
     fit = [[q for q in x.split(" ") if len(q) > 0] for x in inFile.read().strip().split("\n")]

FFrontierHidden_fit = [x for x in fit if len(x) > 0 and x[0].startswith("FFrontierHidden")]
print(FFrontierHidden_fit)
FFrontierHidden = torch.FloatTensor([float(x[1]) for x in FFrontierHidden_fit])
print(FFrontierHidden)
FFrontierHidden_names = [x[0] for x in FFrontierHidden_fit]
print(FFrontierHidden_names)

FFrontierHidden_frontier_shape = getShape(FFrontierHidden, FFrontierHidden_names)
print(FFrontierHidden_frontier_shape)

ancestralStates = [x for x in fit if len(x) > 0 and x[0] == "HIDDEN_LANGUAGES"][0][1:]
print(ancestralStates)
print(len(ancestralStates))
print(FFrontierHidden_frontier_shape.size())
assert len(ancestralStates)+1 == FFrontierHidden_frontier_shape.size()[0]

FFrontierHidden_frontier_shape[:,2] = (0.5*FFrontierHidden_frontier_shape[:,2])+0.5
print(FFrontierHidden_frontier_shape)
for i in range(len(ancestralStates)):
    ancestral[ancestralStates[i]] = FFrontierHidden_frontier_shape[i+1]
print(ancestral)
 


grid = torch.FloatTensor([[((x)/20-1.5,(y)/20-1.5) for y in range(60)] for x in range(60)]).view(-1, 2)

x_variance_langs = torch.FloatTensor([summary_data[(x,"Baseline_Variance_X_ComparedToFrontier")] for x in languages])
y_variance_langs = torch.FloatTensor([summary_data[(x,"Baseline_Variance_Y_ComparedToFrontier")] for x in languages])
xy_covariance_langs = torch.FloatTensor([summary_data[(x,"Baseline_Covariance_XY_ComparedToFrontier")] for x in languages])

baselines_covariance = torch.stack([x_variance_langs, xy_covariance_langs, xy_covariance_langs, y_variance_langs], dim=1).view(N_LANG, 2,2)

print(grid.size(), x_variance_langs.size())
print(baselines_covariance.size())
baseline_density = []
for i in range(len(languages)):
  baseline_density.append(torch.exp(-(grid.t() * torch.matmul(baselines_covariance[i].inverse(), grid.t())).sum(dim=0)))

baseline_densities = torch.stack(baseline_density, dim=0)
baseline_density = torch.stack(baseline_density, dim=0).mean(dim=0)


def drawLine(axis, old, new):
    if old in ancestral:
        x1 = -float(ancestral[old][0])
        y1 = -float(ancestral[old][1])
    else:
        x1 = -x_positions_langs[languages.index(old)]
        y1 = -y_positions_langs[languages.index(old)]
    if new in ancestral:
        x2 = -float(ancestral[new][0])
        y2 = -float(ancestral[new][1])
    else:
        x2 = -x_positions_langs[languages.index(new)]
        y2 = -y_positions_langs[languages.index(new)]
    axis.plot([x1, x2], [y1, y2], c="gray", alpha=0.5)


def plotLang(axis, lang):

    if lang in ancestral:
        x2 = -float(ancestral[lang][0])
        y2 = -float(ancestral[lang][1])
    else:
        x2 = -x_positions_langs[languages.index(lang)]
        y2 = -y_positions_langs[languages.index(lang)]
    axis.text(x2, y2, lang[:3].lower(), c={"SOV" : "green", "SVO" : "blue", "No dominant order" : "yellow", "VSO" : "black", "VOS" : "orange"}[categorical[lang]], alpha=(0.2 if lang in ancestral else 1))





import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

viridis = cm.get_cmap('viridis', 8)

import math

fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(12, 6))


print(fOnFrontier.size())
print(x_coordinates.size())

ax[0].set_xlim(-1.3, 0.2)
ax[0].set_ylim(-1.2,0.2)
ax[1].set_xlim(-1.3, 0.2)
ax[1].set_ylim(-1.2,0.2)
#ax[0].scatter(-x_coordinates.mean(dim=0), -y_coordinates.mean(dim=0), c=fOnFrontier.mean(dim=0), cmap='viridis', vmin=0, vmax=1)

indices = torch.ByteTensor([1 if families[x] == "Latin_Romance" else 0 for x in languages])
for i in range(9):
    ax[0].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)
    ax[1].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)



print(len(chainsPoints), 3)
print(len(simulated_chains), 3)

for chainPoint in chainsPoints:
  print(chainPoint.size())
  for chain in chainPoint[::1]:
    #print(chain)
  #  ax[0].scatter(chain[-1,0], chain[-1,1])
    ax[0].plot(chain[::,0], chain[::,1], linewidth=1, c="orange", alpha=0.1)
    ax[0].scatter(chain[0,0], chain[0,1], c="blue", s=50)
  ax[0].arrow(chainPoint[:,0,0].mean(), chainPoint[:,0,1].mean(), chainPoint[:,-1,0].mean() - chainPoint[:,0,0].mean(), chainPoint[:,-1,1].mean() - chainPoint[:,0,1].mean(), width=0.02, color="orange")
for chainPoint in simulated_chains:
  print(340, chainPoint.size())
  for chain in chainPoint[::1]:
#    print(chain)
 #   ax[1].scatter(chain[-1,0], chain[-1,1])
    ax[1].plot(chain[::7,0], chain[::7,1], linewidth=1, c="orange", alpha=0.1)
    ax[1].scatter(chain[0,0], chain[0,1], c="blue", s=50)
  ax[1].arrow(chainPoint[:,0,0].mean(), chainPoint[:,0,1].mean(), chainPoint[:,-1,0].mean() - chainPoint[:,0,0].mean(), chainPoint[:,-1,1].mean() - chainPoint[:,0,1].mean(), width=0.02, color="orange")

print(chains.max(dim=0))
print(chains.min(dim=0))
print(chains)

ax[0].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey", alpha=0.4)
ax[1].contour(-grid[:,0].view(60, 60), -grid[:,1].view(60, 60), baseline_density.view(60, 60), colors="grey", alpha=0.4)


print(YEARS, "years")


plt.show()

#print(alpha)
#print(Omega)
#print(simulated_chains[:,-1].mean(dim=0))
#print(simulated_chains[:,-1].pow(2).mean(dim=0) - simulated_chains[:,-1].mean(dim=0).pow(2))
