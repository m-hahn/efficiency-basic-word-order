functions {
}
data {
  int<lower=1> NumberOfDimensions;  // number of dimensions of the Ornstein-Uhlenbeck process
  int<lower=1> ObservedN;  // number of observations (i.e., observed languages)
  matrix<lower=-20, upper=20>[ObservedN, NumberOfDimensions] FFrontierObserved; // state of the observed languages
  int<lower=1> HiddenN; // number of unobserved languages (inner nodes without historical corpus)
  int<lower=1> TotalN; // overall number of languages
  int IsHidden[TotalN]; // indicates, for each language, whether it is unobserved or observed
  int ParentIndex[TotalN]; // for each language indicates its parent language in the phylogenetic tree
  int Total2Observed[TotalN]; // for each language, indicates its index within only the observed languages
  int Total2Hidden[TotalN]; // for each language, indicates its index within only the unobserved languages
  vector<lower=0>[TotalN] ParentDistance; // time distance to the parent node
  int prior_only;  // should the likelihood be ignored? (not used)
  int Components; // not used
  matrix[ObservedN, ObservedN] CovarianceMatrix; // matrix of separation times (Delta1 and Delta2)
  int FamiliesNum; // the number of maximal families (phyla)
  int FamiliesSize; // the maximum size of any family
  int FamiliesLists[FamiliesNum, FamiliesSize]; // for each family, indicates which languages belong to it
}
parameters {
  // Note: Unlike ../frontier_historical/90model_reparam2.stan, the states of unobserved languages are marginalized out
  vector[NumberOfDimensions] alpha; // the mean of the process (referred to as mu in Materials&Methods)
  vector<lower=0.1, upper=5>[NumberOfDimensions] sigma_B; // the diagonal entries of the drift matrix (referred to as Gamma in Materials&Methods). Note that, due to numerical issues, we had to take a relatively tighter bound on the variable here, but see ../frontier_historical/90model_reparam2.stan for a version with a looser bound and equivalent results.

  cholesky_factor_corr[NumberOfDimensions] Lrescor_Sigma;  // Cholesky factor of correlation component of Sigma (covariance of instantaneous changes, as defined in Materials&Methods). Referred to as U in SI Appendix Section S7.2.
  vector<lower=0.01, upper=20>[NumberOfDimensions] sigma_Sigma; // standard deviation component of Sigma. Referred to as D in SI Appendix Section S7.2.
}
transformed parameters {
  // This is for Stepping Stone Sampling, not used here
  real stepping = 1.0; 

  real targetPrior = 0;
  real targetLikelihood = 0;
  //

  // intermediate steps
 matrix[NumberOfDimensions, NumberOfDimensions] Sigma_chol; // Referred to as DU in SI Appendix Section S7.2.
 matrix[NumberOfDimensions, NumberOfDimensions] Sigma; // covariance of instantaneous changes, as defined in Materials&Methods

 matrix[NumberOfDimensions,NumberOfDimensions] B; // drift matrix: referred to as Gamma in Materials&Methods
 matrix[NumberOfDimensions,NumberOfDimensions] Omega; // covariance of stationary distribution, as defined in Materials&Methods

  // Fill the drift matrix 
  for(i in 1:NumberOfDimensions) {
     for(j in 1:NumberOfDimensions) {
       if(i==j) {
         B[i,j] = sigma_B[i];
       } else {
         B[i,j]= 0;
       }
     }
  }
//
 Sigma_chol = diag_pre_multiply(sigma_Sigma, Lrescor_Sigma); // referred to as DU in SI Appendix Section S7.2.
 Sigma = multiply_lower_tri_self_transpose(Sigma_chol); // compute Sigma

   
     // Now calculate Omega, the stationary covariance, using formula from Materials&Methods (SI Appendix, Equation (19))
     for(q in 1:NumberOfDimensions) {
       for(r in 1:NumberOfDimensions) {
         Omega[q,r] = Sigma[q,r] / (B[q,q] + B[r,r]);
       };
     }
  


 { //////////////////// likelihood block

 // Prior (as defined in SI Appendix, Section S7.2)
  targetPrior += normal_lpdf(alpha | 0, 1);

  targetPrior += normal_lpdf(sigma_B | 0, 1);
  targetPrior += normal_lpdf(sigma_Sigma | 0, 1);
  targetPrior += lkj_corr_cholesky_lpdf(Lrescor_Sigma | 1);

 // For each family, compute the likelihood of the observations using SI Appendix, Equation (21).
  for(family in 1:FamiliesNum) {

    // Identify the number of languages in this family
    int familySizeHere = FamiliesSize;
    for(language in 1:FamiliesSize) {
       if(FamiliesLists[family, language] == 0) {
          familySizeHere = language-1;
          break;
       }
    } // loop over 'language'

    { // block
     vector[NumberOfDimensions*familySizeHere] own_overall; // the states of the languages in this family
     matrix[NumberOfDimensions*familySizeHere, NumberOfDimensions*familySizeHere] fullCovMat; // the variance-covariance matrix (Equation (21) in SI Appendix)
     vector[NumberOfDimensions*familySizeHere] fullMeanVector; // the mean vector (which consists simply of copies of the overall mean vector)
   
     for(i in 1:familySizeHere) { // for each language in the family...
        for(v in 1:NumberOfDimensions) {
            fullMeanVector[NumberOfDimensions*(i-1)+v] = alpha[v];
        }
        for(j in 1:i) { // and for each other language (do j <= i to avoid duplicating computation)
          matrix[NumberOfDimensions, NumberOfDimensions] covarianceHere; // cross-covariance for the pair of languages
          if(CovarianceMatrix[FamiliesLists[family,i],FamiliesLists[family,j]] > 30) { // essentially no statistical dependency (for our data, this should not happen)
            for(w in 1:NumberOfDimensions) {
               for(z in 1:NumberOfDimensions) {
                 covarianceHere[w,z] = 0;
            }  }
          } else if(i == j) { // here, the distance is zero
            covarianceHere = Omega; // cross-covariance here is simply the stationary covariance
          } else {
            // Equation (21) in SI Appendix
            matrix[NumberOfDimensions,NumberOfDimensions] exponentiated1 = matrix_exp(-CovarianceMatrix[FamiliesLists[family, i],FamiliesLists[family, j]] * B);
            matrix[NumberOfDimensions,NumberOfDimensions] exponentiated2 = matrix_exp(-CovarianceMatrix[FamiliesLists[family, j],FamiliesLists[family, i]] * B);
            covarianceHere = exponentiated1 * Omega * exponentiated2';
          }
          for(u in 1:NumberOfDimensions) {
           for(v in 1:NumberOfDimensions) {
              fullCovMat[NumberOfDimensions*(i-1)+u, NumberOfDimensions*(j-1)+v] = covarianceHere[u,v];
              fullCovMat[NumberOfDimensions*(j-1)+u, NumberOfDimensions*(i-1)+v] = covarianceHere[v,u];
           } // loop over v
          } // loop over u
        } // loop over j
   

        // Fill in observations
        for(j in 1:familySizeHere) {
           for(v in 1:NumberOfDimensions) {
              own_overall[NumberOfDimensions*(j-1)+v] = FFrontierObserved[FamiliesLists[family, j],v];
           }
        } // loop over j



     } // loop over i
        targetLikelihood += multi_normal_lpdf(own_overall | fullMeanVector, fullCovMat); // Likelihood for the languages in the family
    } // end block
   } // loop over 'family'

 } // likelihood block
}
model {
  target += stepping * targetLikelihood + targetPrior;
}
generated quantities {
}

