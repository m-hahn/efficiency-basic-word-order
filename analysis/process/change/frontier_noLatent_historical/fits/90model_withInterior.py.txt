Inference for Stan model: anon_model_8235a8f146f28e31aa212258decf90a2.
4 chains, each with iter=2000; warmup=1000; thin=1; 
post-warmup draws per chain=1000, total post-warmup draws=4000.

                        mean se_mean     sd   2.5%    25%    50%     75%  97.5%  n_eff   Rhat
FFrontierHidden[1,1]   -0.03  7.2e-3   0.57  -1.16   -0.4  -0.03    0.35   1.09   6233    1.0
FFrontierHidden[2,1]    -0.1  3.1e-3   0.24   -0.6  -0.26   -0.1    0.06   0.38   6101    1.0
FFrontierHidden[3,1]    0.21  1.8e-3   0.13  -0.05   0.12   0.21     0.3   0.47   5723    1.0
FFrontierHidden[4,1]   -0.07  5.0e-3   0.37  -0.81  -0.32  -0.07    0.18   0.67   5570    1.0
FFrontierHidden[5,1]    0.06  4.0e-3    0.3  -0.55  -0.14   0.06    0.26   0.64   5620    1.0
FFrontierHidden[6,1]    0.03  6.5e-3   0.43  -0.84  -0.23   0.04    0.31   0.85   4237    1.0
FFrontierHidden[7,1]     0.9  3.3e-3   0.25   0.41   0.73    0.9    1.07    1.4   5783    1.0
FFrontierHidden[8,1]    0.08  7.8e-3   0.61  -1.13  -0.33   0.07    0.49   1.25   6156    1.0
FFrontierHidden[9,1]    -0.1  3.5e-3   0.29  -0.68  -0.29   -0.1     0.1   0.46   7109    1.0
FFrontierHidden[10,1]  -0.22  3.5e-3   0.28  -0.78  -0.41  -0.22   -0.03   0.34   6287    1.0
FFrontierHidden[11,1]  -0.07  1.5e-3   0.11  -0.27  -0.14  -0.07 -3.0e-4   0.15   4947    1.0
FFrontierHidden[12,1]   0.33  2.3e-3   0.16   0.02   0.23   0.33    0.44   0.66   4794    1.0
FFrontierHidden[13,1]   0.43  2.6e-3   0.22   0.01   0.29   0.44    0.58   0.86   7025    1.0
FFrontierHidden[14,1]    0.5  1.6e-3   0.12   0.27   0.43    0.5    0.58   0.73   5583    1.0
FFrontierHidden[15,1]   -0.1  2.5e-3   0.14  -0.38  -0.19   -0.1 -5.5e-3   0.17   3248    1.0
FFrontierHidden[16,1]   0.21  5.4e-3   0.41  -0.61  -0.05   0.21    0.48   1.02   5710    1.0
FFrontierHidden[17,1]   0.52  4.5e-3   0.36  -0.19   0.28   0.52    0.76   1.24   6369    1.0
FFrontierHidden[18,1]    0.2  5.3e-3   0.38  -0.54  -0.05    0.2    0.45   0.94   5134    1.0
FFrontierHidden[19,1]   0.79  4.6e-3   0.37   0.06   0.55    0.8    1.03   1.49   6434    1.0
FFrontierHidden[20,1]  -0.04  2.4e-3   0.18  -0.39  -0.16  -0.04    0.08   0.31   5238    1.0
FFrontierHidden[21,1]  -0.87  1.1e-3   0.09  -1.03  -0.93  -0.87   -0.81   -0.7   6066    1.0
FFrontierHidden[22,1]   0.22  7.3e-3   0.58  -0.91  -0.18   0.22    0.61   1.33   6249    1.0
FFrontierHidden[23,1]  -0.26  2.3e-3   0.17  -0.62  -0.37  -0.26   -0.14   0.09   5642    1.0
FFrontierHidden[24,1]   0.09  5.5e-3   0.42  -0.73  -0.19   0.09    0.37   0.93   5935    1.0
FFrontierHidden[25,1]   0.26  6.1e-3   0.38  -0.49   0.01   0.26     0.5    1.0   3825    1.0
FFrontierHidden[26,1]  -0.34  9.9e-4   0.08   -0.5  -0.39  -0.34   -0.28  -0.17   7095    1.0
FFrontierHidden[27,1]  -0.14  2.0e-3   0.11  -0.38  -0.22  -0.14   -0.07   0.08   3196    1.0
FFrontierHidden[28,1]  -0.14  1.7e-3    0.1  -0.35  -0.21  -0.14   -0.07   0.06   3506    1.0
FFrontierHidden[29,1]  -0.04  5.4e-3   0.44   -0.9  -0.33  -0.04    0.25    0.8   6619    1.0
FFrontierHidden[30,1]   0.07  2.3e-3   0.15  -0.23  -0.03   0.06    0.17   0.37   4303    1.0
FFrontierHidden[31,1]  -0.12  2.0e-3   0.12  -0.36   -0.2  -0.12   -0.03   0.12   3842    1.0
FFrontierHidden[32,1]  -0.26  2.1e-3   0.16  -0.57  -0.36  -0.26   -0.15   0.05   5725    1.0
FFrontierHidden[33,1]    0.2  6.5e-3   0.39  -0.63  -0.05    0.2    0.45   0.97   3707    1.0
FFrontierHidden[34,1]  -0.15  2.2e-3   0.13  -0.41  -0.24  -0.15   -0.06   0.09   3428    1.0
FFrontierHidden[35,1]  -0.03  2.5e-3   0.15  -0.33  -0.13  -0.03    0.07   0.26   3727    1.0
FFrontierHidden[36,1]   -0.2  2.5e-3   0.17  -0.53  -0.31   -0.2   -0.08   0.13   4751    1.0
FFrontierHidden[1,2]   -0.02  5.4e-3   0.42  -0.87   -0.3  -0.02    0.26   0.82   6106    1.0
FFrontierHidden[2,2]    0.08  4.1e-3   0.34  -0.58  -0.14   0.09    0.31   0.73   6620    1.0
FFrontierHidden[3,2]   -0.24  2.6e-3   0.21  -0.66  -0.38  -0.24    -0.1   0.16   6301    1.0
FFrontierHidden[4,2]   -0.05  4.9e-3    0.4  -0.85  -0.32  -0.05    0.21   0.73   6735    1.0
FFrontierHidden[5,2]    0.14  5.7e-3   0.39  -0.66  -0.11   0.14     0.4    0.9   4782    1.0
FFrontierHidden[6,2]   -0.03  5.9e-3   0.43  -0.87  -0.31  -0.04    0.24   0.82   5299    1.0
FFrontierHidden[7,2]    0.39  4.6e-3   0.37  -0.34   0.15   0.38    0.63   1.11   6316    1.0
FFrontierHidden[8,2]   -0.02  5.3e-3   0.43  -0.84  -0.31  -0.03    0.28   0.85   6541    1.0
FFrontierHidden[9,2]   -0.19  5.0e-3   0.38  -0.92  -0.44   -0.2    0.06   0.56   5807    1.0
FFrontierHidden[10,2]   0.04  4.4e-3   0.37   -0.7  -0.21   0.05    0.29   0.74   6872    1.0
FFrontierHidden[11,2]   0.05  2.6e-3   0.17  -0.27  -0.06   0.05    0.16   0.41   4262    1.0
FFrontierHidden[12,2]  -0.52  3.9e-3   0.26  -0.99  -0.68  -0.53   -0.36   0.03   4511    1.0
FFrontierHidden[13,2]   0.12  4.2e-3   0.34  -0.56  -0.11   0.12    0.34    0.8   6551    1.0
FFrontierHidden[14,2]   0.07  2.6e-3   0.18  -0.28  -0.05   0.07    0.19   0.44   5042    1.0
FFrontierHidden[15,2]   0.17  4.1e-3   0.24  -0.31   0.02   0.17    0.32   0.63   3372    1.0
FFrontierHidden[16,2]   0.06  5.3e-3   0.42  -0.75  -0.22   0.07    0.34   0.91   6145    1.0
FFrontierHidden[17,2]   0.12  5.2e-3   0.41  -0.66  -0.16   0.11    0.39   0.95   6134    1.0
FFrontierHidden[18,2]   0.08  5.3e-3   0.41  -0.72  -0.21   0.08    0.36   0.87   6072    1.0
FFrontierHidden[19,2]    0.2  4.8e-3   0.41  -0.57  -0.07   0.19    0.47   1.01   7213    1.0
FFrontierHidden[20,2]   0.12  4.0e-3   0.28  -0.44  -0.06   0.11    0.31   0.66   4917    1.0
FFrontierHidden[21,2]   -0.7  1.7e-3   0.14  -0.97  -0.79   -0.7   -0.61  -0.44   6060    1.0
FFrontierHidden[22,2]   0.04  5.6e-3   0.43  -0.81  -0.26   0.04    0.33    0.9   6095    1.0
FFrontierHidden[23,2]  -0.22  3.6e-3   0.27  -0.77   -0.4  -0.22   -0.03    0.3   5894    1.0
FFrontierHidden[24,2]  -0.06  5.1e-3   0.42  -0.89  -0.33  -0.06    0.22   0.76   6665    1.0
FFrontierHidden[25,2]   0.14  6.2e-3   0.41  -0.66  -0.15   0.14    0.42   0.94   4428    1.0
FFrontierHidden[26,2]    0.1  1.8e-3   0.13  -0.16   0.02    0.1    0.19   0.38   5683    1.0
FFrontierHidden[27,2]   0.15  3.4e-3   0.19  -0.23   0.03   0.16    0.27   0.51   3131    1.0
FFrontierHidden[28,2]   0.16  2.9e-3   0.16  -0.17   0.05   0.16    0.27   0.48   3328    1.0
FFrontierHidden[29,2]  -0.03  4.9e-3   0.41  -0.86   -0.3  -0.02    0.25   0.77   7195    1.0
FFrontierHidden[30,2]  -0.13  4.3e-3   0.26   -0.6  -0.31  -0.14    0.02   0.41   3493    1.0
FFrontierHidden[31,2]   0.23  3.5e-3   0.21  -0.19   0.09   0.22    0.36   0.64   3554    1.0
FFrontierHidden[32,2]  -0.34  3.3e-3   0.26  -0.84  -0.51  -0.34   -0.17   0.17   5939    1.0
FFrontierHidden[33,2]   0.07  6.6e-3   0.42  -0.73  -0.22   0.07    0.36   0.91   4066    1.0
FFrontierHidden[34,2]   0.14  3.8e-3   0.22  -0.31 1.0e-3   0.14    0.28   0.55   3269    1.0
FFrontierHidden[35,2]   0.13  4.0e-3   0.25  -0.36  -0.03   0.12    0.29   0.62   3747    1.0
FFrontierHidden[36,2]   0.17  4.6e-3   0.27  -0.37 3.7e-3   0.18    0.36   0.71   3478    1.0
alpha[1]               -0.03  1.5e-3   0.11  -0.26  -0.11  -0.03    0.04   0.18   5347    1.0
alpha[2]               -0.02  7.7e-4   0.06  -0.14  -0.06  -0.02    0.02   0.09   5336    1.0
sigma_B[1]               0.2  1.3e-3   0.07   0.11   0.15   0.19    0.24   0.37   2595    1.0
sigma_B[2]              1.02    0.01   0.41   0.46   0.73   0.93    1.22   2.07   1656    1.0
Lrescor_Sigma[1,1]       1.0     nan    0.0    1.0    1.0    1.0     1.0    1.0    nan    nan
Lrescor_Sigma[2,1]      0.49  1.9e-3    0.1   0.29   0.42   0.49    0.56   0.68   2604    1.0
Lrescor_Sigma[1,2]       0.0     nan    0.0    0.0    0.0    0.0     0.0    0.0    nan    nan
Lrescor_Sigma[2,2]      0.86  1.1e-3   0.06   0.73   0.83   0.87    0.91   0.96   2499    1.0
sigma_Sigma[1]          0.38  1.0e-3   0.05   0.31   0.35   0.37    0.41   0.49   2112    1.0
sigma_Sigma[2]          0.59  2.7e-3    0.1   0.43   0.52   0.58    0.65   0.84   1555    1.0
stepping                 1.0     nan    0.0    1.0    1.0    1.0     1.0    1.0    nan    nan
targetPrior             -7.1    0.01    0.6  -8.78  -7.27  -6.91   -6.71  -6.51   1668    1.0
targetLikelihood      -73.36    0.27   8.63 -91.53 -78.96 -73.06  -67.32 -57.53    987    1.0
Sigma_chol[1,1]         0.38  1.0e-3   0.05   0.31   0.35   0.37    0.41   0.49   2112    1.0
Sigma_chol[2,1]         0.29  2.2e-3   0.09   0.15   0.23   0.28    0.34   0.52   1828    1.0
Sigma_chol[1,2]          0.0     nan    0.0    0.0    0.0    0.0     0.0    0.0    nan    nan
Sigma_chol[2,2]         0.51  2.0e-3   0.08   0.38   0.45    0.5    0.56   0.71   1739    1.0
Sigma[1,1]              0.15  8.1e-4   0.04   0.09   0.12   0.14    0.17   0.24   2116    1.0
Sigma[2,1]              0.11 10.0e-4   0.04   0.05   0.08    0.1    0.13   0.22   1810    1.0
Sigma[1,2]              0.11 10.0e-4   0.04   0.05   0.08    0.1    0.13   0.22   1810    1.0
Sigma[2,2]              0.36  3.4e-3   0.13   0.19   0.27   0.33    0.42   0.71   1548    1.0
B[1,1]                   0.2  1.3e-3   0.07   0.11   0.15   0.19    0.24   0.37   2595    1.0
B[2,1]                   0.0     nan    0.0    0.0    0.0    0.0     0.0    0.0    nan    nan
B[1,2]                   0.0     nan    0.0    0.0    0.0    0.0     0.0    0.0    nan    nan
B[2,2]                  1.02    0.01   0.41   0.46   0.73   0.93    1.22   2.07   1656    1.0
Omega[1,1]              0.38  1.3e-3   0.08   0.25   0.32   0.37    0.43   0.57   3637    1.0
Omega[2,1]               0.1  5.0e-4   0.03   0.05   0.08   0.09    0.11   0.16   3351    1.0
Omega[1,2]               0.1  5.0e-4   0.03   0.05   0.08   0.09    0.11   0.16   3351    1.0
Omega[2,2]              0.18  6.6e-4   0.03   0.13   0.16   0.18     0.2   0.26   2542    1.0
lp__                  -85.33    0.27   8.38 -102.8 -90.68  -85.0  -79.38 -70.02    983    1.0

Samples were drawn using NUTS at Tue Jan  4 02:03:08 2022.
For each parameter, n_eff is a crude measure of effective sample size,
and Rhat is the potential scale reduction factor on split chains (at 
convergence, Rhat=1).
HIDDEN_LANGUAGES Afroasiatic Arabic Armenian Balto_Slavic Brythonic Central_Semitic Common_Turkic Defoid Eastern_Baltic Finnic Germanic Global_Dutch Goidelic Hindustani Iberian_Romance Indo_European Indo_Iranian Insular_Celtic Iranian Italo_Western_Romance Macro_English Niger_Congo North_Germanic Permic Semitic Serbo_Croatian Slavic South_Slavic Uralic West_Germanic West_Iberian West_Scandinavian West_Semitic West_Slavic Western_Romance Western_South_Slavic
CorrelationFFrontierCongruence.mean() 0.3639435236751333
0.19314530899391535
0.3639405441275929
0.5357022700474512
1.0
