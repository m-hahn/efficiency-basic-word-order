# Phylogenetic analysis

This implementation uses the analytical form of the full variance-covariance matrix.

* [Main analysis](fits/90model.py.txt)
* [Without Indo-European](fits/90model_NoIE.py.txt)
* [With noise](fits/90model_Noise.py.txt)
* [Marginal likelihood](90model_bounds_Marg_Bin.py.txt)
* [Marginal likelihood without coadaptation](marginal_likelihood/90model_bounds_Marg_Bin_NoCor.py.txt)
* [Marginal likelihood with noise](marginal_likelihood/90model_bounds_Marg_Bin_Noise.py.txt)

