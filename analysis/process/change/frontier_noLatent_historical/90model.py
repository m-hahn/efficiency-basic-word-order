import pystan
from math import sqrt, log
import math
import sys

group = "process9_frontier18_Revised"

with open("trees2_2.8.tsv", "r") as inFile:
  trees = [x.split("\t") for x in inFile.read().strip().split("\n")][1:]
print(trees)

# Record which languages are observed, and record for each language its parents
observedLangs = set()
allLangs = set()
parents = {}
for line in trees:
   observedLangs.add(line[0])
   allLangs.add(line[0])
   for i in range(len(line)-1):
       child = line[i]
       parent = line[i+1]
       if child in parents:
         assert parents[child] == parent, (parents[child], line, child, i)
       parents[child] = parent
       allLangs.add(parent)

# Sanity check: Includes historical datasets
assert "PARTITION-Icelandic_middle" in observedLangs
assert "ISWOC_Old_English" in observedLangs

# Retrieve dating for historical languages
with open("groups2_2.8.tsv", "r") as inFile:
  dates = [x.split("\t") for x in inFile.read().strip().split("\n")][1:]
print([len(x) for x in dates])
dates = dict(dates)

# Set the root to some time very far in the past, plays no role in the computations
dates["_ROOT_"] = -50000
print(dates)
for x in allLangs:
  if x not in observedLangs:
    if x not in dates:
       print(x)

import glob
assert group.startswith("process")
# Retrieve the states of the observed languages
pareto_distance_x  = {x[0] : float(x[2]) for x in [x.split(" ") for x in open(f"../../output/{group}.py.tsv", "r").read().strip().split("\n")] if x[1] == "Distance_Surprisal_ComparedFrontier" } # IL
pareto_distance_y  = {x[0] : float(x[2]) for x in [x.split(" ") for x in open(f"../../output/{group}.py.tsv", "r").read().strip().split("\n")] if x[1] == "Distance_DepLen_ComparedFrontier" } # DL
average_f  = {x[0] : float(x[2]) for x in [x.split(" ") for x in open(f"../../output/{group}.py.tsv", "r").read().strip().split("\n")] if x[1] == "AverageFAngleSpace" } # Average congruence

fOnFrontier = {}
for f in glob.glob(f"../../output_frontier/{group}.py_*_fOnFrontierDiscretizedByAngle.tsv"):
  with open(f, "r") as inFile:
    func = [x.split(" ") for x in inFile.read().strip().split("\n")]
    assert len(func) == 10
    func = [float(x[1]) for x in func]
    language = f[f.index(".py")+4:f.rfind("_")]
    print(language)
    fOnFrontier[language] = func
print(fOnFrontier)

# Retrieve attested congruence
with open("../../../../languages/extract_congruence/output/evaluateCongruence_POS_NoSplit.py.tsv", "r") as inFile:
   data = [x.replace('"', '').split("\t") for x in inFile.read().strip().split("\n")]
header = data[0]
header = header
header = dict(list(zip(header, range(len(header)))))
data = data[1:]
print(header)

# Assemble state information for the languages
valueByLanguage = {}
for line in data:
   print(line)
   language = line[header["Language"]]
   print(line)
   if language not in fOnFrontier:
     print("MISSING LANGUAGE", language)
     continue
   x = float('nan')
   y = float('nan')
   z = float(line[header["Congruence_All"]])
   valueByLanguage[language] = [x,y,z] 


# Sanity check
assert "PARTITION-Icelandic_modern" in valueByLanguage

print(valueByLanguage)
print(observedLangs)
print(allLangs)


# Unobserved languages
hiddenLangs = sorted([x for x in allLangs if x not in observedLangs and x != "_ROOT_"])
print(hiddenLangs)


# Sanity check
assert "ISWOC_Old_English" in valueByLanguage
assert "PARTITION-Ancient_Greek_classical" in valueByLanguage
assert "PARTITION-Ancient_Greek_classical" in observedLangs

# Observed languages
observedLanguages = sorted([x for x in list(observedLangs) if x in valueByLanguage])
hiddenLanguages = sorted(hiddenLangs)
totalLanguages = ["_ROOT_"] + hiddenLanguages + observedLanguages
# Number languages
lang2Code = dict(list(zip(totalLanguages, range(len(totalLanguages)))))
lang2Observed = dict(list(zip(observedLanguages, range(len(observedLanguages)))))


# Sanity check: Non-UD languages and historical corpora are included
assert "ISWOC_Old_English" in observedLanguages, observedLanguages
assert "ISWOC_Old_English" in totalLanguages, totalLanguages

assert "PARTITION-Icelandic_middle" in observedLangs
assert "PARTITION-Icelandic_middle" in valueByLanguage
assert "PARTITION-Icelandic_middle" in observedLanguages

assert 'Latin_2.8' in valueByLanguage, valueByLanguage
assert 'Latin_2.8' in totalLanguages
assert 'Latin_2.8' in lang2Code


# For each language, compute the distance to its parent node
distanceToParent = {}
for language in allLangs:
   parent = parents.get(language, "_ROOT_")
   if language in observedLangs and parent == "_ROOT_":
     print("ROOT", language)
   dateLang = dates.get(language, 2000)
   dateParent = dates[parent]
   distanceToParent[language] = (float(dateLang)-float(dateParent))/1000
print(distanceToParent)

# Sanity check
print(parents.get("Classical_Chinese_2.8"))
assert "Classical_Chinese_2.8" in observedLangs

# Prepare data structures encoding the phylogenetic tree and the temporal information
from collections import defaultdict

listOfAllAncestors = defaultdict(list)

def processDescendants(l, desc):
   listOfAllAncestors[desc].append(l)
   if l != "_ROOT_":
      parent = parents.get(l, "_ROOT_")
      processDescendants(parent, desc)
   
for language in totalLanguages:
   processDescendants(language, language)
print(listOfAllAncestors)

# This matrix indicates, for each pair of languages, the time that separates them (Delta1 and Delta2 as in SI Appendix, Section S7.1)
covarianceMatrix = [[None for _ in range(len(observedLanguages))] for _ in range(len(observedLanguages))]
for i in range(len(observedLanguages)):
   for j in range(i+1):
      l1 = observedLanguages[i]
      l2 = observedLanguages[j]
      commonAncestorsDates = [int(dates.get(l, 2000)) for l in listOfAllAncestors[l1] if l in listOfAllAncestors[l2] and l != "_ROOT_"]
      if len(commonAncestorsDates) > 0:
         commonTime = max(commonAncestorsDates)
      else:
         commonTime = -50000
      separateTime1 = (int(dates.get(l1, 2000)) - commonTime)
      separateTime2 = (int(dates.get(l2, 2000)) - commonTime)
      if commonTime > -50000:
         print(l1, l2, separateTime1, separateTime2)
      else:
         separateTime1 = 1000000
         separateTime2 = 1000000
      covarianceMatrix[i][j] = (separateTime1)/1000
      covarianceMatrix[j][i] = (separateTime2)/1000

families = defaultdict(list)
for language in observedLanguages:
  ancestors = listOfAllAncestors[language]
  print(ancestors)
  assert ancestors[-1] == "_ROOT_"
  families[([language] + ancestors)[-2]].append(language)
print(families)
familiesLists = [[observedLanguages.index(x)+1 for x in z] for _, z in families.items()]
print(familiesLists)
familiesListsMaxLen = max([len(x) for x in familiesLists])
familiesLists = [x+[0 for _ in range(familiesListsMaxLen-len(x))] for x in familiesLists]
print(familiesLists)

# Sanity check
assert "PARTITION-Icelandic_middle" in observedLanguages

# Now assemble all the data for the Stan script
dat = {}

# Sanity check
assert len(set(observedLanguages)) == len(observedLanguages)
assert (all(["Greek" in x for x in observedLanguages if x not in pareto_distance_y])), [x for x in observedLanguages if x not in pareto_distance_y]

dat["ObservedN"] = len(observedLanguages)
dat["FFrontierObserved"] = [[pareto_distance_x[x]*2-1, pareto_distance_y[x]*2-1, valueByLanguage[x][2]*2-1, average_f[x]*2-1] for x in observedLanguages]
dat["HiddenN"] = len(hiddenLanguages)+1
dat["TotalN"] = dat["ObservedN"] + dat["HiddenN"]
dat["IsHidden"] = [1]*dat["HiddenN"] + [0]*dat["ObservedN"]
dat["ParentIndex"] = [0] + [1+lang2Code[parents.get(x, "_ROOT_")] for x in hiddenLanguages+observedLanguages]
dat["Total2Observed"] = [0]*dat["HiddenN"] + list(range(1,1+len(observedLanguages)))
dat["Total2Hidden"] = [1] + list(range(2,2+len(hiddenLanguages))) + [0 for _ in observedLanguages]
dat["ParentDistance"] = [0] + [distanceToParent[x] for x in hiddenLanguages+observedLanguages]
dat["CovarianceMatrix"] = covarianceMatrix
dat["prior_only"] = 0
dat["Components"] = 2
dat["NumberOfDimensions"] = len(dat["FFrontierObserved"][0])
dat["FamiliesLists"] = familiesLists
dat["FamiliesNum"] = len(familiesLists)
dat["FamiliesSize"] = len(familiesLists[0])


print(dat)

print(hiddenLanguages)

sm = pystan.StanModel(file=f'{__file__[:-3]}.stan')


fit = sm.sampling(data=dat, iter=10000, chains=4, control=dict(adapt_delta=0.95))
la = fit.extract(permuted=True)  # return a dictionary of arrays

# Extract correlations
import numpy as np
import matplotlib.pyplot as plt

for j in range(la["Omega"].shape[1]):
  for i in range(j):
    correlation = (la["Omega"][:,i,j] / np.sqrt(la["Omega"][:,i,i] * la["Omega"][:,j,j]))
    
    fig, ax = plt.subplots(figsize =(6, 2))
    counts, _, _ = ax.hist(correlation)
    ax.set_xlim(-1,1)
    ax.set(yticklabels= [])
    ax.plot([0,0], [0,counts.max()], color="gray")
    plt.savefig(f"fits/figures/{__file__[:-3]}_{i}_{j}.pdf")
 

# Save posterior summary
with open(f"fits/{__file__}.txt", "w") as outFile:
   print(fit, file=outFile)
   print("HIDDEN_LANGUAGES", " ".join(hiddenLanguages), file=outFile)


   CorrelationFFrontierCongruence = (la["Omega"][:,2,3] / np.sqrt(la["Omega"][:,2,2] * la["Omega"][:,3,3]))
   print("CorrelationFFrontierCongruence.mean()", CorrelationFFrontierCongruence.mean(), file=outFile)
   print(np.quantile(CorrelationFFrontierCongruence, 0.025), file=outFile)
   print(np.quantile(CorrelationFFrontierCongruence, 0.5), file=outFile)
   print(np.quantile(CorrelationFFrontierCongruence, 0.975), file=outFile)
   print(np.mean(CorrelationFFrontierCongruence > 0), file=outFile)
   
   CorrelationDepLenCongruence = (la["Omega"][:,1,2] / np.sqrt(la["Omega"][:,1,1] * la["Omega"][:,2,2]))
   print("CorrelationDepLenCongruence.mean()", CorrelationDepLenCongruence.mean(), file=outFile)
   print(np.quantile(CorrelationDepLenCongruence, 0.025), file=outFile)
   print(np.quantile(CorrelationDepLenCongruence, 0.5), file=outFile)
   print(np.quantile(CorrelationDepLenCongruence, 0.975), file=outFile)
   print(np.mean(CorrelationDepLenCongruence > 0), file=outFile)

   CorrelationSurprisalCongruence = (la["Omega"][:,0,2] / np.sqrt(la["Omega"][:,0,0] * la["Omega"][:,2,2]))
   print("CorrelationSurprisalCongruence.mean()", CorrelationSurprisalCongruence.mean(), file=outFile)
   print(np.quantile(CorrelationSurprisalCongruence, 0.025), file=outFile)
   print(np.quantile(CorrelationSurprisalCongruence, 0.5), file=outFile)
   print(np.quantile(CorrelationSurprisalCongruence, 0.975), file=outFile)
   print(np.mean(CorrelationSurprisalCongruence > 0), file=outFile)



