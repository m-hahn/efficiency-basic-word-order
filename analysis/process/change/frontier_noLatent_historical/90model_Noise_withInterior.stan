functions {
}
data {
  int<lower=1> NumberOfDimensions;  // number of observations
  int<lower=1> ObservedN;  // number of observations
  matrix<lower=-20, upper=20>[ObservedN, NumberOfDimensions] FFrontierObserved;
  int<lower=1> HiddenN;
  int<lower=1> TotalN;
  int IsHidden[TotalN];
  int ParentIndex[TotalN];
  int Total2Observed[TotalN];
  int Total2Hidden[TotalN];
  vector<lower=0>[TotalN] ParentDistance;
  int prior_only;  // should the likelihood be ignored?
  int Components;
  matrix[TotalN-1, TotalN-1] CovarianceMatrix;
  int FamiliesNum;
  int FamiliesSize;
  int FamiliesLists[FamiliesNum, FamiliesSize];
}
parameters {
  matrix[HiddenN-1, NumberOfDimensions] FFrontierHidden;
  vector[NumberOfDimensions] alpha; // the mean of the process
  vector<lower=0.1, upper=5>[NumberOfDimensions] sigma_B;

  cholesky_factor_corr[NumberOfDimensions] Lrescor_Sigma; 
  vector<lower=0.01, upper=20>[NumberOfDimensions] sigma_Sigma;
  vector<lower=0>[NumberOfDimensions] extraVariance;
}
transformed parameters {
  real stepping = 1.0;

  real targetPrior = 0;
  real targetLikelihood = 0;

  // intermediate steps
 matrix[NumberOfDimensions, NumberOfDimensions] Sigma_chol;
 matrix[NumberOfDimensions, NumberOfDimensions] Sigma;

 matrix[NumberOfDimensions,NumberOfDimensions] B;
 matrix[NumberOfDimensions,NumberOfDimensions] Omega;

  for(i in 1:NumberOfDimensions) {
     for(j in 1:NumberOfDimensions) {
       if(i==j) {
         B[i,j] = sigma_B[i];
       } else {
         B[i,j]= 0;
       }
     }
  }
//
 Sigma_chol = diag_pre_multiply(sigma_Sigma, Lrescor_Sigma);
 Sigma = multiply_lower_tri_self_transpose(Sigma_chol);

   // Sigma = instantaneous covariance
   // B = drift matrix (here assumed to be positive definite & symmetric for simplicity)
   
     // Now calculate Omega, the stationary covariance
     for(q in 1:NumberOfDimensions) {
       for(r in 1:NumberOfDimensions) {
         Omega[q,r] = Sigma[q,r] / (B[q,q] + B[r,r]);
       };
     }
  


 { //////////////////// likelihood block
  matrix[TotalN-1, NumberOfDimensions] FFrontierTotal;
  for(w in 1:HiddenN-1) {
     FFrontierTotal[w] = FFrontierHidden[w]; # ObservedN
  }
  for(w in 1:ObservedN) {
     FFrontierTotal[w+HiddenN-1] = FFrontierObserved[w];
  }
  targetPrior += normal_lpdf(alpha | 0, 1);

  targetPrior += normal_lpdf(sigma_B | 0, 1);
  targetPrior += normal_lpdf(sigma_Sigma | 0, 1);
  targetPrior += lkj_corr_cholesky_lpdf(Lrescor_Sigma | 1);

  for(family in 1:FamiliesNum) {
    int familySizeHere = FamiliesSize;
    for(language in 1:FamiliesSize) {
       if(FamiliesLists[family, language] == 0) {
          familySizeHere = language-1;
          break;
       }
    } // loop over 'language'

    { // block
     vector[NumberOfDimensions*familySizeHere] own_overall;
     matrix[NumberOfDimensions*familySizeHere, NumberOfDimensions*familySizeHere] fullCovMat;
     vector[NumberOfDimensions*familySizeHere] fullMeanVector;
   
     for(i in 1:familySizeHere) {
        for(v in 1:NumberOfDimensions) {
            fullMeanVector[NumberOfDimensions*(i-1)+v] = alpha[v];
        }
        for(j in 1:i) {
          matrix[NumberOfDimensions, NumberOfDimensions] covarianceHere;
          if(CovarianceMatrix[FamiliesLists[family,i],FamiliesLists[family,j]] > 30) { // essentially no statistical dependency
            for(w in 1:NumberOfDimensions) {
               for(z in 1:NumberOfDimensions) {
                 covarianceHere[w,z] = 0;
            }  }
          } else if(i == j) { // here, the distance is zero
            covarianceHere = Omega;
            for(b in 1:NumberOfDimensions) {
               covarianceHere[b,b] = covarianceHere[b,b] + extraVariance[b];
            }
          } else {
            matrix[NumberOfDimensions,NumberOfDimensions] exponentiated1 = matrix_exp(-CovarianceMatrix[FamiliesLists[family, i],FamiliesLists[family, j]] * B); // this would have to be matrix_exp if B weren't diagonal
            matrix[NumberOfDimensions,NumberOfDimensions] exponentiated2 = matrix_exp(-CovarianceMatrix[FamiliesLists[family, j],FamiliesLists[family, i]] * B);
            if(exponentiated1[1,1] != exponentiated1[1,1]) {
               print("line 112");
               print(exponentiated1);
               print(B);
               print(CovarianceMatrix[FamiliesLists[family, j],FamiliesLists[family, i]]);
            }
            covarianceHere = exponentiated1 * Omega * exponentiated2';
          }
          for(u in 1:NumberOfDimensions) {
           for(v in 1:NumberOfDimensions) {
              fullCovMat[NumberOfDimensions*(i-1)+u, NumberOfDimensions*(j-1)+v] = covarianceHere[u,v];
              fullCovMat[NumberOfDimensions*(j-1)+u, NumberOfDimensions*(i-1)+v] = covarianceHere[v,u];
//              if(covarianceHere[v,u] != covarianceHere[v,u]) {
//                print(covarianceHere);
//                print(113);
//              }
           } // loop over v
          } // loop over u
        } // loop over j
   
        for(j in 1:familySizeHere) {
           for(v in 1:NumberOfDimensions) {
              own_overall[NumberOfDimensions*(j-1)+v] = FFrontierTotal[FamiliesLists[family, j],v];
           }
        } // loop over j
        //print(fullCovMat);



     } // loop over i
        targetLikelihood += multi_normal_lpdf(own_overall | fullMeanVector, fullCovMat);
    } // end block
   } // loop over 'family'

 } // likelihood block
}
model {
  target += stepping * targetLikelihood + targetPrior;
}
generated quantities {
}

