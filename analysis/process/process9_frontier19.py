import math
import random
import torch
import sys

language = sys.argv[1] #"Korean_2.8"
languageCorpus = language
if languageCorpus == "Japanese_2.8":
   languageCorpus = "Japanese-GSD_2.8"
elif languageCorpus == "Czech_2.8":
   languageCorpus = "Czech-PDT_2.8"
elif languageCorpus == "German_2.8":
   languageCorpus = "German-GSD_2.8"



with open("../landscapes_noPOS_2.8_new.R.tsv", "r") as inFile:
    header = next(inFile).replace('"', "").strip().split(" ")
    header = dict(list(zip(header, range(len(header)))))
    data = [x.replace('"', "").split(" ")[1:] for x in inFile.read().strip().split("\n")]
realCongruences = {line[header["Language"]] : float(line[header["Congruence_All"]]) for line in data}
if language not in realCongruences:
    print("REAL CONGRUENCE NOT FOUND", language)
realCongruence = realCongruences.get(language, 0)
print("CONGRUENCE", realCongruence)

with open("../raw_output/real.tsv", "r") as inFile:
    header = next(inFile).strip().split("\t")
    header = dict(list(zip(header, range(len(header)))))
    data = [x.split("\t") for x in inFile.read().strip().split("\n")]
data = [x for x in data if x[header["language"]] == languageCorpus]
deplenReal = float(data[0][header["DepLen"]])
surprisalReal = float(data[0][header["Surprisal2"]])


with open("../raw_output/landscape.tsv", "r") as inFile:
    header = next(inFile).strip().split("\t")
    header = dict(list(zip(header, range(len(header)))))
    data = [x.split("\t") for x in inFile.read().strip().split("\n")]




data = [x for x in data if x[header["language"]] == languageCorpus and x[header["basic"]] != ""]


surprisal = torch.FloatTensor([float(x[header["Surprisal2"]]) for x in data])
deplen = torch.FloatTensor([float(x[header["DepLen"]]) for x in data])


with open("../raw_output/baselines.tsv", "r") as inFile:
    header_baselines = next(inFile).strip().split("\t")
    header_baselines = dict(list(zip(header_baselines, range(len(header_baselines)))))
    baselines = [x.split("\t") for x in inFile.read().strip().split("\n")]
baselines = [x for x in baselines if x[header_baselines["language"]] == languageCorpus and x[header_baselines["basic"]] != ""]
surprisal_baselines = torch.FloatTensor([float(x[header_baselines["Surprisal2"]]) for x in baselines])
deplen_baselines = torch.FloatTensor([float(x[header_baselines["DepLen"]]) for x in baselines])



with open(f"output/{__file__}.tsv", "a") as outFile:
   print(language, "AtLeastOneSuccess", float(torch.logical_or((surprisalReal < surprisal_baselines), (deplenReal < deplen_baselines)).float().sum()), file=outFile)
   print(language, "DepLenSuccess", float((deplenReal < deplen_baselines).float().sum()), file=outFile)
   print(language, "SurprisalSuccess", float((surprisalReal < surprisal_baselines).float().sum()), file=outFile)
   print(language, "BaselinesTotal", (surprisal_baselines).size()[0], file=outFile)
