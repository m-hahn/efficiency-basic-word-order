from ud_languages_28 import languages
import sys
import glob


languages = sorted(languages)

with open("../../../../../languages/collectPerVerbProperties/outputs/coexpression5_SOOrder.py.tsv", "r") as inFile:
    SOCloser = [x.split("\t") for x in inFile.read().strip().split("\n")]
    SOCloserAdditional = {x[0] : (float(x[1]), float(x[2])) for x in SOCloser[1:]}


with open("../../../../../languages/categorical_order/categoricalOrderAdditional_2.8.tsv", "r") as inFile:
    categorical = [x.split("\t") for x in inFile.read().strip().split("\n")]
    categoricalAdditional = {x[0] : x[1] for x in categorical[1:]}

with open("../../../../../languages/categorical_order/categorical_order_2.8.tsv", "r") as inFile:
    categorical = [x.split("\t") for x in inFile.read().strip().split("\n")]
    categorical = {x[0] : x[1] for x in categorical[1:]}

for x, y in categoricalAdditional.items():
    categorical[x] = y

frontier = []
import torch

from collections import defaultdict
byCategory = defaultdict(list)
for lang in languages:
    byCategory[categorical[lang]].append(lang)
    with open(f"../../../output_frontier/process_cat9_frontier14.py_{lang}_fOnFrontierDiscretizedByAngle.tsv", "r") as inFile:
       basic = [[float(q) for q in x.split(" ")[1:]] for x in inFile.read().strip().split("\n")]
    frontier.append(basic)
frontier = torch.FloatTensor(frontier)
print(frontier)

print(frontier.size())
print(frontier.mean(dim=0).mean(dim=0))
print(frontier.mean(dim=0))

fAcrossFrontier = frontier.mean(dim=1)

VSO = fAcrossFrontier[[i for i in range(len(languages)) if categorical[languages[i]] == "VSO"]]
print(VSO[:,2] / (VSO[:,0] + VSO[:,2]))
SOV = fAcrossFrontier[[i for i in range(len(languages)) if categorical[languages[i]] == "SOV"]]
print(SOV[:,2] / (SOV[:,0] + SOV[:,2]))

print(SOCloserAdditional)
SOCloserRatio = torch.FloatTensor([SOCloserAdditional[i][0] / (SOCloserAdditional[i][0] + SOCloserAdditional[i][1] + 0.0001) for i in ((languages))])
print(SOCloserRatio)

def correlation(x,y):
   return ((x-x.mean()) * (y-y.mean())).mean() / ((x.pow(2).mean() - x.mean().pow(2)) * (y.pow(2).mean() - y.mean().pow(2))).sqrt()

print(correlation((fAcrossFrontier[:,2] / (fAcrossFrontier[:,0] + fAcrossFrontier[:,2])), SOCloserRatio))


import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

viridis = cm.get_cmap('viridis', 8)

import math

fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(12, 6))



ax[0].set_xlim(-0.1,1.1)
ax[0].set_ylim(-0.1,1.1)

for i in range(len(languages)):
#   ax[0].scatter(SOCloserRatio[i], (fAcrossFrontier[i,2] / (fAcrossFrontier[i,0] + fAcrossFrontier[i,2])), color={"VSO" : "lightgreen", "SOV" : "red", "SVO" : "blue", "No dominant order" : "gray", "VOS" : "orange"}[categorical[languages[i]]])
 if categorical[languages[i]] in ["SOV", "VSO"]:
   ax[0].scatter(SOCloserRatio[i], (fAcrossFrontier[i,2] / (fAcrossFrontier[i,0] + fAcrossFrontier[i,2])), color={"VSO" : "lightgreen", "SOV" : "red", "SVO" : "blue", "No dominant order" : "gray", "VOS" : "orange"}[categorical[languages[i]]])

#indices = torch.ByteTensor([1 if families[x] == "Latin_Romance" else 0 for x in languages])
#for i in range(9):
#    ax[0].plot(-x_coordinates[indices].mean(dim=0)[i:i+2], -y_coordinates[indices].mean(dim=0)[i:i+2], c=viridis(float(fOnFrontier[indices].mean(dim=0)[i])), linewidth=4)
#
#meanFrontier = torch.stack([-x_coordinates[indices].mean(dim=0), -y_coordinates[indices].mean(dim=0)], dim=1)
#inner_points = torch.logical_not((interiorCoordinates.unsqueeze(1) < meanFrontier.unsqueeze(0)).all(dim=2).any(dim=1))
#ax[0].scatter(interiorCoordinates[inner_points,0], interiorCoordinates[inner_points, 1], c=fInterior[indices].mean(dim=0)[inner_points], cmap='viridis', vmin=0, vmax=1, alpha=0.04, edgecolors='none', s=500)
#
#
#

plt.show()
#


#print(byCategory)


