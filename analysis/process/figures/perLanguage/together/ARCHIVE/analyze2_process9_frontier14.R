congruence = read.csv("../../../../landscapes_noPOS_2.8_new.R.tsv", sep=" ")
output = read.csv("../../../output/process9_frontier14.py.tsv", sep=" ")
names(output) = c("Language", "Var", "Val")
library(tidyr)
library(dplyr)

library(ggpubr)

families = read.csv("../../../../../languages/genera_2.8.tsv", sep="\t")


case_marking = read.csv("../../../../../languages/case_marking_revised_2.8.tsv")

categorical = read.csv("../../../../../languages/categorical_order/categorical_order_2.8.tsv", sep="\t")
categorical2 = read.csv("../../../../../languages/categorical_order/categoricalOrderAdditional_2.8.tsv", sep="\t") %>% mutate(rationale=NULL)
categorical = rbind(categorical, categorical2) %>% filter(!is.na(description))

# TODO plot
library(ggplot2)
for(v in unique(output$Var)) {
   predicted = output %>% group_by(Language) %>% filter(Var == v) %>% summarise(Val = mean(Val, na.rm=TRUE))
   u = merge(predicted, congruence, by=c("Language"), all=TRUE)
#   u = merge(u, families, by=c("Language"))
   plot = ggplot(u, aes(x=Congruence_All, y=Val))  + stat_cor()+ geom_text(aes(label=Language, color=Family)) + theme_classic() + xlab("Real Congruence") + ylab(v)  + theme(legend.position="none")
   ggsave(plot, file=paste("figures/analyze", "process9_frontier14", v, ".pdf", sep="_"), height=6, width=6)

   u = merge(u, case_marking, by=c("Language"), all.x=TRUE)
   plot = ggplot(u, aes(x=Congruence_All, y=Val))  + stat_cor()+ geom_text(aes(label=Language, color=value)) + theme_classic() + xlab("Real Congruence") + ylab(v)  + theme(legend.position="none")
   ggsave(plot, file=paste("figures/analyze", "process9_frontier14", v, "case.pdf", sep="_"), height=6, width=6)

   u = merge(u, categorical, by=c("Language"), all.x=TRUE)
   plot = ggplot(u, aes(x=Congruence_All, y=Val))  + stat_cor()+ geom_text(aes(label=Language, color=description)) + theme_classic() + xlab("Real Congruence") + ylab(v)  + theme(legend.position="none")
   ggsave(plot, file=paste("figures/analyze", "process9_frontier14", v, "categorical.pdf", sep="_"), height=6, width=6)

}

library(brms)

doBRMSAnalysis = function(v) {
   predicted = output %>% group_by(Language)  %>% filter(Var == v) %>% summarise(Val = mean(Val, na.rm=TRUE))
   u = merge(predicted, congruence, by=c("Language"), all=TRUE)
   print(cor.test(u$Congruence_All, u$Val))
   u = merge(u, families, by=c("Language"))
   model = brm(Val ~ Congruence_All + (1+Congruence_All|Family.y) + (1+Congruence_All|Genus), data=u, iter=10000)
   samples = posterior_samples(model)
   print(v)
   print(mean(samples$b_Congruence_All))
   print(mean(samples$b_Congruence_All < 0))
   print(quantile(samples$b_Congruence_All, 0.025))
   print(quantile(samples$b_Congruence_All, 0.975))
 }


doBRMSAnalysis("AverageFAngleSpace")
#[1] "AverageFAngleSpace"
#[1] 0.4022825
#[1] 1e-04
#     2.5% 
#0.1863198 
#    97.5% 
#0.6369381 
#



doBRMSAnalysisCategorical = function(v) {
   predicted = output %>% group_by(Language)  %>% filter(Var == v) %>% summarise(Val = mean(Val, na.rm=TRUE))
   u = merge(predicted, congruence, by=c("Language"), all=TRUE)
   u = merge(u, families, by=c("Language"))
   u = merge(u, categorical, by=c("Language"), all.x=TRUE) %>% filter(description %in% c("SVO", "SOV", "VSO")) %>% mutate(Same = (description != "SVO"))
   print(cor.test(u$Same+0, u$Val))
   model = brm(Same ~ Val + (1+Val|Family.y) + (1+Val|Genus), data=u, iter=10000, family="bernoulli")
   samples = posterior_samples(model)
   print(v)
   print(mean(samples$b_Val))
   print(mean(samples$b_Val < 0))
   print(quantile(samples$b_Val, 0.025))
   print(quantile(samples$b_Val, 0.975))
 }
doBRMSAnalysisCategorical("AverageFAngleSpace")
#	Pearson's product-moment correlation
#
#data:  u$Same + 0 and u$Val
#t = 4.4352, df = 68, p-value = 3.447e-05
#alternative hypothesis: true correlation is not equal to 0
#95 percent confidence interval:
# 0.2686029 0.6376793
#sample estimates:
#      cor 
#0.4736807 
#[1] "AverageFAngleSpace"
#[1] 8.378138 --> clear coefficient
#[1] 0.00395 --> significant
#    2.5% 
#2.361957 
#   97.5% 
#15.94873 




#################
#predicted = output %>% group_by(Language)  %>% filter(Var == "Angle01") %>% summarise(Val = mean(Val, na.rm=TRUE))
#u = merge(predicted, congruence, by=c("Language"), all=TRUE)
#cor.test(u$Congruence_All, u$Val)
#
## is there a bias towards one angle?
#u = merge(u, families, by=c("Language"))
#model2 = brm(Val ~ 1 + (1|Family.y) + (1|Genus), data=u, iter=10000)
#print(summary(model2))
#
#
###########
#predicted = output %>% group_by(Language)  %>% filter(Var == "DistanceFromFrontierAlongLineRelative") %>% summarise(Val = mean(Val, na.rm=TRUE))
#u = merge(predicted, congruence, by=c("Language"), all=TRUE)
#cor.test(u$Congruence_All, u$Val)
#
## what radius do languages concentrate along?
#
#u = merge(u, families, by=c("Language"))
#model2 = brm(Val ~ 1 + (1|Family.y) + (1|Genus), data=u, iter=10000)
#print(summary(model2))
##Intercept     0.16      0.03     0.10     0.23 1.00    13165    11114
#
#
###################
#
#
#
#
#predicted = output %>% group_by(Language)  %>% filter(Var == "AverageFCurveSpace") %>% summarise(Val = mean(Val, na.rm=TRUE))
#predicted
#merge(predicted, congruence, by=c("Language"), all=TRUE)
#u = merge(predicted, congruence, by=c("Language"), all=TRUE)
#cor.test(u$Congruence_All, u$Val)
#plot(u$Congruence_All, u$Val)
#head(output)
#predicted = output %>% group_by(Language)  %>% filter(Var == "FNextFrontierByLine") %>% summarise(Val = mean(Val, na.rm=TRUE))
#u = merge(predicted, congruence, by=c("Language"), all=TRUE)
#cor.test(u$Congruence_All, u$Val)
#predicted = output %>% group_by(Language)  %>% filter(Var == "AverageFLambdaSpace") %>% summarise(Val = mean(Val, na.rm=TRUE))
#u = merge(predicted, congruence, by=c("Language"), all=TRUE)
#cor.test(u$Congruence_All, u$Val)
#
#
## [1] AverageFCurveSpace        FNextFrontier            
## [3] FNextFrontierByLine       LambdaEstimateAlongLine  
## [5] DistanceFrontierAlongLine LambdaEstimate           
## [7] DistanceRandom            DistanceFrontier         
## [9] SmoothedReal              AverageFLambdaSpace      
#
#predicted = output %>% group_by(Language)  %>% filter(Var == "SmoothedReal") %>% summarise(Val = mean(Val, na.rm=TRUE))
#u = merge(predicted, congruence, by=c("Language"), all=TRUE)
#cor.test(u$Congruence_All, u$Val)
#
#
## TODO brm by families/genera (as a sanity check)
library(brms) #
