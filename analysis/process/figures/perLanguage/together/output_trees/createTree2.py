
from collections import defaultdict
trees = defaultdict(set)
trees["Indo_European"] = set()

with open("../../../../../../languages/trees2_2.8.tsv", "r") as inFile:
    next(inFile)
    for line in inFile:
        line = line.strip().split("\t")
        if line[0] not in trees:
           trees[line[0]] = set()
        for i in range(len(line)-1):
            trees[line[i+1]].add(line[i])



print("\\documentclass{standalone}")
print("\\usepackage[utf8]{inputenc}")
print("\\usepackage[T1]{fontenc}")
print("\\usepackage{float}")
print("\\usepackage{forest}")
print("\\usepackage{graphicx}")
print("")
print("\\usepackage{tikz}")
print("\\usetikzlibrary{positioning,arrows}")
print("")
print("\\begin{document}")
print("")
print("")
 
import sys
import os

for language in trees:
  print("\\newsavebox\\"+language.replace("_","").replace("2.8", "UD"))
  print("\sbox{\\"+language.replace("_", "").replace("2.8", "UD")+"}{%")
  print(" % \\begin{tikzpicture}[remember picture, scale=.6, every node/.style={transform shape}] %sub-picture")
  print("\\begin{minipage}{0.1\linewidth}")
  print(language.replace("_", ""))
  if os.path.exists("../../process9_frontier11.py_"+language+"_smoothed.pdf"):
    print("")
    print("\\includegraphics[width=\linewidth]{{../../../process9_frontier11.py_"+language+"_smoothed}.pdf}")
  print("\\end{minipage}")
  print("%  \\end{tikzpicture}% needed, otherwise anchors are wrong!")
  print("}""")
  
 
def drawRecursively(lang):
    children = trees[lang]
    print("[")
    print("\\usebox{\\"+lang.replace("_", "").replace("2.8", "UD")+"}")
    for c in children:
        drawRecursively(c)
    print("]")

print("\\begin{forest}")
drawRecursively("Indo_European")
print("\\end{forest}")




print("\\end{document}")
