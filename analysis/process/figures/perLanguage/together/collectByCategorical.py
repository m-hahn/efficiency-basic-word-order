from ud_languages_28 import languages
import sys
import glob

group = sys.argv[1]

languages = sorted(languages)

with open("../../../../landscapes_noPOS_2.8_new.R.tsv", "r") as inFile:
  congruence = [x.replace('"', "").split(" ") for x in inFile.read().strip().split("\n")]
  header = dict(list(zip(congruence[0], range(len(congruence[0])))))
  congruence = {x[1] : x[1:] for x in congruence}

languages = sorted(languages, key=lambda x:float(congruence[x][header["Congruence_All"]]))
print(languages)

with open("../../../../../../optimization-landscapes/analysis/categorical_order/categoricalOrderAdditional_2.8.tsv", "r") as inFile:
    categorical = [x.split("\t") for x in inFile.read().strip().split("\n")]
    categoricalAdditional = {x[0] : x[1] for x in categorical[1:]}

with open("../../../../../../optimization-landscapes/analysis/categorical_order/categorical_order_2.8.tsv", "r") as inFile:
    categorical = [x.split("\t") for x in inFile.read().strip().split("\n")]
    categorical = {x[0] : x[1] for x in categorical[1:]}

for x, y in categoricalAdditional.items():
    categorical[x] = y

from collections import defaultdict
byCategory = defaultdict(list)
for lang in languages:
    byCategory[categorical[lang]].append(lang)

print(byCategory)

for category in byCategory:
 with open(f"output/{__file__}_{group}_{category.replace(' ', '')}.tex", "w") as outFile:
  print("\\documentclass[margin=0pt]{standalone}", file=outFile)
  print("\\usepackage{graphicx}", file=outFile)
  print("\\begin{document}", file=outFile)
  print("\\begin{tabular}{ccccccccccccccccccccccccccccccccc}", file=outFile)
  k = 12
  languages_ = byCategory[category]
  for i in range(0, len(languages_), k):
      print(" & ".join([x.replace("_", " ") for x in languages_[i:i+k]]) + "\\\\", file=outFile)
      figs = []
      for language in languages_[i:i+k]:
#          process_cat9_frontier14.py_Vietnamese_2.8_smoothed_SOV.pdf
         pdfFileName = "../../"+group+".py_"+language+"_smoothed"
         if len(glob.glob("output/"+pdfFileName+".pdf")) > 0:
                 figs.append("\\includegraphics[width=0.25\\textwidth]{{"+pdfFileName+"}.pdf}")
         else:
                 figs.append("")
      print(" & ".join(figs) + "\\\\", file=outFile)
  print("\\end{tabular}", file=outFile)
  print("\\end{document}", file=outFile)
  
