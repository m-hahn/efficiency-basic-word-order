# Analysis Scripts (Correlations and Mixed-Effects Analyses)

The main analysis scripts are the following:

* [Congruence and Efficiency, excluding IE](analyze2_process9_frontier15_AverageFAngleSpace_Congruence2_NoIE.R)
* [Congruence and Efficiency](analyze2_process9_frontier15_AverageFAngleSpace_Congruence2.R)
* [Coexpression of subjects and objects](analyze2_process9_frontier15_AverageFAngleSpace_Congruence_Coexpressioin.R)

