
	Pearson's product-moment correlation

data:  u$Congruence_All and u$Val
t = 1.1707, df = 78, p-value = 0.2453
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 -0.09093859  0.34126779
sample estimates:
      cor 
0.1314039 


	Spearman's rank correlation rho

data:  u$Congruence_All and u$Val
S = 77990, p-value = 0.4478
alternative hypothesis: true rho is not equal to 0
sample estimates:
       rho 
0.08591186 


SAMPLING FOR MODEL '047f394817b351dbb9ffe23f89711662' NOW (CHAIN 1).
Chain 1: 
Chain 1: Gradient evaluation took 2.9e-05 seconds
Chain 1: 1000 transitions using 10 leapfrog steps per transition would take 0.29 seconds.
Chain 1: Adjust your expectations accordingly!
Chain 1: 
Chain 1: 
Chain 1: Iteration:     1 / 20000 [  0%]  (Warmup)
Chain 1: Iteration:  2000 / 20000 [ 10%]  (Warmup)
Chain 1: Iteration:  4000 / 20000 [ 20%]  (Warmup)
Chain 1: Iteration:  6000 / 20000 [ 30%]  (Warmup)
Chain 1: Iteration:  8000 / 20000 [ 40%]  (Warmup)
Chain 1: Iteration: 10000 / 20000 [ 50%]  (Warmup)
Chain 1: Iteration: 10001 / 20000 [ 50%]  (Sampling)
Chain 1: Iteration: 12000 / 20000 [ 60%]  (Sampling)
Chain 1: Iteration: 14000 / 20000 [ 70%]  (Sampling)
Chain 1: Iteration: 16000 / 20000 [ 80%]  (Sampling)
Chain 1: Iteration: 18000 / 20000 [ 90%]  (Sampling)
Chain 1: Iteration: 20000 / 20000 [100%]  (Sampling)
Chain 1: 
Chain 1:  Elapsed Time: 17.0466 seconds (Warm-up)
Chain 1:                18.8063 seconds (Sampling)
Chain 1:                35.8529 seconds (Total)
Chain 1: 

SAMPLING FOR MODEL '047f394817b351dbb9ffe23f89711662' NOW (CHAIN 2).
Chain 2: 
Chain 2: Gradient evaluation took 2.1e-05 seconds
Chain 2: 1000 transitions using 10 leapfrog steps per transition would take 0.21 seconds.
Chain 2: Adjust your expectations accordingly!
Chain 2: 
Chain 2: 
Chain 2: Iteration:     1 / 20000 [  0%]  (Warmup)
Chain 2: Iteration:  2000 / 20000 [ 10%]  (Warmup)
Chain 2: Iteration:  4000 / 20000 [ 20%]  (Warmup)
Chain 2: Iteration:  6000 / 20000 [ 30%]  (Warmup)
Chain 2: Iteration:  8000 / 20000 [ 40%]  (Warmup)
Chain 2: Iteration: 10000 / 20000 [ 50%]  (Warmup)
Chain 2: Iteration: 10001 / 20000 [ 50%]  (Sampling)
Chain 2: Iteration: 12000 / 20000 [ 60%]  (Sampling)
Chain 2: Iteration: 14000 / 20000 [ 70%]  (Sampling)
Chain 2: Iteration: 16000 / 20000 [ 80%]  (Sampling)
Chain 2: Iteration: 18000 / 20000 [ 90%]  (Sampling)
Chain 2: Iteration: 20000 / 20000 [100%]  (Sampling)
Chain 2: 
Chain 2:  Elapsed Time: 16.341 seconds (Warm-up)
Chain 2:                15.4354 seconds (Sampling)
Chain 2:                31.7764 seconds (Total)
Chain 2: 

SAMPLING FOR MODEL '047f394817b351dbb9ffe23f89711662' NOW (CHAIN 3).
Chain 3: 
Chain 3: Gradient evaluation took 2e-05 seconds
Chain 3: 1000 transitions using 10 leapfrog steps per transition would take 0.2 seconds.
Chain 3: Adjust your expectations accordingly!
Chain 3: 
Chain 3: 
Chain 3: Iteration:     1 / 20000 [  0%]  (Warmup)
Chain 3: Iteration:  2000 / 20000 [ 10%]  (Warmup)
Chain 3: Iteration:  4000 / 20000 [ 20%]  (Warmup)
Chain 3: Iteration:  6000 / 20000 [ 30%]  (Warmup)
Chain 3: Iteration:  8000 / 20000 [ 40%]  (Warmup)
Chain 3: Iteration: 10000 / 20000 [ 50%]  (Warmup)
Chain 3: Iteration: 10001 / 20000 [ 50%]  (Sampling)
Chain 3: Iteration: 12000 / 20000 [ 60%]  (Sampling)
Chain 3: Iteration: 14000 / 20000 [ 70%]  (Sampling)
Chain 3: Iteration: 16000 / 20000 [ 80%]  (Sampling)
Chain 3: Iteration: 18000 / 20000 [ 90%]  (Sampling)
Chain 3: Iteration: 20000 / 20000 [100%]  (Sampling)
Chain 3: 
Chain 3:  Elapsed Time: 18.6875 seconds (Warm-up)
Chain 3:                17.4214 seconds (Sampling)
Chain 3:                36.1089 seconds (Total)
Chain 3: 

SAMPLING FOR MODEL '047f394817b351dbb9ffe23f89711662' NOW (CHAIN 4).
Chain 4: 
Chain 4: Gradient evaluation took 1.5e-05 seconds
Chain 4: 1000 transitions using 10 leapfrog steps per transition would take 0.15 seconds.
Chain 4: Adjust your expectations accordingly!
Chain 4: 
Chain 4: 
Chain 4: Iteration:     1 / 20000 [  0%]  (Warmup)
Chain 4: Iteration:  2000 / 20000 [ 10%]  (Warmup)
Chain 4: Iteration:  4000 / 20000 [ 20%]  (Warmup)
Chain 4: Iteration:  6000 / 20000 [ 30%]  (Warmup)
Chain 4: Iteration:  8000 / 20000 [ 40%]  (Warmup)
Chain 4: Iteration: 10000 / 20000 [ 50%]  (Warmup)
Chain 4: Iteration: 10001 / 20000 [ 50%]  (Sampling)
Chain 4: Iteration: 12000 / 20000 [ 60%]  (Sampling)
Chain 4: Iteration: 14000 / 20000 [ 70%]  (Sampling)
Chain 4: Iteration: 16000 / 20000 [ 80%]  (Sampling)
Chain 4: Iteration: 18000 / 20000 [ 90%]  (Sampling)
Chain 4: Iteration: 20000 / 20000 [100%]  (Sampling)
Chain 4: 
Chain 4:  Elapsed Time: 16.7204 seconds (Warm-up)
Chain 4:                33.0388 seconds (Sampling)
Chain 4:                49.7592 seconds (Total)
Chain 4: 
 Family: gaussian 
  Links: mu = identity; sigma = identity 
Formula: Congruence_All ~ Val + (1 + Val | Genus) 
   Data: u (Number of observations: 31) 
  Draws: 4 chains, each with iter = 20000; warmup = 10000; thin = 1;
         total post-warmup draws = 40000

Group-Level Effects: 
~Genus (Number of levels: 16) 
                   Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
sd(Intercept)          0.27      0.18     0.01     0.70 1.00     8169    12074
sd(Val)                0.34      0.23     0.02     0.87 1.00     4740    12663
cor(Intercept,Val)    -0.15      0.57    -0.96     0.92 1.00     9096    17907

Population-Level Effects: 
          Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
Intercept     0.25      0.20    -0.16     0.62 1.00    21185    23867
Val           0.42      0.26    -0.08     0.93 1.00    17752    25697

Family Specific Parameters: 
      Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
sigma     0.21      0.04     0.15     0.32 1.00    15066    19069

Draws were sampled using sampling(NUTS). For each parameter, Bulk_ESS
and Tail_ESS are effective sample size measures, and Rhat is the potential
scale reduction factor on split chains (at convergence, Rhat = 1).
    Estimate Est.Error      Q2.5     Q97.5
R2 0.6777444 0.1091574 0.3940518 0.8189025
[1] "Distance_Surprisal_ComparedFrontier"
[1] 0.4230673
[1] 0.046725
       2.5% 
-0.07898135 
    97.5% 
0.9343349 
