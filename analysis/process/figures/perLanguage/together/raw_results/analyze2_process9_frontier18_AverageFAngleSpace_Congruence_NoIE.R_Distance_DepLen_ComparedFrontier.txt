
	Pearson's product-moment correlation

data:  u$Congruence_All and u$Val
t = -4.799, df = 78, p-value = 7.532e-06
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 -0.6309707 -0.2879321
sample estimates:
       cor 
-0.4774442 


	Spearman's rank correlation rho

data:  u$Congruence_All and u$Val
S = 124244, p-value = 2.603e-05
alternative hypothesis: true rho is not equal to 0
sample estimates:
       rho 
-0.4562119 


SAMPLING FOR MODEL '047f394817b351dbb9ffe23f89711662' NOW (CHAIN 1).
Chain 1: 
Chain 1: Gradient evaluation took 2.4e-05 seconds
Chain 1: 1000 transitions using 10 leapfrog steps per transition would take 0.24 seconds.
Chain 1: Adjust your expectations accordingly!
Chain 1: 
Chain 1: 
Chain 1: Iteration:     1 / 20000 [  0%]  (Warmup)
Chain 1: Iteration:  2000 / 20000 [ 10%]  (Warmup)
Chain 1: Iteration:  4000 / 20000 [ 20%]  (Warmup)
Chain 1: Iteration:  6000 / 20000 [ 30%]  (Warmup)
Chain 1: Iteration:  8000 / 20000 [ 40%]  (Warmup)
Chain 1: Iteration: 10000 / 20000 [ 50%]  (Warmup)
Chain 1: Iteration: 10001 / 20000 [ 50%]  (Sampling)
Chain 1: Iteration: 12000 / 20000 [ 60%]  (Sampling)
Chain 1: Iteration: 14000 / 20000 [ 70%]  (Sampling)
Chain 1: Iteration: 16000 / 20000 [ 80%]  (Sampling)
Chain 1: Iteration: 18000 / 20000 [ 90%]  (Sampling)
Chain 1: Iteration: 20000 / 20000 [100%]  (Sampling)
Chain 1: 
Chain 1:  Elapsed Time: 11.3338 seconds (Warm-up)
Chain 1:                10.318 seconds (Sampling)
Chain 1:                21.6517 seconds (Total)
Chain 1: 

SAMPLING FOR MODEL '047f394817b351dbb9ffe23f89711662' NOW (CHAIN 2).
Chain 2: 
Chain 2: Gradient evaluation took 1.5e-05 seconds
Chain 2: 1000 transitions using 10 leapfrog steps per transition would take 0.15 seconds.
Chain 2: Adjust your expectations accordingly!
Chain 2: 
Chain 2: 
Chain 2: Iteration:     1 / 20000 [  0%]  (Warmup)
Chain 2: Iteration:  2000 / 20000 [ 10%]  (Warmup)
Chain 2: Iteration:  4000 / 20000 [ 20%]  (Warmup)
Chain 2: Iteration:  6000 / 20000 [ 30%]  (Warmup)
Chain 2: Iteration:  8000 / 20000 [ 40%]  (Warmup)
Chain 2: Iteration: 10000 / 20000 [ 50%]  (Warmup)
Chain 2: Iteration: 10001 / 20000 [ 50%]  (Sampling)
Chain 2: Iteration: 12000 / 20000 [ 60%]  (Sampling)
Chain 2: Iteration: 14000 / 20000 [ 70%]  (Sampling)
Chain 2: Iteration: 16000 / 20000 [ 80%]  (Sampling)
Chain 2: Iteration: 18000 / 20000 [ 90%]  (Sampling)
Chain 2: Iteration: 20000 / 20000 [100%]  (Sampling)
Chain 2: 
Chain 2:  Elapsed Time: 11.8602 seconds (Warm-up)
Chain 2:                10.9329 seconds (Sampling)
Chain 2:                22.7931 seconds (Total)
Chain 2: 

SAMPLING FOR MODEL '047f394817b351dbb9ffe23f89711662' NOW (CHAIN 3).
Chain 3: 
Chain 3: Gradient evaluation took 1.5e-05 seconds
Chain 3: 1000 transitions using 10 leapfrog steps per transition would take 0.15 seconds.
Chain 3: Adjust your expectations accordingly!
Chain 3: 
Chain 3: 
Chain 3: Iteration:     1 / 20000 [  0%]  (Warmup)
Chain 3: Iteration:  2000 / 20000 [ 10%]  (Warmup)
Chain 3: Iteration:  4000 / 20000 [ 20%]  (Warmup)
Chain 3: Iteration:  6000 / 20000 [ 30%]  (Warmup)
Chain 3: Iteration:  8000 / 20000 [ 40%]  (Warmup)
Chain 3: Iteration: 10000 / 20000 [ 50%]  (Warmup)
Chain 3: Iteration: 10001 / 20000 [ 50%]  (Sampling)
Chain 3: Iteration: 12000 / 20000 [ 60%]  (Sampling)
Chain 3: Iteration: 14000 / 20000 [ 70%]  (Sampling)
Chain 3: Iteration: 16000 / 20000 [ 80%]  (Sampling)
Chain 3: Iteration: 18000 / 20000 [ 90%]  (Sampling)
Chain 3: Iteration: 20000 / 20000 [100%]  (Sampling)
Chain 3: 
Chain 3:  Elapsed Time: 11.7978 seconds (Warm-up)
Chain 3:                11.8449 seconds (Sampling)
Chain 3:                23.6427 seconds (Total)
Chain 3: 

SAMPLING FOR MODEL '047f394817b351dbb9ffe23f89711662' NOW (CHAIN 4).
Chain 4: 
Chain 4: Gradient evaluation took 1.6e-05 seconds
Chain 4: 1000 transitions using 10 leapfrog steps per transition would take 0.16 seconds.
Chain 4: Adjust your expectations accordingly!
Chain 4: 
Chain 4: 
Chain 4: Iteration:     1 / 20000 [  0%]  (Warmup)
Chain 4: Iteration:  2000 / 20000 [ 10%]  (Warmup)
Chain 4: Iteration:  4000 / 20000 [ 20%]  (Warmup)
Chain 4: Iteration:  6000 / 20000 [ 30%]  (Warmup)
Chain 4: Iteration:  8000 / 20000 [ 40%]  (Warmup)
Chain 4: Iteration: 10000 / 20000 [ 50%]  (Warmup)
Chain 4: Iteration: 10001 / 20000 [ 50%]  (Sampling)
Chain 4: Iteration: 12000 / 20000 [ 60%]  (Sampling)
Chain 4: Iteration: 14000 / 20000 [ 70%]  (Sampling)
Chain 4: Iteration: 16000 / 20000 [ 80%]  (Sampling)
Chain 4: Iteration: 18000 / 20000 [ 90%]  (Sampling)
Chain 4: Iteration: 20000 / 20000 [100%]  (Sampling)
Chain 4: 
Chain 4:  Elapsed Time: 11.1205 seconds (Warm-up)
Chain 4:                10.3225 seconds (Sampling)
Chain 4:                21.443 seconds (Total)
Chain 4: 
 Family: gaussian 
  Links: mu = identity; sigma = identity 
Formula: Congruence_All ~ Val + (1 + Val | Genus) 
   Data: u (Number of observations: 31) 
  Draws: 4 chains, each with iter = 20000; warmup = 10000; thin = 1;
         total post-warmup draws = 40000

Group-Level Effects: 
~Genus (Number of levels: 16) 
                   Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
sd(Intercept)          0.35      0.19     0.04     0.83 1.00    10445     8913
sd(Val)                0.33      0.27     0.01     1.02 1.00     6767    13542
cor(Intercept,Val)    -0.22      0.58    -0.98     0.91 1.00    16794    18678

Population-Level Effects: 
          Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
Intercept     1.02      0.21     0.60     1.44 1.00    28378    29307
Val          -0.62      0.30    -1.20    -0.03 1.00    28163    27110

Family Specific Parameters: 
      Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
sigma     0.21      0.04     0.14     0.31 1.00    16661    22968

Draws were sampled using sampling(NUTS). For each parameter, Bulk_ESS
and Tail_ESS are effective sample size measures, and Rhat is the potential
scale reduction factor on split chains (at convergence, Rhat = 1).
    Estimate Est.Error      Q2.5     Q97.5
R2 0.6965063 0.1051257 0.4215212 0.8283147
[1] "Distance_DepLen_ComparedFrontier"
[1] -0.6245016
[1] 0.979125
     2.5% 
-1.197238 
      97.5% 
-0.02619471 
