from ud_languages_28 import languages
import sys
import glob

group = sys.argv[1]

languages = sorted(languages)

with open("../../../../landscapes_noPOS_2.8_new.R.tsv", "r") as inFile:
  congruence = [x.replace('"', "").split(" ") for x in inFile.read().strip().split("\n")]
  header = dict(list(zip(congruence[0], range(len(congruence[0])))))
  congruence = {x[1] : x[1:] for x in congruence}

languages = sorted(languages, key=lambda x:float(congruence[x][header["Congruence_All"]]))
print(languages)

with open(f"output/{__file__}_{group}.tex", "w") as outFile:
  print("\\documentclass[margin=0pt]{standalone}", file=outFile)
  print("\\usepackage{graphicx}", file=outFile)
  print("\\begin{document}", file=outFile)
  print("\\begin{tabular}{ccccccccccccccccccccccccccccccccc}", file=outFile)
  k = 12
  for i in range(0, len(languages), k):
      print(" & ".join([x.replace("_", " ") for x in languages[i:i+k]]) + "\\\\", file=outFile)
      figs = []
      for language in languages[i:i+k]:
         pdfFileName = "../../"+group+".py_"+language+"_smoothed"
         if len(glob.glob("output/"+pdfFileName+".pdf")) > 0:
                 figs.append("\\includegraphics[width=0.25\\textwidth]{{"+pdfFileName+"}.pdf}")
         else:
                 figs.append("")
      print(" & ".join(figs) + "\\\\", file=outFile)
  print("\\end{tabular}", file=outFile)
  print("\\end{document}", file=outFile)
  
