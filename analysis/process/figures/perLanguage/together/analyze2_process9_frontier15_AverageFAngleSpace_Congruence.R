congruence = read.csv("../../../../landscapes_noPOS_2.8_new.R.tsv", sep=" ")
output = read.csv("../../../output/process9_frontier18_Revised.py.tsv", sep=" ")
names(output) = c("Language", "Var", "Val")
library(tidyr)
library(dplyr)

library(ggpubr)

families = read.csv("../../../../../languages/genera_2.8.tsv", sep="\t")


case_marking = read.csv("../../../../../languages/case_marking_revised_2.8.tsv")

categorical = read.csv("../../../../../languages/categorical_order/categorical_order_2.8.tsv", sep="\t")
categorical2 = read.csv("../../../../../languages/categorical_order/categoricalOrderAdditional_2.8.tsv", sep="\t") %>% mutate(rationale=NULL)
categorical = rbind(categorical, categorical2) %>% filter(!is.na(description))

library(brms)

FILE = "analyze2_process9_frontier18_AverageFAngleSpace_Congruence.R"

doBRMSAnalysis = function(v) {
   predicted = output %>% group_by(Language)  %>% filter(Var == v) %>% summarise(Val = mean(Val, na.rm=TRUE))
   u = merge(predicted, congruence, by=c("Language"), all=TRUE)
   sink(paste("raw_results/", FILE, "_", v, ".txt", sep=""))
   print(cor.test(u$Congruence_All, u$Val))
   print(cor.test(u$Congruence_All, u$Val, method="spearman"))
   u = merge(u, families, by=c("Language"))
#   model = brm(Val ~ Congruence_All + (1+Congruence_All|Family.y) + (1+Congruence_All|Genus), data=u, iter=20000, control=list(adapt_delta=0.95))
   model = brm(Val ~ Congruence_All + (1+Congruence_All|Genus), data=u, iter=20000, control=list(adapt_delta=0.99), prior=c(prior("normal(0,1)", class="b"), prior("student_t(3,0,2.5)", class="sigma"), prior("student_t(3,0,2.5)", class="sd"), prior("normal(0.5, 1)", class="Intercept")))
   samples = posterior_samples(model)
   print(summary(model))
   print(bayes_R2(model))
   print(v)
   print(mean(samples$b_Congruence_All))
   print(mean(samples$b_Congruence_All < 0))
   print(quantile(samples$b_Congruence_All, 0.025))
   print(quantile(samples$b_Congruence_All, 0.975))
   sink()
 }
doBRMSAnalysis("AverageFAngleSpace")
doBRMSAnalysis("Distance_Surprisal_ComparedFrontier")
doBRMSAnalysis("Distance_DepLen_ComparedFrontier")

