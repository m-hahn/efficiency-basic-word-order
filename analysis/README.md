# Lists of languages and their families

* families_2.8.tsv
* ud_languages_2.8.txt
* genera_2.8.tsv

# Collect results
* [Full](landscapes_NoPOS_2.8_VIZ_AUC-HalfDLM.R)
* [Only DL (SI Section S20)](landscapes_NoPOS_2.8_VIZ_AUC-HalfDLM_OnlyDLM.R)
* [Only IL (SI Section S20)](landscapes_NoPOS_2.8_VIZ_AUC-HalfDLM_OnlyIL.R)
* [Subsets](landscapes_NoPOS_2.8_VIZ_AUC-HalfDLM_Subset.R)
* [Neural Estimators (SI Section S23)](landscapes_NoPOS_2.8_VIZ_AUC-HalfDLM_Neural.R)


# Greenberg Correlations (SI Section S21)

* [Collect](landscapes_NoPOS_2.8_VIZ_AUC-HalfDLM_Greenberg.R)

